#Dockerfile

Por medio de esta herramienta es posible la creación de contenedores en forma organizada y con ello, el versionado de los pasos hasta lograr la imagen deseada.

## Crear carpeta y archivo de configuración

Inicialmente se crea una carpeta cuyo nombre esté asociado al contenedor que se desea crear y luego un archivo cuyo nombre es "Dockerfile"

```bash
$ mkdir "Carpeta"
$ cd "Carpeta"
~/"Carpeta"$ nano Dockerfile
```

Donde: |
-- | --
**Carpeta** = Directorio donde se realizará el contenedor. |

## Instrucciones de Dockerfile

La estructura del archivo Dockerfile se compone de un conjunto de instrucciones, las cuales pueden ser:

Instrucción | Sintaxis | función
-- | -- | --
FROM | | especifica la imagen que se va a utilizar como base del contenedor.
MAINTAINER | MAINTAINER \<nombre> \<Correo> | nombre y contacto de quien desarrolla o mantiene el contenedor.
RUN | RUN \<comandos> | comando/s que se van a ejecutar dentro del nuevo contenedor.
ENV | ENV \<key>=\<value> | indica las variables que se especifican en el contenedor.
ADD | ADD \<src>... \<dest> | copia archivos hacia el contenedor.
EXPOSE | EXPOSE \<puertos> | permite poner al contenedor a escuchar en los puertos indicados.


## Creación del contenedor

Mediante el comando docker y opción build, se ejecuta lo indicado en el archivo Dockerfile y se crea una imagen, como se muestra a continuación:

```bash
~/"Carpeta"$ docker build -t "nombre" "ubicacion" 
```

Donde: | 
-- | --
**nombre** = nombre con que se crea la imagen | **ubicacion**  = directorio donde se encuentra el archivo Dockerfile

### Ejemplo
