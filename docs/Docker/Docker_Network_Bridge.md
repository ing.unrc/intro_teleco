# Bridge

Un **bridge** es un dispositivo/software de capa de enlace capaz de reenviar tráfico entre segmentos de una red. En caso de docker, es utilizado para interconectar contenedores, proveyendo aislación entre los que no estén conectado a un mismo bridge.

Puede aplicarse entre contenedores que se encuentran en la misma PC host. Si se quiere conectar contenedores entre diferentes host, es necesario implementar ruteo a nivel del sistema operativo.

Cuando se inicia Docker, se crea automáticamente un bridge de red y se utiliza automáticamente para interconectar los contenedores que no tienen una configuración específica. Apesar de ello, se puede configurar bridges en forma manual.

Sitio web [link](https://docs.docker.com/network/bridge/)

## Bridge por defecto

Éste es considerado el modo por defecto de Docker al no indicarse ninguna red mediante el argumento **--network**.

Aquellos contenedores que se encuentren en esta red, pueden comunicarse entre sí a nivel de dirección IP.

## Bridge creado por usuario

Gestión de Bridges [link](https://docs.docker.com/engine/reference/commandline/network_create/)

## Crear una red Bridge

Como se indicó anteriormente, es el modo por defecto, por lo que es equivalente utilizar **--drive bridge** o no contemplarlo. Esto se muestra en estas dos opciones, las cuales son equivalentes:

```bash
docker network create "NombreDeRed"
```

```bash
docker network create --driver bridge "NombreDeRed"
```

Donde: ||
-- | -- |
**NombreRed** = Nombre de la red que se quiere analizar | |

### Ejemplo de carga de una red en modo Bridge

```bash
docker network create RedBridge
ba2e1dfc337345f049cd859343ac869b49ee63bb0961da119b3f8005ac1fd57f

docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
ba2e1dfc3373        RedBridge           bridge              local
```
