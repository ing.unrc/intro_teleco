#Docker

Web Site: [www.docker.com](https://www.docker.com/)

Docker Compose installer: [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

## How Docker works
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/pgiFwriExYg" width="800" height="450" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## First Steps
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/eCm9xUl7quk" width="800" height="450" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
