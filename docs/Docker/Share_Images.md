## Create a new image from a container's changes
```bash
docker commit "docker_id" "image_name":"tag"
```

## Export an image into a .tar file
```bash
docker export "container_id" > "image.tar"
```

## Import an image from a .tar file
```bash
docker import "image.tar" "image_name":"tag"
```