# Docker Compose - Archivos de configuración

## Definir dependencias.

### Crear una carpeta para el proyecto:

```bash
mkdir "NombreCarpeta"
cd "NombreCarpeta"
```

Donde: ||
-- | -- |
**NombreCarpeta** = Nombre de la carpeta a donde se va a alojar el proyecto | |

## Archivo **Dockerfile**

Crear un nuevo archivo llamado **Dockerfile** , el cual contiene el entorno necesario para ejecutar el proyecto.

## Archivo **docker-compose.yml**

Finalmente, crear un archivo llamado **docker-compose.yml** con:

```bash
version: "3.8"
services:
  web:
    build: .
    ports:
      - "5000:5000"
  redis:
    image: "redis:alpine"
```

## Construir y ejecutar la configuración

Dentro de la carpeta del proyecto ejecutar el siguiente comando:

```bash
docker-compose up
```
