# Instalación de Docker 

Sitio del instalador de Docker Compose: [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

## Prerequisitos

Es necesario haber instalado Docker Engine.

## Instalación

Descargar la versión actual de Docker Compose:

```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

```

Cambiar los permisos de ejecución del binario:

```bash
sudo chmod +x /usr/local/bin/docker-compose

```

Comprobar la instalación:

```bash
docker-compose --version
```

## Desinstalación:

```bash
sudo rm /usr/local/bin/docker-compose

```
