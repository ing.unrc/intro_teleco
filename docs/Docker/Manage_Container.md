# Installation and use of Docker containers

## Run a command in a new container
```bash
docker run -i -t "container_name" "image_name":"tag"
```

-i: to have interaction        -t: to use a pseudo terminal
After running the command (point 4.1), the linux terminal enters automatically the new docker container terminal (notice that the prompt changes). For example:
```bash
GuestMachine:/#
```

## Exit the container
```bash
GuestMachine:/# exit
```

## See all containers
```bash
docker ps -a
```

CONTAINER ID
IMAGE
COMMAND
CREATED
STATUS
PORTS
NAMES
docker_id
image_name:tag








container_name
In this visualization it is possible to know the docker identification. This “id” is necessary to work with it.

## Start a container:
```bash
docker start "docker_id"
```

## Enter the container:
```bash
docker attach "docker_id"
```

## Remove a docker
```bash
docker rm "docker_id"
```

## Copy files from/to the container (this command is like scp):
```bash
docker cp "file" "docker_id":"location"
```
## Remove a container:
```bash
docker rm "docker_id"
```