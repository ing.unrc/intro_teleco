# host

En este modo de configuración, el stack de red del contenedor no estará aislado del de la computadora host.
Con ello ambos se encontrarán en el mismo namespace y no es posible asignar una IP en particular al contenedor.

Sitio web [link](https://docs.docker.com/network/host/)

## **ports** (mapeo de puertos)

Este modo de red es incompatible con el mapeo de puertos.

## Ejemplo de asignación de red "host" con comando docker

```bash
docker run -it --network host ubuntu:latest bash
```

## Ejemplo de asignación de red "host" en docker-compose.yml

Ejemplo de contenedor de "nginx". Nótese que no se pueden mapear los puertos, por lo que si el host cuenta por ejemplo con un servidor "nginx" o "apache2", no se podrá acceder al contenido del contenedor al puerto 80 ya que estará ocupado.

```bash
version: "3.8"
services:
  ubuntu:
    image: ubuntu:latest
    network_mode: host
```
