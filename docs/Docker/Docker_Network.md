# docker network

El comando **docker network** permite gestionar las redes internas de docker y la asignación a contenedores, entre otras opciones. 

A continuación se listan algunas opciones para operar con este comando y luego en otras secciones del tutorial se analizan cómo es el funcionamiento y configuración de cada uno de los modos de red en particular.

## connect 

Conecta un contenedor a una red.

## create

Comando para crear una nueva red.

```bash
docker network create --driver "Modo" "NombreRed"
```

Donde: ||
-- | -- |
**Modo** = Indica el modo de funcionamiento de la red: **bridge**/**ipvlan**/**macvlan**/**overlay** | |
**NombreRed** = Nombre de la red que se quiere analizar | |

## disconnect 

Desconecta un contenedor de una red.

## inspect

Visualizar detalle sobre la configuración de una red:

```bash
docker network inspect "NombreRed"
```

Donde: ||
-- | -- |
**NombreRed** = Nombre de la red que se quiere analizar ||

Visualizar detalle sobre la configuración de red en un contenedor:

```bash
docker inspect "NombreContenedor"
```

Donde: ||
-- | -- |
**NombreContenedor** = Nombre del un contenedor para visualizar la configuración de red asociada ||

## ls

Lista las redes creadas.

```bash

docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
"Identificador"     "Nombre"            "Tipo"
```

Donde: | |
-- | -- |
**Identificador** = Número identificador asignado a la red | **Nombre** = Nombre asignado por el usuario al momento de crear la red |
**Tipo** = Tipo de red (bridge, host, macvlan...) | |

## prune

Elimina todas las redes sin uso.

## rm

Elimina una o más redes.
