# Macvlan

Este driver permite asignar una dirección MAC a un contenedor, convirtiéndolo en un dispositivo físico de red.

Using the macvlan driver is sometimes the best choice when dealing with legacy applications that expect to be directly connected to the physical network, rather than routed through the Docker host’s network stack. See Macvlan networks.

Más información sobre Macvlan: [link](https://docs.docker.com/network/macvlan/)

Más información de la configuración dentro de un contenedor: [link](https://docs.docker.com/network/network-tutorial-macvlan/)

## Carga de una red macvlan

Crear una macvlan asociada a una interfaz en particular:

```bash
docker network create --driver macvlan \
  --subnet="IPRed"/"Mascara" \
  --ip-range="IPRango"/"MascaraRango" \
  --gateway="GW" \
  -o parent="Interfaz" "NombreRed"
```

Donde: ||
-- | -- |
**IPRed** = Dirección IP de red | **Mascara** = Máscara de red |
**GW** = Puerta de enlace de la red | **Interfaz** = Interfaz del host |
**NombreRed** = Nombre asociado a la nueva red | |

Notar:

* "--driver macvlan" especifica que se lleva a cabo este tipo de configuración.

* "--ip-range " determina un pool de direcciones a partir de una subred ("IPRango"/"MascaraRango") para asignar a los contenedores.

## Ejemplo de asignación de red "macvlan" con comando docker

```bash
docker run -it --network "NombreRed" ubuntu:latest bash
```

Donde: ||
-- | -- |
**NombreRed** = Nombre asociado a la red macvlan creada con anterioridad | |
