
#!/bin/bash 
#apt update
title="Instalador automático de Labredes"
prompt="Seleccione el programa a instalar"
options=("brave" "docker" "geany" "gns3" "iperf3" "Open VSwitch" "putty" "ssh" "virtualbox" "vlc" "winbox" "wine" "wireshark")

echo "$title"
PS3="$prompt "

if [ -z "$XDG_CURRENT_DESKTOP" ]
then
	echo "entro"
	opt=1
	#select opt in "brave" "docker" "geany" "gns3" "iperf3" "Open VSwitch" "putty" "ssh" "virtualbox" "vlc" "winbox" "wine" "wireshark"
else
    opt=$(zenity --title="$title" --text="$prompt" --list \
                   --column="Opciones" "${options[@]}")
fi

while $opt
	if [ -z "$XDG_CURRENT_DESKTOP" ]
	then
		PS3="Seleccioná una opción: ";
		echo "opciones: 1) brave 2)docker 3)geany 4)gns3";
		read opt
	fi
do
    case "$opt" in
    "brave")
    	sudo apt install apt-transport-https curl
		sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
		echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list
		sudo apt update
		sudo apt install brave-browser -y
		clear
		#zenity --info --text="Se ha instalado Brave en su computadora"
		;;
    "docker")
    	sudo apt remove docker docker-engine docker.io containerd runc
		sudo apt install apt-transport-https ca-certificates curl gnupg2 software-properties-common -y
		curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
		sudo add-apt-repository \
		   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
		   $(lsb_release -cs) \
		   stable"
		sudo apt install -y docker-ce docker-ce-cli containerd.io
		sudo groupadd docker
		sudo usermod -aG docker $USER
		clear    
		#zenity --info --text="Para que docker funcione correctamente, es necesario reiniciar la PC"
		;;
    "geany")
		sudo apt install -y geany
		clear
		#zenity --info --text="Se ha instalado geany en su computadora"
		;;
	"gns3")
		sudo add-apt-repository ppa:gns3/ppa -y
		sudo apt update
		sudo apt install -y gns3-gui gns3-server gnome-terminal tigervnc-viewer
		# Correccion de errores:
		sudo chown "user":"user" /dev/kvm
		sudo chmod +x /usr/bin/ubridge
		sudo chmod +x /usr/bin/dumpcap
		clear;;
	"iperf3")		
		sudo apt remove iperf3 libiperf0 -y
		sudo apt install libsctp1 -y
		wget https://iperf.fr/download/ubuntu/libiperf0_3.9-1_amd64.deb
		wget https://iperf.fr/download/ubuntu/iperf3_3.9-1_amd64.deb
		sudo dpkg -i libiperf0_3.9-1_amd64.deb iperf3_3.9-1_amd64.deb
		rm libiperf0_3.9-1_amd64.deb iperf3_3.9-1_amd64.deb
		clear
		#zenity --info --text="Se instaló iperf3 version 3.9-1 (17/08/2020)"
		;;			
	"Open VSwitch")
		sudo apt install openvswitch-switch
		;;
	"putty")
		sudo apt install -y putty
		clear;;
	"ssh")
		sudo apt install -y openssh-server
		clear;; 
	"virtualbox")
		sudo apt install -y virtualbox virtualbox-ext-pack
		clear;; 
	"vlc") 
		sudo apt install -y vlc
		clear;; 
	"winbox")
		sudo apt install -y wine64
		wget https://mt.lv/winbox64
		wine ./winbox64 &
		clear
		#zenity --info --text="Se descargó winbox64.exe en esta carpeta e instaló wine64"
		;; 
	"wine")
		sudo apt install -y wine64
		clear;; 
	"wireshark")
		sudo apt install -y wireshark
		clear;; 	
    esac
done
