#!/bin/bash

#export NEWT_COLORS='
#window=,#33afff
#border=white,#33afff
#textbox=white,#33afff
#button=black,white
#'

CHOICES=$(whiptail --title "Instalador" --checklist "Seleccione los programas que desea instalar:" 20 60 12 \
brave "Navegador web" OFF \
docker "Gestor de contenedores" OFF \
geany "Editor de textos" OFF \
gns3 "Virtualización de redes" OFF \
iperf3 "Generador de tráfico" OFF \
"Open VSwitch" "Switch Virtual" OFF \
python "Programa + librerías" OFF \
putty "Conexión por consola" OFF \
rar "Compresión rar" OFF \
ssh "Servidor y cliente ssh" OFF \
virtualbox "Hipervisor de máquinas virtuales" OFF \
vlc "Reproductor multimedia" OFF \
winbox "Gestor de Mikrotik (instala wine)" OFF \
wine "Soporte para software windows" OFF \
wireshark "Captura y análisis de tráfico" OFF \
3>&1 1>&2 2>&3)

if [ -z "$CHOICE" ]; then
	for CHOICE in $CHOICES; do
		case "$CHOICE" in
			"\"brave\"")
				sudo apt install apt-transport-https curl
				sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
				echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list
				sudo apt update
				sudo apt install brave-browser -y
				clear
				;;
			"\"docker\"")
				sudo apt remove docker docker-engine docker.io containerd runc
				sudo apt install apt-transport-https ca-certificates curl gnupg2 software-properties-common -y
				curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
				sudo add-apt-repository \
				   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
				   $(lsb_release -cs) \
				   stable"
				sudo apt install -y docker-ce docker-ce-cli containerd.io
				sudo groupadd docker
				sudo usermod -aG docker $USER
				sudo chmod 666 /var/run/docker.sock
				clear
				echo "Para que docker funcione correctamente, es necesario reiniciar la PC"
				;; 
			"\"geany\"")
				sudo apt install -y geany
				clear
				;;
			"\"gns3\"")
				sudo add-apt-repository ppa:gns3/ppa -y
				sudo apt install -y gns3-gui gns3-server gnome-terminal tigervnc-viewer
				# Correccion de errores:
				sudo chown "user":"user" /dev/kvm
				sudo chmod +x /usr/bin/ubridge
				sudo chmod +x /usr/bin/dumpcap
				clear
				;;
			"\"iperf3\"")		
				sudo apt remove iperf3 libiperf0 -y
				sudo apt install libsctp1 -y
				wget https://iperf.fr/download/ubuntu/libiperf0_3.9-1_amd64.deb
				wget https://iperf.fr/download/ubuntu/iperf3_3.9-1_amd64.deb
				sudo dpkg -i libiperf0_3.9-1_amd64.deb iperf3_3.9-1_amd64.deb
				rm libiperf0_3.9-1_amd64.deb iperf3_3.9-1_amd64.deb
				clear
				echo "Se instaló iperf3 version 3.9-1 (17/08/2020)"
				;;			
			"\"Open VSwitch\"")
				sudo apt install openvswitch-switch
				;;		
			"\"putty\"")
				sudo apt install -y putty
				clear
				;;
			"\"rar\"")
				sudo apt install rar -y
				clear
				;;
			"\"ssh\"")
				sudo apt install -y openssh-server
				clear
				;; 
			"\"virtualbox\"")
				sudo apt install -y virtualbox virtualbox-ext-pack
				clear
				;; 
			"\"vlc\"") 
				sudo apt install -y vlc
				clear
				;; 
			"\"winbox\"")
				sudo apt install -y wine64
				wget https://mt.lv/winbox64
				wine ./winbox64 &
				clear
				echo "Se descargó winbox64.exe en esta carpeta e instaló wine64"
				;; 
			"\"wine\"")
				sudo apt install -y wine64
				clear
				;; 
			"\"wireshark\"")
				sudo apt install -y wireshark
				clear
				;;
			"\"python\"")
				rm instalador_python.sh
				wget instalador https://labredes.gitlab.io/tutoriales/scripts/instalador_python.sh
				chmod +x instalador_python.sh
				./instalador_python.sh
				rm instalador_python.sh
				clear
				;;
    *)
      echo "Opción no soportada!" >&2
      exit 1
      ;;
    esac
  done
else
  echo "No se selecteccionó alguna opción"
fi
rm instalador_tmp.sh
