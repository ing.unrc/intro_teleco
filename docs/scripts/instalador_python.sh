#!/bin/bash 

CHOICES=$(whiptail --title "Instalador Python" --checklist "Seleccione las librerías que desea instalar:" 20 70 12 \
jupyter-notebook "Servidor y web para programación" OFF \
asyncio "Programación asíncrona" OFF \
distfit "Distribución de probabilidad" OFF \
graphviz "Entorno iteractivo" OFF \
matplotlib "Gráficas" OFF \
numpy "Cálculo matemático" OFF \
pandas "Análisis y procesamiento de datos" OFF \
scapy "Generación de tráfico" OFF \
"sounddevice y soundfile" "Iteracción con placa de sonido" OFF \
xlrd "Plantillas de cálculo" OFF \
3>&1 1>&2 2>&3)


if [ -z "$CHOICE" ]; then
	for CHOICE in $CHOICES; do
		case "$CHOICE" in
			"\"jupyter-notebook\"")
				sudo apt install python3-pip texlive-xetex texlive-fonts-recommended texlive-plain-generic -y
				pip3 install notebook pandoc
				;;
			"\"asyncio\"")
				sudo apt install python3-pip -y
				pip3 install asyncio
				;;
			"\"distfit\"")
				sudo apt install python3-pip -y
				pip3 install distfit
				;;
			"\"graphviz\"")
				sudo apt-get install python3-pip graphviz -y
				pip3 install graphviz
				;;
			"\"matplotlib\"")
				sudo apt install python3-pip -y
				pip3 install matplotlib
				;;
			"\"numpy\"")
				sudo apt install python3-pip -y
				pip3 install numpy
				;;
			"\"pandas\"")
				sudo apt install python3-pip -y
				pip3 install pandas
				;;
			"\"scapy\"")
				sudo apt install python3-pip -y
				pip3 install scapy
				;;
			"\"sounddevice y soundfile\"")
				sudo apt install python3-pip -y
				pip3 install sounddevice soundfile
				;;
			"\"xlrd\"")
				sudo apt install python3-pip -y
				pip3 install xlrd
				;;
    *)
      echo "Opción no soportada!" >&2
      exit 1
      ;;
    esac
  done
else
  echo "No se selecteccionó alguna opción"
fi
rm instalador_python.sh
