# Direcciones IP

En esta sección se desarrollan los aspectos referidos a la configuración de direcciones IP a interfaces de un dispositivo Mikrotik.

Para más información, visitar en tutorial oficial: [https://wiki.mikrotik.com/wiki/Manual:IP/Address](https://wiki.mikrotik.com/wiki/Manual:IP/Address)

## Asignación de una dirección IP a una interfaz
```bash
/ip address add address="x.x.x.x"/"y" interface="z"
```

Donde: | | |
-- | -- | --
**x.x.x.x** = Dirección IP | **y** = tamaño (en decimal) de la máscara de red | **z** = nombre lógico de la interface


### Visualización de configuración IP
Ejecutar la opción print:

```bash
/ip address print
Flags: X ­ disabled, I ­ invalid, D ­ dynamic 
#     ADDRESS           NETWORK     BROADCAST      INTERFACE
"#"   "x.x.x.x"/"y"     "n"         "br"           "z"
```

Donde: | | |
-- | -- | --
***#** = número de línea | **x.x.x.x** = dirección IP | **y** = tamaño (en decimal) de la máscara de red 
**n** = dirección de red | **br** = dirección de broadcast | **z** = nombre lógico de la interface

## Eliminar direcciones IP asignadas a una interfaz
Ejemplo, eliminar la dirección **192.186.1.2/24** asignada a la interfaz **ether1**:

1º Visualizar direcciones IP configuradas

```bash
/ip address print

Flags: X ­ disabled, I ­ invalid, D ­ dynamic 
#  ADDRESS           NETWORK         BROADCAST        INTERFACE
0  192.168.1.2/24    192.168.1.0     192.168.1.255    ether1
1  192.168.10.4/24   192.168.10.0    192.168.10.255   ether2
```
Ver el número que se encuentra en la columna **#**, para la IP **192.168.1.2/24** el número es **0**.

2º Remover la dirección IP en función al valor obtenido en el paso anterior:
```bash
/ip address remove "0"
```

3º Corroborar que se eliminó la IP:
```bash
/ip address print

Flags: X ­ disabled, I ­ invalid, D ­ dynamic 
#  ADDRESS            NETWORK         BROADCAST        INTERFACE
0   192.168.10.4/24   192.168.10.0    192.168.10.255   ether2
```

Notar que en el paso 3º no se muestra la red 192.186.1.2/24

## Ejemplo

Visualizar ejemplo 1.1 [enlace](Ejemplos.md)
