# Pool de direcciones    
```bash
/ip pool add name="n" ranges="y.y.y.y"-"z.z.z.z"
```

| Donde: | |
| -- | -- |
| **n** = nombre del Pool | |
| **y.y.y.y** = dirección IP de inicio del pool | **z.z.z.z** = última dirección IP del rango del pool |
