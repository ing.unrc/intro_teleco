# PPP
## Configuración del equipo como cliente PPPoE
```bash
/interface pppoe-client add name="n" user="u" password="p" interface="y" disabled="yes/no"
```

Donde: | |
-- | --
**n** = Nombre asignado a la interfaz | **u** = Nombre de usuario PPPoE
**p** = Contraseña PPPoE | **y** = interfaz desde donde se realiza la conexión PPPoE

```bash
/interface pppoe-client print
```
## Configuración de servidor PPPoE
### Carga de perfil de cliente
```bash
/ppp profile add name="n" local-address="x.x.x.x" remote-address="y" dns-server="d.d.d.d" rate-limit="u" kbps/"d" kbps
```

Donde: | |
-- | --
**n** = Nombre del perfil | **x.x.x.x** = Dirección IP del servidor PPPoE 
**y** = Pool de direcciones reservado con anterioridad  | **d.d.d.d** = dirección IP del servidor DNS
**u** = Límite de velocidad de subida [kbps] | **d** = Límite de velocidad de bajada [kbps]

### Asignación del servicio a una interface

```bash
/interface pppoe-server server add service-name="x" interface="y" default-profile="z" disabled=no
```

Donde: | | |
-- | -- | --
**x** = Nombre del Servidor | **y** = Nombre de interfaz donde funciona el servidor | **z** = Nombre del perfil


### Carga de clientes PPPoE
```bash
/ppp secret add name="u" password="p" service="pppoe" profile="n"
```

Donde: | | |
-- | -- | --
**u** = nombre de usuario | **p** = contraseña | **n** = nombre del perfil


### Visualizar clientes cargados al sistema
```bash
/ppp secret print
Flags: X - disabled
#     NAME        SERVICE CALLER-ID     PASSWORD       PROFILE     REMOTE-ADDRESS
"#"   "usuario"   "servicio"            "contraseña"   "perfil"    "ip"    
```

Donde: | |
-- | -- 
**#** = número de línea  | **usuario** = nombre de usuario
**servicio** = nombre del servidor | **contraseña** = contraseña de usuario
**perfil** = perfil de usuario asignado | **ip** = dirección de IP estática asignada


### Visualizar funcionamiento
```bash
/log print
```
Mediante el log se pueden determinar problemas, por ejemplo si un usuario intenta identificarse y tiene un error en su contraseña.
