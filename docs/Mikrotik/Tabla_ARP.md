#Tabla ARP

En esta sección se desarrolla la visualización y carga estática de la tabla ARP. Para más información, visitar el sitio web en el [enlace](https://wiki.mikrotik.com/wiki/Manual:IP/ARP).

## Carga manual de una dirección MAC a la tabla ARP
```bash
/ip arp add mac-address="xx:xx:xx:xx:xx:xx"  address="x.x.x.x"  interface="n"
```

Donde: | |
-- | --
**xx:xx:xx:xx:xx:xx** = dirección mac del dispositivo | **x.x.x.x** = dirección IP asociada 
**n** = nombre de la interfaz |

## Visualización la tabla ARP
```bash
/ip arp print
Flags: X - disabled, I - invalid, H - DHCP, D - dynamic, P - published, C - complete
 #    ADDRESS      MAC-ADDRESS           INTERFACE     
"#"   "x.x.x.x"    "xx:xx:xx:xx:xx:xx"   "n"
```

Donde: | |
-- | -- 
**#** = número de línea | **xx:xx:xx:xx:xx:xx** = dirección mac del dispositivo
**x.x.x.x** = dirección IP asociada | **n** = nombre de la interfaz
