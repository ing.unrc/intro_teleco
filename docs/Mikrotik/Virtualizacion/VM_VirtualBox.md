# Virtualización - Máquina virtual en VirtualBox

Previo a la instalación de una máquina virtual es necesario instalar Virtual Box.

Sitio oficial de [Virtual Box](https://www.virtualbox.org/)

Tutorial de uso de VirtualBox [enlace](https://labredes.gitlab.io/virtualbox/)

## Instalación de VirtualBox

Para realizar la instalación, se pueden seguir los pasos indicados en el siguiente [enlace](https://labredes.gitlab.io/virtualbox/Instalacion_VirtualBox/)

## Descarga de Máquina Virtual

1- Acceder a la sección de descarga de Software de Mikrotik [link](https://mikrotik.com/download)

2- Desplazarse a la sección "Cloud Hosted Router" y descargar la opción correspondiente (se recomienda "OVA template" y la versión "long-term"):

![](imagenes/software_ova.jpg)

La plantilla "OVA template" incluye todos los archivos necesarios y la configuración recomendada para la instalación de la Máquina Virtual.

3- Si se ha descargado "OVA template", ejecutar el archivo, con lo que se obtiene una ventana de este tipo:

![](imagenes/ova.jpg)

### Opciones de descarga

* "VHDX", "VMDK", "VDI": archivos de discos rígidos virtuales que cuentan con RouterOS instalado.

* "OVA template": archivo de configuración, que incluye todos los parámetros de la máquina virtual como también el disco rígido virtual asociado.

* "Raw disk": imagen de instalador de RouterOS, puede ejecutarse al instalar desde cero una Máquina Virtual o en una computadora física

* "Extra packages": packetes con funcionalidades extras a la instalación básica de RouterOS.

* "The Dude": software para análisis del funcionamiento de una red.
