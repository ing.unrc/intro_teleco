# Multicast

## Configuración de Bridge

IGMP Snooping which controls multicast streams and prevents multicast flooding is implemented in RouterOS starting from version 6.41.
It's settings are placed in bridge menu and it works independently in every bridge interface.
Software driven implementation works on all devices with RouterOS but CRS1xx/2xx/3xx series switches also support IGMP Snooping with hardware offloading.

```bash
/interface bridge add name="NombreBridge" igmp-snooping="HabilitarIGMP"
```

Donde: | |
-- | -- |
**NombreBridge** = Nombre asignado al bridge | **HabilitarIGMP** = **yes**/**no** (habilitar o no) |

## Configuración de puertos

### Monitoring multicast groups in the Bridge Multicast Database

```bash
[admin@MikroTik] > interface bridge mdb print 
BRIDGE                   VID GROUP                                              PORTS           
bridge1                  200 229.1.1.2                                          ether3          
                                                                                ether2          
                                                                                ether1          
bridge1                  300 231.1.3.3                                          ether4          
                                                                                ether3          
                                                                                ether2          
bridge1                  400 229.10.10.4                                        ether4          
                                                                                ether3          
bridge1                  500 234.5.1.5                                          ether5          
                                                                                ether1          
```

## Configuración de unknown-multicast-flood

When enabled, bridge floods unknown multicast traffic to all bridge egress ports.

When disabled, drops unknown multicast traffic on egress ports.

Multicast addresses that are in /interface bridge mdb are considered as learned multicasts and therefore will not be flooded to all ports.

Without IGMP Snooping all multicast traffic will be dropped on egress ports.

Has effect only on an egress port. This option does not limit traffic flood to the CPU.

Note that local multicast addresses (224.0.0.0/24) are not flooded when unknown-multicast-flood is disabled, as a result some protocols that rely on local multicast addresses might not work properly, such protocols are RIPv2m OSPF, mDNS, VRRP and others. Some protocols do send a IGMP join request and therefore are compatible with IGMP Snooping, some OSPF implementations are compatible with RFC1584, RouterOS OSPF implementation is not compatible with IGMP Snooping.
This property should only be used when igmp-snooping is set to yes.

### Con Inundación

```bash
/interface bridge port add bridge="NombreBridge" interface="NombreInterfaz" unknown-multicast-flood = "yes"
```

Donde: | |
-- | -- |
**NombreBridge** = Bridge al que se quiere asignar el puerto | **NombreInterfaz** = Nombre lógico de la interface |
**HabilitarInundacion** = **yes** -> habilita la inundación | |

### Sin Inundación

```bash
/interface bridge port add bridge="NombreBridge" interface="NombreInterfaz" unknown-multicast-flood = "no"
```

Donde: | |
-- | -- |
**NombreBridge** = Bridge al que se quiere asignar el puerto | **NombreInterfaz** = Nombre lógico de la interface |
**HabilitarInundacion** = **no** -> deshabilita la inundación | |
