# Bridge

## Crear bridge

A partir del siguiente comando, se crea y especifica el nombre de un nuevo bridge:

```bash
/interface bridge add name="NombreBridge"
```

Donde: | |
-- | --
**NombreBridge** = nombre asociado al bridge

## Asociar interfaces a un bridge

Una vez creado un bridge, se le puede asignar interfaces de la siguiente forma:

```
/interface bridge port add bridge="NombreBridge" interface="Interfaz"
```

Donde: | |
-- | --
**NombreBridge** = nombre del bridge a configurar |**Interfaz** = Nombre de los puertos que se adicionan al Bridge. |
