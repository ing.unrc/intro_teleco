# Asignación de puertos a VLANs en Bridges

Una vez creado un Bridge, como se presenta en [enlace](../Bridge), es posible configurar puertos en modo troncal, de acceso o híbridos.

```mikrotik
# Creación de Bride:
/interface bridge add ingress-filtering=no name="NombreBridge" vlan-filtering="Filtro"

# Asignación de puertos troncales:
/interface bridge port add bridge="NombreBridge" interface="Interfaz"

# Asignación de puertos modo acceso:
/interface bridge port add bridge="NombreBridge" interface="Interfaz" pvid="#v"

# Asignación de puertos a una VLAN:
/interface bridge vlan add bridge="NombreBridge" tagged="Interfaces" untagged="interfaces" vlan-ids="#v"
```

Donde: | |
-- | -- |
**NombreBridge** = nombre del bridge a configurar | **Filtro** = [**yes**/**no**] habilita el funcionamiento de etiquetado de VLANs. |
**Interfaz** = Nombre de los puertos que se adicionan al Bridge. | **Interfaces**  = nombre de la/s interfaz/ces que se asignan. Si es más de una, se separan por "," (comas). |
**#v** = número de vlan ||

Durante la asignación de puertos a VLANs, es necesario contemplar que si hay puertos troncales y de acceso en un mismo Bridge, puede ser necesario ingresar el nombre del Bidge como una interfaz donde se etiquetan las vlans en cuestión.
