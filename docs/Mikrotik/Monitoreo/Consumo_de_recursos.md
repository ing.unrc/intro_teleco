# Consumo de recursos

## Análisis de recursos

### Visualización en Winbox

Dentro de ** system > resource** se accede a un listado resumido de las características del dispositivo y el consumo de recursos:

![](../imagenes/Monitoreo_Consumo_de_recursos.png)

### Visualización por consola

#### resource

Lista el estado de los principales recursos del dispositivo en el instante de tiempo que se solicita:

```
/system resource print
```

##### resource > cpu

Muestra el estado de consumo de cada núcleo del cpu del sistema en el instante de tiempo que se solicita:

```
/system resource cpu print
```

##### resource > monitor

```
/system resource monitor
```

## Detalle de CPU

### Visualización en Winbox

Dentro de **Tools > Profile** es posible conocer el consumo de cada CPU del equipo y a grandes rasgos, la distribución de procesamiento entre las aplicaciones:

![](../imagenes/Monitoreo_Consumo_de_recursos_2.png)

### Visualización por consola

Muestrar el consumo de cpu por tipo de aplicación, ya sea en todos los nucleos o en un núcleo en particular:

```
/tool profile cpu=all
```
