# Captura de paquetes

Con el fin de simplificar esta explicación, la captura de paquetes se explica mediante Winbox, a pesar de ello, es posible utilizar la herramienta desde consola.

## Configuración General

La herramienta de captura de datos se encuentra en la sección: **Tools > Packet Sniffer**. Para poner en funcionamiento el programa, es necesario aplicar los cambios en la configuración. Se permite limitar el consumo de memoria, limitar el tamaño de cada trama/paquete al conjunto de encabezados, sobre escribir el archivo al momento que llegue al tamaño máximo, asignar un nombre al archivo de salida y limitar tamaño máximo del archivo resultante, en kb:

![](../imagenes/Monitoreo_Captura_1.png)

una vez configurados todos los parámetros, es necesario aplicar los cambios para luego:

**Start** -> inicia la captura
** Stop** -> finaliza la captura

En el margen inferior izquierdo se muestra el estado stopped/starting.

**Importante:** cerrar el programa no corta la captura, por lo que es necesario presionar el boton "Stop" para finalizar su funcionamiento

## Filtrado

Como se muestra en la siguiente figura, se puede filtrar el contenido del tráfico capturado según interfaces, direcciones MAC, protocolos en capa 2, direcciones IP, protocolos en capa 3 y puertos, procesamiento según CPU, sentido del flujo de datos:

![](../imagenes/Monitoreo_Captura_2.png)
