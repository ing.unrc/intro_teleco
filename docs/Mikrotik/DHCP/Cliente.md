# Cliente DHCP

Tutorial oficial: [https://wiki.mikrotik.com/wiki/Manual:IP/DHCP_Client](https://wiki.mikrotik.com/wiki/Manual:IP/DHCP_Client)

## Configuración de interfaz cliente

La implementación como cliente DHCP se realiza indicando por cuál interfaz se ha de solicitar la configuración:

```bash
/ip dhcp-client add interface="n"
```

Donde: | | |
-- | -- | --
**n** = nombre de la interfaz

## Visualización de configuración obtenida
```bash
/ip dhcp-client print
Flags: X - disabled, I - invalid, D - dynamic

#     INTERFACE     USE     ADD-DEFAULT-ROUTE     STATUS          ADDRESS           
"#"   "interfaz"    yes     yes                   "searching…"     
"#"   "interfaz"    yes     yes                   "bound"         "x.x.x.x/y"
```

Donde: | |
-- | -- 
**#** = línea de la visualización | **bound** = ya cuenta con configuración desde el DHCP
**interfaz** = nombre de interfaz lógica donde se implementa el cliente dhcp | **x.x.x.x/y** = Dirección IP y máscara de red asignada
**searching...** = buscando servidor DHCP
