# Servidor DHCP

La implementación de un servidor DHPC en mikrotik se realiza indicando la red (direción y máscara), puerta de enlace y servidor/es dns, seguido de la interfaz y el pool de direcciones que se ha de utilizar. Para crear un pool de direcciones, se pueden ver los pasos en el siguiente [enlace](../Pool_de_direcciones)

Tutorial oficial: [https://wiki.mikrotik.com/wiki/Manual:IP/DHCP_Server](https://wiki.mikrotik.com/wiki/Manual:IP/DHCP_Server)

### Parámetros de red

Como se indicó con anterioridad, en este paso se ha de indicar los parámetros que hacen a la red:

```bash
/ip dhcp-server network add address="x.x.x.x"/"y" gateway="z.z.z.z" dns-server="d.d.d.d" name="n" disable="yes/no"
```

Donde: | |
-- | --
**x.x.x.x** = dirección de la red | **y** = tamaño (en decimal) de la máscara de red
**z.z.z.z** = dirección IP de la puerta de enlace | **d.d.d.d** = dirección IP de la puerta del servidor DN
**n** = nombre asignado al servidor


### Selección de interfaz y pool de direcciones

Para ejecutar el siguiente comando, es necesario haber configurado con anterioridad un pool de direcciones.

```bash
/ip dhcp-server add interface="n" address-pool="d"
```

Donde: | |
-- | --  
**n** = nombre de la interfaz | **d** = nombre de un pool de direcciones IP


## Visualización de clientes conectados

```bash
/ip dhcp-server lease print
```
