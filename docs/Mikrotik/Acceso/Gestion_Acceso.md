# Gestión de opciones de acceso
## Visualización de modos de acceso
```bash
/ip service print
Flags: X - disabled, I - invalid
 #      NAME         PORT     ADDRESS              CERTIFICATE   
 0      telnet       23
 1      ftp          21
 2      www          80
 3  XI  www-ssl      443                           none          
 4      api          8728
 5      winbox       8291
 6      api-ssl      8729
 #  XI  servicio     puerto     DireccionIP     certificado  
```

Donde: | | |
-- | -- | --
**#** = número de línea | **XI** = habilitado/deshabilitado - configuración válida o no | **servicio** = tipo de conexión
**puerto** = puerto en capa 4 | **DireccionIP** = Direcciones IP permitidas | **certificado** = certificado de conexión

## Editación modos de acceso
```
/ip service set "servicio" "opción"
```

Donde: | |
-- | --
**servicio** = tipo de conexión | **opción** = address/certificate/disabled/port

Se recomienda por seguridad deshabilitar el acceso por telnet:
```bash
/ip service set telnet disabled=yes
```
## Gestión de usuarios
```bash
/user print
Flags: X - disabled
 #     NAME        GROUP        ADDRESS     LAST-LOGGED-IN      
"#"    "usuario"   "permisos"               "fecha"
```

Donde: | |
-- | --
**#** = número de línea | **usuario** = nombre de usuario
**permisos** = permisos de usuario: full/read/write | **fecha** = fecha y hora del último acceso
