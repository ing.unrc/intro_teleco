# Winbox

Winbox es un software con entorno gráfico, que representa cada sección de configuración por medio de ventanas.

Está desarrollado para Windows. En caso de utilizarse Linux es necesario ejecutarlo con Wine.

## Instalación de Wine (Linux)

Wine es un software que permite la ejecución de programas desarrollados para windows en linux. La instalación puede hacerse desde consola de la siguiente forma:

```
sudo apt update
sudo apt install wine64 wine32
```

## Descarga de Winbox

Acceder a la sección de descarga de Software de Mikrotik [link](https://mikrotik.com/download)

![](imagenes/Descargar_winbox.jpg)

### Ejecución en Linux

#### Desde consola

Una vez descargado winbox, acceder a la carpeta a donde se encuentra el archivo y ejecutar:

Para winbox de 64 bits:

```
wine ./winbox64.exe
```

Para winbox de 32 bits:

```
wine ./winbox.exe
```

#### Modo gráfico

Una vez descargado winbox, hacer click con el botón derecho y seleccionar la opción "Abrir con otra aplicación":

![](imagenes/click_wine.jpg)

Si wine no se encuentra dentro del listado de programas disponibles, ingresarlo de la forma en que se indica en la figura. A su vez, indicar que sea predeterminado para este tipo de archivos:

![](imagenes/seleccionar_wine.jpg)

