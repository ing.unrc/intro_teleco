# NAT

Para más información, visitar en tutorial oficial: [https://wiki.mikrotik.com/wiki/Manual:IP/Firewall/NAT](https://wiki.mikrotik.com/wiki/Manual:IP/Firewall/NAT)

## Enmascaramiento

En los siguientes pasos, se muestran opciones para enmascarar la dirección IP de origen de los paquetes salientes de un router en función a una dirección IP disponible en la interfaz por la que sale el flujo de datos:

### Enmascaramiento genérico

```bash
/ip firewall nat add chain="srcnat" action="masquerade"
```

Donde: | |
-- | -- 
**srcnat** = source nat | **masquerade** = interface de salida

### Enmascaramiento tráfico saliente por una interfaz

A diferencia del caso anterior, al indicar "out-interface", en el siguiente modo de configuración sólo se enmascaran los paquetes que salen por dicha interfaz:

```bash
/ip firewall nat add chain="srcnat" action="masquerade" out-interface="x"
```

Donde: | | |
-- | -- | --
**srcnat** = source nat | **masquerade** = interface de salida | **x** = interface de salida

Ejemplo: trasladar las direcciones IP de origen de los paquetes que salgan por la interfaz ether1 a una dirección IP de dicha interfaz:

```bash
/ip firewall nat add chain=srcnat action=masquerade out-interface=ether1
```



## Visualización de tabla NAT
```
/ip firewall connection print
Flags: E - expected, S - seen-reply, A - assured, C - confirmed, D - dying,
F - fasttrack, s - srcnat, d - dstnat
 #     PR..          SRC-ADDRESS     DST-ADDRESS      TCP-STATE     
"#"    "protocolo"   "x.x.x.x:po"    "y.y.y.y":"pd"
```

Donde: | | |
-- | -- | --
**#** = número de línea | **x.x.x.x** = dirección IP de origen | **po** = puerto de origen
**protocolo** = protocolo | **y.y.y.y** = dirección IP de destino | **pd** = puerto destino
