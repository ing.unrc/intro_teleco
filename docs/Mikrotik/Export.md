# Visualizar / Exportar configuración

## Visualizar

Mediante el comando **print** se puede visualizar la configuración dentro de una sección de configuración. A pesar de ello, puede resultar de interés conocer la configuración completa del dispositivo.

Para ello existe el comando **export**, a partir del cual se presenta toda la configuración en forma de comandos (salvo información crítica, como contraseñas de usuarios).

## Exportar
