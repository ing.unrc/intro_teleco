# Estructura de comandos

RouterOS presenta una estructura de comandos en forma de árbol. 

La raiz se simboliza con **/** y es el inicio de todo comando, siendo cada rama una sección de la configuración.

Se puede cargar un comando en forma absoluta, comenzando por **/** o en forma relativa, en función a la posición actual dentro del árbol.

Desde una rama, se puede volver a la rama anterior mediante **..** o a la raíz con **/**.

Ante cualquier duda, presionando **tab** una o dos veces, se autocompletará el comando que se esté ingresando o prensentarán las opciones en pantalla de qué se puede hacer.

Para visualizar la configuración del equipo, se debe ingresar el comando **print** dentro de la rama que se quiere analizar, por ejemplo si se quiere ver las interfaces y el estado en que se encuentra, se utiliza el comando: **/interface print**.

## Ramas de comandos básicos

A continuación se presenta la estructura básica de comandos desarrollados en este tutorial:

| Sección | Descripción |
| -- | -- |
**/**                          | Raíz
/**interface**                | Rama de configuración de interfaces físicas y virtuales
/interface **bridge**         | Gestión de bridges entre interfaces
/interface **pppoe-client**   | Configuración de interfaz para cliente PPPoE
/interface **pppoe-server**   | Configuración de interfaz para servidor PPPoE
/**ip**                       |  Rama de configuración de IPv4
/ip **address**               | Gestión de direcciones IP
/ip **arp**                   | Tabla ARP
/ip **dhcp-client**           | Selección de interfaz como cliente DHCP
/ip **dhcp-server**           | Parámetros para ofrecer DHCP
/ip **dns**                   | Servidores DNS
/ip **firewall**
/ip firewall **nat**          | Configuración de NAT
/ip **pool**                  | Reserva de direcciones IP (ej: para servidor DHCP o PPP)
/ip **route**                 | Tabla de rutas
/ip **service**               | Gestión de los modos de acceso
/**ppp**                      | Gestión de parámetros para ofrecer PPP
/**log**                      | Visualización del funcionamiento del equipo / errores
