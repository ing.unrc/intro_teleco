# Tabla de rutas

En esta sección se desarrolla cómo configurar una puerta de enlace y cargar las entradas en una tabla de rutas. Para más información sobre ruteo estático, visitar el sitio oficial en el siguiente [enlace](https://wiki.mikrotik.com/wiki/Manual:Simple_Static_Routing).

## Configuración de puerta de enlace
```bash
/ip route add dst-­address=0.0.0.0/0 gateway="z.z.z.z"
```

Donde: | |
-- | -- 
**0.0.0.0/0** = son todas las redes desconocidas | **z.z.z.z** = dirección IP de la puerta de enlace / Gateway

## Agregado de rutas en forma estática
``` bash
/ip route add dst-­address="x.x.x.x"/"y" gateway="z.z.z.z"
```

Donde: | | |
-- | -- | --
**x.x.x.x** = red a alcanzar | **y** = tamaño (decimal) de la máscara de red | **z.z.z.z** = dirección IP del próximo salto


## Visualización tabla de rutas
Se ingresa a la rama de rutas en IP y se ejecuta el comando print:
```bash
/ip route print
Flags: X - disabled, A - active, D - dynamic,
C - connect, S - static, r - rip, b - bgp, o - ospf, m - mme,
B - blackhole, U - unreachable, P - prohibit
#                  DST-ADDRESS     PREF-SRC     GATEWAY     DISTANCE
"#"  "banderas"    "x.x.x.x/y"                  "z.z.z.z"   "distancia"
```

Donde: | | |
-- | -- | --
**#** = número de línea | **banderas** = Forma en que se configuró la red | **x.x.x.x/y** = Dirección IP y máscara
**z.z.z.z** = Próximo salto | **distancia** = Cantidad de saltos hasta el destino
