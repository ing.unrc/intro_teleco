# PPP - cliente PPPoE

## Visualización de configuración

```bash
/interface pppoe-client print
```

## Configuración de cliente

Para la configuración de un cliente pppoe es necesario ingresar el nombre de usuario, la contraseña y seleccionar la interfaz a partir de la cual se conecta al servidor:

```bash
/interface pppoe-client add name="n" user="u" password="p" interface="y" disabled="yes/no"
```

Donde: | |
-- | --
**n** = Nombre asignado a la conexión | **u** = Nombre de usuario PPPoE
**p** = Contraseña PPPoE | **y** = interfaz desde donde se realiza la conexión PPPoE
