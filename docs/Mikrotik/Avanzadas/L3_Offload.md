# L3 Hardware Offload

Funcionalidades disponibles: https://wiki.mikrotik.com/wiki/Manual:CRS3xx_series_switches

## Configuración en un Switch

Para habilitar **Layer 3 Hardware Offloading**, configurar l3-hw-offloading=yes en el switch:

```
/interface/ethernet/switch set 0 l3-hw-offloading=yes
```

## Configuración de Fasttrack

```
/ip firewall filter add chain=forward action=fasttrack-connection connection-state=established,related
/ip firewall filter add chain=forward action=accept connection-state=established,related
```
