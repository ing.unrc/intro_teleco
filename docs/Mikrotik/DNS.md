# DNS

En esta sección se presenta la configuración del servicio de DNS. 

Para más información, visitar en tutorial oficial: [https://wiki.mikrotik.com/wiki/Manual:IP/DNS](https://wiki.mikrotik.com/wiki/Manual:IP/DNS)

## Configuración de servidor DNS

Para indicar un servidor DNS donde hacer consultas, implementar el siguiente comando:

```bash
/ip dns set servers="d.d.d.d"
```

Donde: | |
-- | -- |
**d.d.d.d** = dirección IP del servidor DNS | |

## Visualización de configuración DNS

Para conocer la configuración DNS, implementar el siguiente comando:

```bash
/ip dns print
```
