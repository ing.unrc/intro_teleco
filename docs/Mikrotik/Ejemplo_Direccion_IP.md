# Ejemplo 1 - Configuración Básica

Considerando el siguiente esquema:

![](imagenes/Diagrama1.png)

Configurar el equipo Mikrotik para que tenga acceso a internet.

## 1.1 Direcciones IP

<iframe width="728" height="410" src="https://www.youtube.com/embed/LIz9GQ-_CDQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## 1.2 Puerta de enlace

<iframe width="728" height="410" src="https://www.youtube.com/embed/cNSJF7zos-Q" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## 1.3 DNS

<iframe width="728" height="410" src="https://www.youtube.com/embed/_t6w6-3kkCE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
