## No se puede ejecutar "add-apt-repository"

En caso que el comando no se encuentre disponible, instalar el siguiente paquete:

```bash
sudo apt-get install software-properties-common
```

## gnome-terminal

```bash
sudo apt install gnome-terminal
```
## vncviewer

**Error: **

```bash
Cannot start console application: [Errrno 2] No cuch file or directory: 'vncviewer':'vncviewer'
```

Alternativas a instalar:

### tigervnc-viewer
```bash
sudo apt install tigervnc-viewer
```

### xtightvncviewer
```bash
sudo apt install xtightvncviewer
```

## KVM Error

**Error: **

```bash
Execution log:
Could not access KVM kernel module: Permission denied
qemu-system-x86_64: failed to initialize KVM: Permission denied
```

Corrección:

```bash
sudo chown "user":"user" /dev/kvm
```

|Donde: ||
| :-: | :-: |
**user** = your computer user | **KVM** = Kernel-based Virtual Machine

## Creación de enlaces - uBridge

**Error: **

```bash
Error while creating link: uBridge is not available, path doesn't exist, or you just installed GNS3 and need to restart your user session to refresh user permissions.
```

Solución, cambiar permisos de ejecución a uBridge:

```
sudo chmod +x /usr/bin/ubridge
```

## Error al abrir Wireshark

**Error: **

```bash
Couldn't run /usr/bin/dumpcap in child process. Permiso denegado
```

Solución, cambiar permisos de ejecución a dumpcap:

```
sudo chmod +x /usr/bin/dumpcap
```
