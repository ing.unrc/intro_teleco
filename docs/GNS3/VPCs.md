# VPCS

VPCS son computadoras virtuales disponibles para hacer pruebas básicas, como son: configuración IP, ping y trace.

## Visualización de configuración

Comando | Resultado
-- | --
show **ip** | Muestra las direcciones IP configuradas
show **arp** | Muestra la tabla ARP

## Configuración estática

### Dirección IP y puerta de enlace

Con el siguiente comando se asigna una dirección IP, máscara y puerta de enlace:

```script
ip **"DirIP"** / **"mask"** **"gateway"
```

Donde: | |
-- | --|
**DirIP** = Dirección IP | "mask" = Máscara de red en formato decimal |
"gateway" = Puerta de enlace|

### DNS
El siguiente comando permite la carga de un servidor DNS

```script
ip dns "IPdns"
```

Donde: |
-- | 
 **IPdns** = Dirección IP del servidor dns |

## DHCP

Comando | Resultado
-- | --
dhcp | Solicitud de configuración por DHCP
dhcp **-d**| Visualización de DORA
dhcp **-r** | Renovación DHCP

## trace

Este comando traza la ruta hasta la dirección IP de destino:

```script
trace "DestIP" -P "proto" -m "ttl"
```

Donde: ||
-- | --
**DestIP** = Dirección IP destino a la que se quiere conocer la ruta | |
**proto** = protocolo para el envío de los mensajes: 1 - icmp, 17 - udp (default), 6 - tcp | |
**ttl** = máximo valore de ttl (cantidad de saltos al destino) - por defecto 8 | |
