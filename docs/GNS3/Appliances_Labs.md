# Appliances y Labs

## Appliances

En la sección [https://gns3.com/marketplace/appliances](https://gns3.com/marketplace/appliances)

se presentan varios tipos de dispositivos que pueden ser incorporados a gns3. 

Cada archivo representa la configuración completa para llevar a cabo la integración de emuladores de múltiples equipos.

Dentro de algunos elementos, se encuentran:

* Cisco

* Mikrotik

* Juniper

* Linux

* Windows

![](imagenes/Appliances.jpg)

## Labs

En la sección: [https://gns3.com/marketplace/labs](https://gns3.com/marketplace/labs) se puede acceder a un conjunto de laboratorios pensados para realizar prácticas para el estudio y análisis de protocolos y configuraciones de red.

![](imagenes/Labs.jpg)
