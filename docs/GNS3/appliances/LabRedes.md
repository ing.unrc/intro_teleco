# LabRedes

A continuación se listan appliances desarrolladas para actividades en el Laboratorio de Redes.

## Ubuntu net-tools

Al descargar la [appliance](ubuntu-net-tools.gns3a), se instala un contenedor Ubuntu que se descarga automáticamente del hub de docker del LabRedes.

* Es necesario tener instalado docker [enlace](https://labredes.gitlab.io/docker/Docker_Instalacion/).

## Ubuntu iproute2

La [appliance](ubuntu-iproute2.gns3a) cuenta con un contenedor Ubuntu, el cual se obtiene desde el hub de docker de LabRedes.

* Es necesario tener instalado docker [enlace](https://labredes.gitlab.io/docker/Docker_Instalacion/).
