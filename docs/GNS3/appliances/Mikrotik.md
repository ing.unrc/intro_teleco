# Instalación de appliance Mikrotik:

1) Descargar la appliance: <a href="https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router" target="_blank">enlace</a>

2) Descargar de la sección "Cloud Hosted Router" el archivo tipo "Raw disk image" con la version de RouterOS deseada: <a href="https://mikrotik.com/download" target="_blank">enlace</a>

![](imagenes/Mikrotik_image.png)

3) En caso que la imagen esté comprimida, descomprimirla.

4) Importar la appliance en el menú: "File > Import appliance"

5) Seleccionar dónde se va a alojar el dispositivo virtual. Por defecto dentro del la misma PC local:

![](imagenes/ImportMikrotik1.png)

6) Aceptar el tipo de emulación de dispositivo que propone la appliance por defecto:

![](imagenes/ImportMikrotik2.png)

7) Seleccionar la versión de Mikrotik que se ha de instalar. Si ninguna coincide con la versión descargada en el punto 2, se puede puede generar un nuevo nombre y luego se debe importar la imagen, cuyo resultado queda de la forma:

![](imagenes/ImportMikrotik3.png)

8) Continuar los pasos hasta finalizar la importación.
