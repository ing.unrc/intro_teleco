#Modos de Operación

## Modo Tiempo Real

![](imagenes/Realtime.jpg)

Al armar un esquema de red este se ejecuta en un modelo de tiempo real, dentro de los límites del protocolo en los modelos utilizados. 

La red responde a sus acciones inmediatamente, por ejemplo, al hacer una conexión Ethernet, el led de la conexión aparecerá, mostrando el estado de la misma. 

Cada vez que escriba un comando en la CLI (como el ping o show), el resultado o la respuesta se genera en tiempo real y se ve como tal.

## Modo Simulación

![](imagenes/Simulacion.jpg)

Este modo de operación permite:

* "congelar" el tiempo - control del tiempo en relación con el flujo de paquetes. 

* simular la red paso a paso, o evento a evento. 

* configurar escenarios, ejemplo envío de un paquete ping de un dispositivo a otro. 

* detener la simulación, dar paso hacia adelante o hacia atrás en el tiempo, para ver información específica sobre los paquetes y dispositivos en un momento específico. 

![](imagenes/simulacion_2.jpg)

### Análisis de tramas

#### Panel de control

![](imagenes/simulacion_3.jpg)

#### Captura de mensajes

![](imagenes/simulacion_4.jpg)
