# PPPoE

La siguiente práctica permite analizar el funcionamiento de PPPoE en equipamiento Linux y Mikrotik.

## Objetivos

* Implementación de los siguientes elementos:

    * servidor PPPoE y DHCP (el último ya está configurado) en un equipo Mikrotik.

    * cliente PPPoE en Linux.

    * cliente PPPoE en Mikrotik.

    * cliente DHCP en Linux (ya configurado).
    
* Análisis de encapsulación de protocolo PPPoE.

* Configuración de distintos parámetros en perfiles PPPoE.

* Visualización de encolado para perfiles PPPoE.

## Diagrama de red

<div align="center"> <img src="https://labredes.gitlab.io/labs/PPPoE/PPPoE.png"></div>

<div align="center"> Figura 1 - Arquitectura a implementar </div>

## Prerequisitos

Para la implementación de este laboratorio se requieren los siguientes programas:

Docker [enlace de instalación](https://labredes.gitlab.io/docker/Docker_Instalacion/)

GNS3 [enlace de instalación](https://labredes.gitlab.io/gns3/Instalacion/)

## Proyecto GNS3

El Proyecto que incluye todos los archivos para la implementación de la arquitectura se encuentra disponible en el siguiente [enlace](https://labredes.gitlab.io/labs/PPPoE/PPPoE.gns3project)

Los dispositivos cuentan con la siguiente configuración:

### Servidor PPPoE Mikrotik

Pre-configuración:

```console
# Configuración de nombre de equipo:
/system identity set name=MKT-PPPoE-Server

# Cliente DHCP interfaz ether1
/ip dhcp-client add disabled=no interface=ether1

# Direccion IP interfaz ether2:
/ip address add address=192.168.88.1/24 interface=ether2 network=192.168.88.0

# Pool de direcciones:
/ip pool add name=PoolDHCP ranges=192.168.88.100-192.168.88.199

# Servidor DHCP interfaz ether 2:
/ip dhcp-server network add address=192.168.88.0/24 dns-server=192.168.88.1,8.8.8.8 gateway=192.168.88.1
/ip dhcp-server add address-pool=PoolDHCP disabled=no interface=ether2 name=DHCP-server

# NAT
/ip firewall nat add action=masquerade chain=srcnat
```

**El dispositivo no cuenta con configuración PPPoE**

### Clientes PPPoE 

#### Linux

El cliente se encuentra implementado en un contenedor linux, por lo que es necesario contar con docker.

Pre-configuración:

Contenido del archivo **/etc/ppp/peers/dsl-provider**:

```console
user "labredes"
plugin rp-pppoe.so eth0
noipdefault
usepeerdns
defaultroute
persist
noauth
```

Contenido del archivo **/etc/ppp/cat chap-secrets **:

```console
# Secrets for authentication using CHAP
# client	server	secret			IP addresses
"labredes"  *       "labredes"
```

La configuración que se muestra en los archivos, se corresponde con el [tutorial](https://labredes.gitlab.io/linux/PPP/Cliente/)

#### Mikrotik

Pre-configuración:

```console
# Configuración de nombre de equipo:
system identity set name=MKT-Cliente-PPPoE
```

**Este dispositivo no cuenta con configuración PPPoE.**

### Cliente DHCP

Pre-configuración:

Contenido del archivo **/etc/network/interfaces**:

```console
# DHCP config for eth0
auto eth0
iface eth0 inet dhcp
```

## Ejercicios

1) Planificar todos los aspectos faltantes para lograr el servidor PPPoE en función a los parámetros configurados en el cliente Linux y los necesarios para nuevos clientes.

2) Configurar el servidor PPPoE en el equipo Mikrotik1 [tutorial](https://labredes.gitlab.io/mikrotik/PPP/servidor/).

3) Configurar el cliente PPPoE en el equipo Mikrotik2 [tutorial](https://labredes.gitlab.io/mikrotik/PPP/Cliente/).

4) Comprobar que se establezcan las conexiones PPPoE desde ambos clientes al servidor.

5) Capturar y analizar el encapsulado PPPoE.

6) Configurar el profile PPPoE de forma tal de limitar la velocidad de los usuarios y analizar las queues que se generan en el equipo Mikrotik1 [tutorial](https://labredes.gitlab.io/mikrotik/PPP/servidor/).
