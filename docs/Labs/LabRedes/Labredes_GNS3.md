# LabRedes GNS3

La implementación de red que se muestra en la Figura 1 contiene un equipo Mikrotik configurado en forma equivalente al que se encuentra en el laboratorio de redes. A su vez, cuenta con un switch para la interconexión de otros dispositivos y salida a internet por la interfaz ether1 del router.

<div align="center"> <img src="https://labredes.gitlab.io/tutoriales/Labs/LabRedes/GNS3_LabRedes.png"></div>

<div align="center"> Figura 1 - Diagrama de red </div>

Elementos:

Nombre | Descripción |
-- | -- |
**LabRedes** | Enrutador con configuración equivalente a la del laboratorio de redes |
**Internet (Nube)** | Nodo que conecta con la PC anfitriona y todas las redes a las que tiene acceso |

## LabRedes

El enrutador **LabRedes** cuenta con una configuración equivalente al router que se encuentra en el **Rack 1** del Laboratorio de Redes.

## Internet (Nube)

El ícono que representa internet implementa la herramienta "NAT" que contiene GNS3, la cual conecta el entorno emulado a la PC anfitriona por medio de una interfaz virtual, la cual tiene la dirección IP 192.168.122.1/24 y cuenta con un servidor de DHCP.

Descarga del proyecto: [enlace](../LabRedesGNS3.gns3project)
