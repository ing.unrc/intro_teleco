# Configuración básica de red en múltiples Sistemas Operativos

El siguiente proyecto en GNS3 plantea la conexión y configuración de equipos Linux, Cisco y Mikrotik en forma equivalente a la implementación real en el Laboratorio de Redes. Para ello, el enrutador "LabRedes" presenta una configuración como en la red física cableada.

<div align="center"> <img src="https://labredes.gitlab.io/labs/Configuracion_Basica/Diagrama_conexion.png"></div>

<div align="center"> Figura 1 - Diagrama de red </div>

Tener presente que el lado derecho se corresponde con la implementación ofrecida en el proyecto de [implementación de LabRedes en GNS3](../../LabRedes/GNS3/Labredes_GNS3/).

## Objetivos

* Relevar el equipamiento y conexión que presenta el proyecto.

* Planificar los parámetros de red necesarios para cada equipo.

* Configurar los equipos Linux, Cisco y Mikrotik.

* Comprobar el correcto funcionamiento entre los dispositivos e internet.

## Actividades

1) Descagar el proyecto GNS3 desde el [enlace](../Practica_Configuracion_IP_Multiple_OS.gns3project)

2) Realizar un diagrama de red con el **relevamiento** de toda la información necesaria de cada equipo.

3) Completar el diagrama de red con la **planificación** de los parámetros faltantes.

4) **Configurar** los equipos: PC1, PC2, Cisco y Mikrotik.

5) Comprobar la conectividad entre los host e internet por medio de ping. En caso que sea necesario instalar el paquete que contiene el comando ping en linux, puede accederse al siguiente [enlace](https://labredes.gitlab.io/linux/ping/).

6) Documentar en un archivo de texto los comandos utilizados en el equipo Mikrotik y el equipo Cisco.
