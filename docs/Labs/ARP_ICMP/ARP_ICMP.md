# Captura y análisis de Protocolos ARP e ICMP

El siguiente proyecto en GNS3 propone el análisis de los protocolos ARP e ICMP en relación con los protocolos IPv4 y Ethernet.
El enrutador es un dispositivo Mikrotik, mientras que las dos PCs cuentan con Linux.

<div align="center"> <img src="https://labredes.gitlab.io/labs/ARP_ICMP/ARP_ICMP_GNS3.png"></div>

<div align="center"> Figura 1 - Diagrama de red </div>

Proyecto GNS3: [enlace]()

### Actividades previas

Revisar la configuración de las computadoras y el router, de forma tal de cotejar si se corresponde con lo planteado en el diagrama de red.

En las PCs es posible utilizar los comandos "ifconfig" o "ip".
Importante: en el caso del comando "ip", no es posible usar los argumentos "-c" o "-br".

## Protocolo ARP

1) Visualización de Tablas ARP

Analizar las tablas ARP de ambas PCs y el enrutador, para conocer el estado en el que se encuentran al comenzar la práctica. 
De ser necesario, limpiar su contenido.

Comandos en Linux:
* ip neigburg: [enlace](https://labredes.gitlab.io/tutoriales/Linux/Direcciones_MAC/ip_neighbour/)
* arp: [enlace](https://labredes.gitlab.io/tutoriales/Linux/Direcciones_MAC/arp/)

Comandos en Mikrotik: [enlace](https://labredes.gitlab.io/tutoriales/Mikrotik/Tabla_ARP/)

2) Captura de tráfico

Poner a capturar el tráfico por medio de Wireshark en los 3 enlaces que tiene conectado el switch.

3) Generación y análisis de mensajes ARP

Implementar un ping entre la PCA y la PCB.
Analizar los mensajes ARP que fueron capturados en wireshark y corroborar si el comportamiento se corresponde con lo desarrollado en la teoría.

Repetir el ejercicio anterior, haciendo un ping entre la PCA y el enrutador.

## Protocolo ICMP

### Comunicación dentro de la misma red

1) Poner a capturar wireshark en el vínculo entre la PCA y el switch.

2) Abrir dos