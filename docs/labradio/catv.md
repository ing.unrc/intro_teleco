# Sistemas de CATV - Televisión por Cable

ANEXO 11

Redes de Televisión por Cable






A. 11. 1. Características de las Redes de TV por Cable, CATV
La competitiva contienda de los servicios de telecomunicaciones gira actualmente también en torno a las redes de CATV. Se actúa en la integración a las redes de telefonía y a las de Internet. Para universalizar el ámbito de las redes de telecomunicaciones, deberemos adentrarnos en su particular estudio.

Las redes de acceso de las empresas prestadoras del servicio de televisión por cable, tienen fun- damentalmente las características de las redes de banda ancha. Están constituidas sobre arquitec- turas de cables de fibras ópticas o híbridas de fibra-coaxial (HFC).

Actualmente, la principal particularidad de estas redes es prestar servicio analógico al cliente, aun- que están preparadas o se están preparando, para ofrecer transmisión digital bidireccional, con banda ancha asimétrica en ambos sentidos. En ese caso, están constituidas por canales descen- dentes (de cabecera al usuario) y ascendentes (del usuario a cabecera). De esta forma podremos obtener el monitoreo de los servicios, para brindar telefonía, datos y servicios interactivos.

Básicamente la red de CATV ofrece el servicio de difusión masiva (broadcasting) de canales com- partidos, es decir sus usuarios reciben la misma programación. Con las nuevas tecnologías la transmisión podrá ofrecerse en forma multicasting. Los usuarios abonan por estos servicios una cuota mensual básica fija. El gran cambio lo ha representado ofrecer otros servicios mediante un pago extra, como ser, video sobre demanda VoD (Video on Demand), pago para ver PPV (Pay per View), juegos interactivos (SEGA Channel), Internet, telebanking, telecompras, etc.

A. 11. 1. 1. Perspectiva histórica

Al presente solo EUA dispone de una penetración mayor al 63 % del servicio de CATV, sobre un área servida del 95%, excediendo 60 millones de hogares. Le siguen mundialmente Canadá y la República Argentina.

Tal acontecimiento, con su vigoroso crecimiento y semejante aceptación, ha causado el tan elevado interés de los proveedores del servicio telefónico, produciendo a su vez la expansión de un conjunto de tecnologías competitivas, como ser equipos de grabación, sistemas como el de difusión directa satelital (DirecTV), o el acceso mediante una computadora personal a los servicios satelitales reci- biendo difusión de datos, de video e Internet.

Esto incrementa la actividad de los entes reguladores nacionales. En EUA, en 1941, se ha estanda- lizado el sistema NTSC blanco y negro, en 1953 en color y en 1984 en sonido estéreo. En Europa mientras tanto, se ha normalizado el sistema PAL. Los países Latinoamericanos, según el caso, siguieron ambas normas.

Los primeros años de la TV se han destinado en proveer al abonado entretenimiento, luego se lo eligió como vehículo para permitir rápida y variada información con la introducción de canales espe- cializados en noticias y de documentales, posteriormente se descubrió la posibilidad de proporcio- nar múltiples servicios, como ser compras televisadas, encuestas de mercadeo, alarmas de seguri- dad, audio digital de alta calidad, etc.

Los sistemas de CATV comenzaron como un medio para permitir buena calidad de imagen en zo- nas donde la recepción de TV por aire era mala por su lejanía a la emisora de TV, debido a la topo- logía del terreno o a urbanizaciones que dispusieran edificaciones muy altas. En EUA se formaron asociaciones comunitarias a ese fin, por esa razón a estas redes se les llamó inicialmente Comunity Antenna Televisión (CATV), siglas que se impusieron aunque con el significado de TV por Cable.

Ya en 1950 se construyó una red coaxial de CATV sobre postes, en la ciudad de Lansford, Penn- sylvania, EUA. En principio estas redes complementaban el servicio de TV abierta. A continuación con uso satelital se llegó a ciudades que no disponían del servicio. Posteriormente se desarrollaron los servicios en grandes ciudades donde la recepción de las señales de pocos canales era franca- mente deficiente. Por último comenzó la competición entre sistemas, proporcionando gran cantidad de canales nacionales e internacionales.

Los convenios de CATV y servicios como el de TV satelital permiten asimismo competir en forma mas amplia, nacional e internacionalmente. El servicio de EUA DirecTV lanzado en 1994, en con- venio con 65 redes de cable, incluye 175 canales, con 60 canales PPV que difunden programación de deportes y estrenos de películas, servicios de VoD y canales codificados como Playboy TV. Este servicio es ahora explotado mundialmente.

Tres grandes categorías de CATV se desarrollaron:

Super estaciones locales, como el The Turner Broadcasting System de Atlanta, Georgia, EUA, que, distribuyen la señal con la asistencia y por medio de satélites.

Canales especializados en deportes, noticias, el tiempo, educación, ventas, etc.
Canales solo de películas, como HBO (Home Box Office).

Los servicios brindados tienen tanto el carácter de básicos, como de canales y/o programas PPV, o de pagar para ver impulsivo IPPV (Impulse Pay Per View).

La diferencia entre estos sistemas, radica en que en el PPV se deberá programar con antelación y reservar la recepción deseada con el pago diferenciado del programa de película o evento determi- nado (concierto, deporte, etc). Mientras que en el sistema IPPV se podrá acceder al programa codi- ficado por un corto período, luego del cual si no se confirma su pago, su recepción se inhabilita.

La habilitación e inhabilitación del canal se realiza filtrando y cambiando la sincronización de la res- pectiva señal. También se han desarrollado los servicios de video sobre demanda VoD y los deno- minados, cuasi video sobre demanda NVoD (Near Video on Demand).

En el servicio de VoD se accede a la recepción de cualquier programa, tal como si se dispusiese de una videocasetera en la casa del abonado. En el servicio de NVoD se accede a la selección de los títulos generalmente de estreno, por distintos canales, donde la transmisión de cada programa co- menzará cada media hora. Solo se deberá esperar unos 15 minutos para el comienzo de una pelí- cula o programa especial.


A. 11. 1. 2. Acondicionamiento de la red de acceso

Los operadores telefónicos discreparon en la conveniencia de actualizar sus redes de acceso cons- truidas en pares trenzados, para facilitar telefonía digital, debido a los altos costos de su reingenier- ía y a sus largos períodos del reintegro de capital.

Solo el brindar los servicios de Internet y de video digitalizado, decidió la actualización de las redes de acceso, introduciendo sistemas xDSL, fibras ópticas, cables coaxiales y la revisión de la metodo- logía integral de los diseños de las redes de acceso.

Por otra parte, el problema mayor para las empresas de CATV, se ha presentado en obtener la concesión de la servidumbre de paso o derecho de vía pública. Generalmente las intendencias ex-

tendieron permisos precarios para la instalación de ramales aéreos, para su uso en un período cor- to y la construcción posterior subterránea que posibilitará el montaje de su red. La correcta legisla- ción de usos compartidos entre empresas ha permitido facilitar la explotación de estos servicios.

Otro inconveniente lo representa la utilización del espectro electromagnético. La telefonía requiere solo un ancho de banda de 3.1 KHz, el audio de alta fidelidad necesita 20 KHz, para la transmisión estéreo 40 KHz, el video consume 4.2 MHz y la televisión de alta definición HDTV (High Definition TV), una vez comprimida 6 MHz.

Técnicamente cada canal de TV requiere de 6 MHz, para evitar superposiciones. Esta señal luego debe ser modulada sobre una portadora que permita el transporte de múltiples señales, lo que au- menta los requerimientos de espectro electromagnético.

El espectro utilizado por la TV comercial abierta es ocupado asimismo por las comunicaciones co- rrientes de aeronavegación y para librar sus transmisiones de emergencia. La navegación marítima y aplicaciones militares consumen también parte del espectro de las frecuencias.

Solo la tecnología del cable coaxial y la fibra óptica hizo posible sanear el uso de este ocupado es- pectro, separando frecuencias de TV y aislándolas del espectro exterior.

La aplicación de frecuencias se diseña con uso similar a la transmisión de TV abierta, lo que permi- te entrar y salir al servicio de CATV, sin efectuar grandes cambios en los aparatos de TV, una de las razones del éxito de estos servicios.

A. 11. 2. Arquitectura de la red CATV
El servicio de CATV, tiene la filosofía de emisión y distribución tipo “uno a todos” (broadcasting). La red debe ser capaz de transportar una única señal a todos los clientes, con uso intensivo del ancho de banda utilizado. Tecnológicamente es el mas sencillo de los servicios.

La arquitectura de la red consta en general de una central cabecera HE (headend), desde donde parten redes de transporte hasta amplificadores troncales. La red troncal de transporte, alimenta a redes de acceso.

Esta red recorre importantes distancias, debe permitir dividir el área de servicio, en distintas zonas de distribución y preservar la calidad admisible de transmisión de la señal. Cada red de acceso o distribuidora, tramo final de la red, termina en las acometidas a los domicilios de los clientes.



Los equipos necesarios en cada una de las partes de la red de CATV, son:

Los requeridos a la obtención de la señal (recepción y/o generación) y del procesamiento de la señal (codificación, modulación y mezclado), en la central cabecera,

Los amplificadores troncales, puente (bridge) o derivadores y los prolongadores de línea en los ramales, en la red,

Terminales de recepción de video y audio aparato de televisión, video-casetera de registro VCR (Video Cassette Recorder) (VCR), y en su caso los conversores, STB (demodulación y decodifi- cación), caja inteligente de uso complementario al televisor en la casa del cliente.



A. 11. 2. 1. Conformación de la red de CATV

De la conformación típica de red troncal y red distribuidora, surge la topología básica árbol (troncal y ramales), constituida por una línea principal con sus derivaciones.


Históricamente las redes de CATV han sufrido varios cambios, de red enteramente coaxial pasó a ser una red híbrida fibra óptica - coaxial, llegando al presente a disponer de redes digitales todo óptica, e incluso con uso de pares trenzados de cobre.

Dos tipos de cables coaxiales son principalmente usados, uno rígido con un tubo de aluminio como conductor exterior y un conductor central de cobre recubierto de aluminio y otro flexible con un con- ductor exterior formado por un alambre trenzado con un conductor central de cable de acero recu- bierto con una lámina de cobre. La impedancia característica del cable coaxial utilizado para CATV es de 75 Ohm.

La principal desventaja del cable coaxial es su relativa pérdida. Su atenuación es función del diáme- tro del cable, construcción del dieléctrico, temperatura ambiente, y frecuencia de operación. Un ca- ble de de diámetro en 1.27 cm (media pulgada), trabajando a 181 MHz, tiene aproximadamente 1  dB de atenuación por cada 30 m., mientras que un cable de 2.54 cm (una pulgada), posee 0.59 dB.

El logaritmo de la atenuación (en dB) del cable varía con la raíz cuadrada de la frecuencia. Luego la atenuación a 216 MHz (canal 13) es el doble que a 54 MHz (canal 2), ya que la relación de frecuen- cias es de cuatro veces. Si el canal 2 tiene una atenuación de 10 dB en 300 m, el canal 13 tendrá  20 dB (Fig. 1).



Frecuencia (MHz)

Fig. 1 - Atenuación vs. frecuencia en cable coaxial
Se puede estudiar las configuraciones tradicionales de redes coaxiales considerando cinco principa- les partes:

1) Estación cabecera.
2) Cable troncal.
3) Distribución zonal.
4) Acometida a la casa del abonado y su alambrado interno.
5) Equipo terminal del abonado.

Estación cabecera.

La estación cabecera es el punto de origen de la señal. La estación cabecera dispone su equipamiento en:
Antenas parabólicas satelitales, para la recepción de programas desde otros países, antenas parabólicas de radioenlaces en microondas terrestres para recepcionar señales desde emisoras de difusión abierta (broadcasting) y en VHF y UHF para recepcionar emisiones de TV abierta.


Demoduladores, que extraen la banda base de video y audio de las señales aéreas en radio fre- cuencia RF.
Receptores y decodificadores satelitales, para extrae la banda base de señales desde satélites.
Receptores de VHF / UHF o microondas, operando en banda base de televisión abierta.
Receptores ópticos, para los cables troncales de fibra óptica.
Conversores de normas PAL / SECAM / NTSC, tipos N, M, B y G.
Moduladores, que permiten construir a partir de señales de banda base una señal de banda an- cha sobre portadora de RF, para ser distribuida, estándar IRC / HRC.

Combinadores, que combinan distintas señales de TV de salida de los moduladores e insertarlas al cable coaxial.
Procesadores de señales combinadas de modulador y demodulador, que ajustan el nivel y re- transmite canales abiertos, en nuevo canal.
Codificadores mezcladores, scramblers, de las señales de video y/o audio para evitar la recep- ción a programas o canales no autorizados.
Tales equipos permitirán la recepción de señales, originar y editar programas locales, la grabación en cintas de video de su programación y de los comerciales, así como retransmitir señales a otras emisoras, etc. (Fig. 2).



Fig. 2 - Sistema cabecera

Cable troncal

El cable troncal transporta la señal a las zonas barriales. Su principal función es la de cubrir tal dis- tancia preservando la calidad de la señal. El equipamiento de la red troncal dispone de amplificado- res troncales. Estos tienen características de baja distorsión a bajo nivel de salida. Pueden poseer salidas de distribución tipo bridge.

El número de amplificadores empleado dependerá de la longitud de la red troncal, entre la cabecera y la zona de servicios.

Los amplificadores instalados en cascada en una ruta troncal (llamada una corrida) están limitados por el ruido acumulado de acuerdo al factor 10 log N (donde N es el número de amplificadores a lo

largo de la cascada), el que es llamado distorsión por ruido (building-up). Podrán instalarse para una aplicación con relativo ancho de banda, unos 30 amplificadores, aunque primitivos sistemas con pocos canales pueden tener tanto como 60 amplificadores en cascada (Fig. 3).



Fig. 3 - Planta de distribución.

La red troncal podrá estar constituida con cables coaxiales de 0.750”, 0.860”, 0.875”, 1.00” y 1.25” de diámetro. A mayor diámetro, le corresponderá menor resistencia eléctrica, por unidad de longi- tud. La práctica actual es construir la red troncal empleando cables de fibra óptica, aunque existen todavía troncales en coaxial. Con cables coaxiales se requieren instalar amplificadores cada 600 m, dependiendo esta distancia del ancho de banda del sistema. Esta planta comprende aproximada- mente el 15 % de la red.

Distribución zonal

La distribución zonal o alimentación barrial se realiza mediante cables coaxiales, que disponen ca- jas de derivación (taps), las que permiten la conexión de la acometida al domicilio del abonado. Es- tos cables tienen como interfaz al cable troncal un amplificador denominado amplificador puente (bridge amplifier), que incrementa el nivel de la señal para llegar correctamente a múltiples hogares. Los elementos activos de esta red, corresponden a amplificadores de distribución, de alta distorsión y con alto nivel de salida.

Los elementos pasivos de la red de distribución, (que no recibe alimentación eléctrica), comprenden los taps para instalar las acometidas de abonados y los divisores de señales ópticas (splitters), para dividir la línea en 2, 4 u 8 partes.

Si se desea una red para servicios bidireccionales, no se deben utilizar mas de tres divisores, ya que a medida que se divide la señal, mas usuarios comparten el ancho de banda lo que producirá efectos indeseables como ser la producción de ruido. En este sector de red se podrá disponer ele- mentos adicionales como ser:

Generadores del fluido eléctrico, que permiten proveer la tensión de alimentación a la red,
Rectificadores de alimentación, que llevan la corriente alterna a corriente continua, alimentación de los amplificadores,
Conectores de cables y terminales.
Acopladores direccionales de potencia, que permiten tomar derivaciones de una línea de energía con un menor nivel, dejando pasar la corriente alternada,
El sector de ramales de distribución, podrá incluir cables coaxiales de 0.412”, 0.500” y 0.625” de diámetro. Uno o mas amplificadores, denominados generalmente extensores de línea, son incorpo- rados en los ramales de distribución. Esta planta comprende aproximadamente el 40 % de la red.

Acometida a la casa del abonado


La acometida a la casa del abonado o cable de bajada y su cableado interno, se efectúa mediante un cable coaxial flexible que tiene una longitud típica de 45 m (Fig. 4).



Fig. 4 - Equipos terminales y acometida al domicilio del abonado

La sección de acometida al abonado, podrá montar cables tipo RG-59, RG-6, RG-7 y RG-11 Esta planta comprende aproximadamente el 45 % de la red.
Equipos de abonados

En un caso simple el terminal del abonado esta representado por el aparato de televisión.  También se podrá disponer de una videocasetera tipo VCR para grabar y/o reproducir programas, un decodi- ficador de canales condicionados, un conversor de frecuencias para lograr sintonizar canales no disponibles en el televisor y/o un conversor / decodificador inteligente del tipo STB, que contenga  las funciones de un codificador para sistemas PPV o IPPV y que permita la operación bi-direccional para el uso de servicios interactivos como ser telecompra o homebanking.

Tanto el conversor como el codificador o el STB disponen de un sintonizador interno que laboran típicamente con el canal 3 ó 4. La sintonía se logra seleccionando los canales dentro de la gama de frecuencias establecidas y disponibles por la empresa de CATV local (Fig. 5).


Fig. 5 - Plan normalizado de frecuencias

En la figura los números superiores indican la asignación del estándar EIA (Electronics Industry Association), mientras los inferiores indican la designación histórica de canales en USA.  Conformación de la red total

Al finalizar la construcción de la red se observará una conformación tipo árbol con troncales y rama- les bien definidos (Fig. 6).



Fig. 6 - Conformación de la topología tipo árbol
Emplazamiento de los equipos

Una cuestión secundaria, pero de la mayor importancia práctica a la implementación de una red, la constituye la ubicación de los equipos coaxiales terminales, comunes a varios abonados. Existen dos alternativas de emplazamiento, con ubicación fuera del domicilio del cliente o dentro del mismo.

La primera opción podrá ser implementada de dos formas distintas, en fachada o en pedestales ubicados en aceras. En todos los casos los equipos son alojados en armarios de regular dimensión.

En el caso de ubicar los equipos dentro del edificio se buscará emplazar el armario en un sótano de acceso común. El caso de instalación en fachada será factible para áreas de baja densidad pobla- cional, mientras que el caso de instalación en acera será dado para áreas de mayor densidad de- mográfica, siempre que se disponga de suficiente espacio y que fuera factible la concesión de los permisos municipales correspondientes.

Los equipos coaxiales terminales, ubicados dentro del edificio plantean como ventaja el no requerir gabinete para intemperie y obtener facilidades mas simples en la toma de energía eléctrica. Por su mayor accesibilidad la experiencia inclina hacia la opción de emplazamiento exterior.

Toma de energía eléctrica

La alimentación eléctrica de los equipos se relaciona a la condición tradicional de mantener la con- tinuidad del servicio telefónico básico sin interrupciones.

El potencial de corriente alterna se toma desde la fuente de potencia y es insertado en la red por medio de un implemento denominado LPI (Line Power Insert). La fuente provee un voltaje de 60 VAC (RMS) en forma de onda cuasicuadrada. Esta onda cuasicuadrada es un compromiso entre una onda cuadrada y una onda senoidal. Esta forma de onda se utiliza por disponer de mayor valor eficaz RMS (Root Mean Square) o valor cuadrático medio, para un mismo valor pico de tensión.

En caso de red coaxial / pares trenzados, la alimentación se podrá efectuar a través de los coaxia- les, centralizando la generación eléctrica y llevándola por este intermedio a las cercanías del clien- te. Los inconvenientes se originan al emplear corrientes eléctricas con formas de ondas cuasicua- dradas las que provocan altas pérdidas, por lo que se deberá implementar poderosos generadores y baterías en las centrales.
Al emplear redes híbridas fibra / coaxial no es posible enviar alimentación eléctrica desde la central como se realiza habitualmente con la red de pares trenzados

Una solución sería la instalación de cables mixtos, ópticos y de conductores de cobre, o cables pa- ralelos como alimentadores, recurso totalmente antieconómico. Otra forma es la alimentación des- de el domicilio del cliente, mas de esta forma no se obtiene seguridad para los cortes de energía

accidentales o producidos adrede. Ello requeriría colocar baterías de respaldo, lo que encarecería al sistema.

Una tercera opción es recurrir a la telealimentación desde los centros remotos, donde se debe im- plementar la toma de energía a la red eléctrica pública y disponer las baterías de respaldo, que ga- ranticen cortes por varias horas; de 4 á 10 horas según el servicio operado.

A. 11. 2. 2. Calidad de la señal

La red de distribución de TV por cable debe cumplir severas normas de calidad. En caso contrario la señal que recibe el abonado, por hacer uso de modulación analógica, se traducirá en degrada- ciones perceptibles de la imagen. Por otra parte el objetivo del sistema de CATV es permitir una adecuada calidad de servicio a un aceptable precio que compense sus altas inversiones.

Este difícil balance no sería necesario si se dispusiera brindar alta calidad de video sin importar la consideración de los costos. Definir la calidad de servicio será considerar múltiples factores como ser calidad de imagen, sonido, variación de la programación, etc. Los principales parámetros, según normas del FCC de EUA, son:

Nivel de entrada > 0 dBmV,
Relación señal de portadora / ruido (CNR) > 43 dB,
Relación portadora / modulación cruzada (C / Xmod) 53 dB,
Relación portadora / batido de segundo orden compuesto (C / CSO) 51 dB,
Relación portadora / batido triple compuesto (C / CTB) 51 dB,
Hum (zumbido interferente presente en pantalla, originado por el batido con frecuencia de líneas de potencia).

El factor mas importante en la calidad de la imagen, sonido y video, es el ruido. Se expresa en la relación de ruido a portadora de canal CNR (Carrier to Noise Radio), su valor característico es de 48 á 50 dB. Se presenta el efecto de lluvia o nieve en la pantalla entre 42 á 44 dB, siendo intolera- ble entre 40 á 41 dB.

Otro efecto se produce por reflexión de la señal debido a discontinuidad de la impedancia carac- terística de la línea que produce la modulación cruzada llamada de batido, ofreciendo sobre la pan- talla movimientos erráticos diagonales. Valores recomendados para lograr una correcta señal están dados por organismos tales como el FCC de USA:

VALORES CARACTERÍSTICOS DE CALIDAD DE SEÑAL : FCC

Parámetro
Valor
Nivel de señal de TV
0 á 3 dBmV
Carrier / Noise (CNR)
48 á 50 dB
Batido de segundo orden compuesto (CSO)
51 dB
Portadora / modulación cruzada (C / Xmod)
53 dB
Batido triple compuesto (CTB)
51 dB

Las mediciones de los niveles de la señal se realizan en dBmV, es decir relativos a 1 mV medidos sobre 75 Ohm. El objetivo es obtener una figura de ruido entre 0 á 10 dBmV en el receptor de TV, valores menores producen nieve y efectos de modulación cruzada.

Como el objetivo del cable troncal es transportar la señal en largas distancias con mínima degrada- ción, es utilizada la fibra óptica o cable coaxial de baja perdida, como lo es el de 1.9 cm (0.75 “) de diámetro, mientras que coaxiales de 1.27 cm (0.50”) se emplean en ramales de distribución. La se-

ñal en el sector troncal es ampliada de 30 á 32 dBmV, espaciando los amplificadores el equivalente a 20 dB.
A. 11. 2. 3. Conversor inteligente, STB

La introducción de nuevos servicios en la red de CATV, además de la difusión de televisión, con otros servicios adicionales tan o mas importantes, Internet, telefonía, datos de alta velocidad, entre- tenimientos, etc., debe hacer uso de un conversor inteligente, STB (Set Top Box).  El mismo deberá disponer, para cada servicio con alguna de las siguientes funciones:

Decodificación de canales codificados,
Recepción de la información digital transmitida sobre una subportadora asociada al canal sinto- nizado o por un canal dedicado a ese fin,

Memoria no volátil protegida,
Transmisión de datos digitales por canal de retorno,
Generación de caracteres y posibilidad de sobreimprimirlos en pantalla.

La decodificación sirve al control de transmisiones de fútbol, películas condicionadas o premium, de mayor calidad o comercialización diferenciada. Uno de los métodos de codificación es dado por la supresión del sincronismo.

La recepción de la información digital transmitida sobre una subportadora asociada al canal sintoni- zado o por un canal dedicado a ese fin, sirve como información necesaria a la decodificación de canal o programa determinado. Esta información podrá ser enviada por mensajes broadcast o diri- gidos a cada cliente.

La memoria no volátil sirve a los servicios de PPV, por ejemplo información de tarificación definida. Es protegida contra intervenciones del cliente. La transmisión de datos digitales por canal de retor- no sirve a la comunicación bidireccional, por ejemplo en el sevicio PPV recoger los datos de tarifi- cación registrada en el STB.

La generación de caracteres de vídeo con posibilidad de sobre imprimirlos, se utiliza para enviar mensajes generados por el operador o el mismo STB, por ejemplo de interrupción o codificación de programas.

Los STB inteligentes podrán ser programados a distancia en forma general o individual. Por ejem- plo en el servicio IPPV, en el mismo se podrá decodificar los primeros cinco minutos del programa de un canal y si no recibe confirmación del pago diferenciado, vuelve al estado de codificado.

El servicio de teleguía reside en ayudar al cliente en la selección de programas. La cabecera envía por ejemplo, un menú al STB de la programación de varios días, por horario, canales o tipo de pro- gramas. El STB para este caso deberá disponer del generador de caracteres.

A. 11. 2. 4. Redes coaxiales

En un comienzo las redes para CATV son comercializadas por múltiples operadores, por ello se configuraban múltiples pequeñas redes.

En estas condiciones de pequeñas áreas, los cables coaxiales sirven en forma óptima a los clien- tes. Estas redes están conformadas por varios cables coaxiales que nacen en la central cabecera como troncos y de los que parten los ramales de distribución.

Los cables troncales tienen características de baja atenuación, los amplificadores troncales están espaciados cada 700 m y operan a bajo nivel de señal a fin de no producir distorsiones producidas por los elementos electrónicos activos. Con esta conformación, no es posible cubrir muy grandes distancias por la degradación de la señal en la red troncal (Fig. 7).





Fig. 7 - Red coaxial

Los canales de retorno son compartidos por todos los clientes (de 2000 á 200 000 ó mas), resul- tando que el ancho de banda disponible es insuficiente. Como consecuencia, sobre el canal de re- torno, en la cabecera, solo se recibe ruido.

A. 11. 2. 5. Red híbridas fibra - coaxial, HFC

La evolución de la arquitectura de la red, prosiguió como consecuencia de la fusión de los distintos productores de programas de CATV y operadores de las redes. La nueva red resulto entonces, con múltiples cabeceras. Como la programación emitida deberá ser la misma en todas las áreas, excep- to algunos canales zonales, exigió unir entre sí las cabeceras.

Por cuestiones de confiabilidad, capacidad y calidad de la señal se pasó de enlaces coaxiales a vínculos con fibra óptica. En las cabeceras se retiraron los equipos de transmisión, luego con la provisión de amplificadores de fibra óptica, surgió la posibilidad de confeccionar nuevos recorridos sin utilizar las primitivas cabeceras locales, salvo para casos especiales de emitir canales zonales.

Al generalizarse el uso de la fibra, ya no solo se empleó como enlaces de redes o troncales, sino que también se instalaron en ramales de gran longitud combinándolas con cables coaxiales. Los tramos troncales, columnas vertebrales de la red (backbone), realizados en fibra óptica permiten llegar con ésta hasta centros de distribución secundarios, llamados nodos ópticos, donde amplifica- dores y divisores de la señal, dan origen a ramales distribuidores que cubren las áreas de servicio.

La red se podrá construir en ramales aéreos o subterráneos,  dependiendo de las reglamentaciones edilicias vigentes. Los nodos ópticos son ubicados externamente, en fachada, pedestales o en pos- tes. Estos nodos realizan la conversión óptico-eléctrica de la señal para alimentar los cables coaxia- les. A medida que se reduzcan las zonas de servicio, la extensión de la red distribuidora es menor y se reduce el número de amplificadores de distribución.
En general, las áreas de servicio de tamaño reducido, sirven a 500 domicilios. En un principio, mientras no se utilice a plenitud la función iteractiva y a fin de disminuir costos, cada uno de éstos nodos ópticos podrá servir áreas de hasta 2000 domicilios.

El diseñador deberá confeccionar en distintas situaciones, para diversos proyectos, esquemas futu- ros de áreas para 500 domicilios, de tal forma que las actuales áreas de 2000 domicilios, puedan  en cualquier momento ser subdivididas. Para el caso de utilización de canal de retorno las áreas de servicio se reducirán a 500 casas, dentro de los hogares pasados (domicilios en calles cableadas).

A cada nodo óptico llegarán dos fibras ópticas. El canal hacia los clientes puede ser implementado con una red pasiva, siendo alimentados todos los nodos con la misma central, la fibra de retorno será individual para cada nodo.

Las ventajas del uso de la fibra óptica son:

Ancho de banda muy amplio,
Mejorar la calidad final de la señal,
Ser inmune a las señales interferentes,
Brindar mayor confiabilidad,
Disminuye el número de amplificadores,
Muy flexible a disponer topologías anillo, árbol, bus o árbol.

El esquema de troncal de fibra óptica, combinado con cables coaxiales, toma la forma de topología tipo árbol (Fig. 8).



Áreas de servicio

Fig. 8 - Sector troncal (backbone) de fibra

Las ventajas de una red HFC, radican en producir una plataforma económicamente provechosa, para la prestación de los servicios de CATV. Contiene un ancho de banda por usuario, que permite la evolución hacia sistemas con conmutación digital de video y servicios multimedia interactivos.

Las desventajas se centran en una alta sensibilidad a las pérdidas de retorno, originadas por conec- tores y empalmes.

Se podrá utilizar modulación analógica del Láser, en AM ó FM. La transmisión puede ser en 1310 nm ó en 1550 nm, En tercera ventana, 1550 nm se alcanzan mayores distancias, debido a la menor pérdida, dB/Km.

La siguiente tabla resume las principales características de posibles redes a desarrollar.

CARACTERÍSTICAS DE LAS DISTINTAS ARQUITECTURAS DE REDES DE CATV


Coaxial
Multicabecera
Troncal de FO
HFC
Costo
Bajo
Medio
Medio
Alto
Confiabilidad
Baja
Media
Media
Alta

Calidad
Baja
Media
Media
Alta
Ruido canal de retorno
Alto
Medio
Alto
Bajo
Ancho de banda de retorno
Bajo
Medio
Bajo
Alto

Como vimos, la red de CATV podría estar constituida íntegramente por cables coaxiales o por re- des híbridas de cables de fibra y cables coaxiales, también se podrá como alternativa emplear re- des construidas enteramente con cables de fibras ópticas. En estas redes se reemplaza la red tron- cal y distribuidora, hasta los nodos ópticos y/o áreas de servicio, con fibra óptica, de tal forma se eliminan los amplificadores troncales. Cada nodo óptico operará en forma independiente.

Según el tipo de cliente, grandes edificios, negocios o clientes residenciales, se podrá llegar, con distintas topologías y cables. Actualmente en general se combina las formaciones en anillo con to- pologías tipo árbol. Los anillos ópticos, mediante técnicas FDDI, al reconstruir los caminos de servi- cio, proveen dos caminos de acceso a cada nodo, aprovechable para los servicios interactivos o como enlace de reserva para posibles fallas en la red.

A. 11. 3. Relevos de la red de acceso CATV
Con el paso de los años, el proporcionar relevos a las redes existentes de CATV se ha transforma- do en la prioridad mas importante. Una suma de eventos ha contribuido a incrementar la necesidad de relevar las redes actualizándolas a las necesidades del mercado.

Sin lugar a duda, la liberalización de los servicios y la alta competencia entre operadores han origi- nado el motivo inicial, sin embargo mas trascendental aún ha sido, el proporcionar no solo VoD o HDTV, sino contribuir a satisfacer los servicios de comunicaciones personales PCS (Personal Communication System).

A. 11. 3. 1. Relevos iniciales

Vimos que las primeras redes estaban conformadas bajo una combinación de cables coaxiales. Generalmente, estas redes se ampliaban en forma lineal, agregando ramales de distribución, de tal forma resultaban conformadas con una serie inconveniente de amplificadores troncales funcionan- do en cascada.

La solución la proporcionó la fibra óptica, en cuanto permitió cubrir largos trayectos con mínimas pérdidas. El recurso mas conveniente pareció ser interceptar nodos ópticos en el recorrido del tra- yecto del cable troncal, alimentados desde la estación cabecera o el concentrador remoto, con lar- gos tramos de fibra óptica, mediante los cuales se dividió estos trayectos en sectores independien- tes.

La arquitectura conformada se la denominó fibra al área del cable CAN (Cable Area Netwoork). En un principio, al nodo receptor llegó tanto el cable coaxial existente, como la fibra de relevo. En el nodo la señal óptica era convertida en señal de RF y se seleccionaba la señal que llegaba por la fibra o por el coaxial (Fig. 9).



Fig. 9 - Relevo tipo fibra al área del cable (CAN)

Como en un principio la fibra óptica no contaba con la confianza suficiente de no producir fallas, se instalaron en dichos nodos relevadores que conmutarían la señal al troncal coaxial, que se manten- ía vinculado. En realidad se conmutaba al cable coaxial solo en caso de falla de la fibra. Con ello se minimizaban los efectos indeseables proporcionados por los amplificadores instalados en cascada, incrementando a su vez la calidad de la señal a utilizar de fibra óptica.

A. 11. 3. 2. Relevo de áreas de servicios

Las redes iniciales de cables coaxiales en muchos casos habían creado importantes plantas de distribución por lo que estos primeros relevos fueron severamente criticados, lo que originó nuevos estudios y nuevas soluciones.

El éxito de las redes CAN, llevo a la creación de diseños mas avanzados. Uno de estos arreglos fue el determinar áreas de servicio, las que serían servidas por nodos ópticos, vinculadas desde la ca- becera mediante cables alimentadores de fibra óptica. A estas arquitecturas se les llamó, fibra al área de servicio FSA (Fiber to the Serving Area) (Fig. 10).



Fig. 10 - Relevo tipo fibra al área de servicio (FSA)

La eliminación del respaldo de cable coaxial, dado en el método anterior, creó la necesidad, en ese momento, de implementar elementos redundantes, estudiar los métodos de restablecimiento rápido del servicio en caso de fallas y efectuar verificaciones constantes de la calidad del servicio para prevenir su aparición. Los operadores mensuraron los tiempos fuera de servicio, estudiaron los me- jores métodos para la rehabilitación rápida del servicio y las causas que las determinaban.

Se analizó la cantidad de falla por mes y el total de minutos fuera de servicio por mes, comparándo- la con los métodos de relevo anteriores y la posición relativa del abonado en la red. El resultado de los estudios aconsejó incrementar los elementos redundantes para disminuir los tiempos fuera de servicio, cuidar la calidad de estos elementos y la correspondiente a la mano de obra de instalación y del mantenimiento, así como disponer de convenientes registros. Este método dio servicio expan- dido a 550 MHz, principiando en USA, con 2500 hogares.
A. 11. 3. 3. Relevo mediante amplificadores puente

Al generalizarse el método de relevo con creación de áreas de servicio, conformó redes del tipo estrella, desde la estación cabecera. Estos relevos de redes coaxiales permitió efectuar reingenier- ías con bajos costos, sin embargo a producido en las distintas áreas a intervenir, tiempos altos fue- ra de servicio para efectuar los relevos. Se considera un termino medio de un día fuera de servicio, para producir el relevo del cable alimentador, conexión al nodo óptico y la reubicación de los ampli- ficadores afectados.

Por otra parte muchos operadores estaban deseosos de reutilizar los cables existentes, tanto como fuese factible. Se creó luego, el método y arquitectura denominada, terminación de tronco con ter-

minal intermediario FITT (Forward Intermediate / Terminnating Trunk) a modo de nueva versión del cable troncal en fibra óptica (Fig. 11).



Fig. 11 - Relevo mediante arquitectura FITT

Esta arquitectura además de permitir alimentar un sector de red mediante fibra óptica y un nodo amplificador, provee estaciones puente que actúan como derivadores de los cables coaxiales ali- mentadores existentes a los distintos ramales distribuidores.

Las estaciones puente aíslan la cadena de amplificadores en cascada, lo que permite anular sus indeseables efectos. Estas actúan como acopladores direccionales o divisores pasivos (splitters) de las señales.
Tal arquitectura también provee servicios hasta 550 MHz. Su ventaja principal radica en que los relevos se podrán efectuar con el mínimo tiempo fuera de servicio, ya que no es necesario reubicar los amplificadores. El punto de inserción será estudiado de forma tal que quede equilibrada la can- tidad de amplificadores en cascada existente.

Un número guía de diseño, es considerar como límite máximo desde el nodo óptico un máximo de 8 amplificadores en cascada. Para este valor se considera 4 amplificadores sobre el cable troncal, 1 como estación puente (Intermediate Bridge Station), 1 amplificador distribuidor (System Amplicator) y 2 amplificadores sobre un ramal, como extensores de bucle (Line Extender).

A. 11. 3. 4. Relevo mediante conmutadores ópticos

El relevo de un enlace óptico y así la planta completa de CATV, puede ser una operación despro- porcionada al tener que adicionar elementos redundantes al sistema. Su ejecución podrá ser facili- tada con el empleo de un conmutador óptico que permita efectuar la permutación de los transmiso- res, receptores, medidores o las mismas rutas de cables ópticos con suma facilidad.
El operador del CATV podrá elegir el elemento con mayor probabilidad de sufrir fallas y emplazar  un equipo redundante como respaldo (backup), mediante el conmutador óptico se podrá producir el recambio en caso de falla, mediante una simple operación de conmutación. La operación básica será, prever el reemplazo de un transmisor primario por otro de respaldo (backup) (Fig. 12).




Fig. 12 - Transmisor de respaldo con conmutador óptico

Esta configuración también permite permutar un circuito transmisor - receptor, introduciendo un reflectómetro óptico en el dominio del tiempo (OTDR), sin necesidad de abrir las conexiones (Fig. 13).


Fig. 13 - Prueba de un sistema mediante conmutador óptico

Un conmutador óptico podrá también ser utilizado para introducir un receptor de respaldo (Fig. 14) o mediante dos conmutadores ópticos conectar una fibra óptica alternativa (Fig. 15)




Fig. 14 - Receptor de respaldo con conmutador óptico



Fig. 15 Fibra óptica alternativa con conmutadores ópticos

Los tres elementos, transmisor, fibra óptica y receptor podrán ser combinados para un sistema completamente redundante (Fig. 16).





Fig. 16 - Transmisor, fibras ópticas y receptores redundantes

Proveer varios equipos redundantes eleva el costo de la red, luego se podrá considerar conveniente proveer un solo componente redundante y emplear la conexión apropiada de varios conmutadores ópticos (Fig. 17). También se podrá emplear los conmutadores ópticos para conectar dos amplifica- dores a una línea óptica (Fig. 18).


Fig. 17 - Transmisor de respaldo múltiple



Fig. 18 - Línea óptica compartida
A. 11. 3. 5. Arquitectura de los relevos

Los distintos métodos de relevos para redes de CATV, anteriormente desarrollados, podrán ser catalogados como conformación de distintas arquitecturas diferenciadas:

Supertronco (supertrunk)

Tipo FBB (Fiber Backbone)

Tipo CAN (Cable Area network)

Tipo FTF (Fiber to the Feeder)

Arquitectura de supertronco

La limitación de amplificadores en cascada se podrá solventar implementando supertroncos que llevan las señales desde las estaciones cabeceras hasta centros de distribución (hub), en los que se efectúa la conversión del formato de modulación. Este enlace podrá, según el caso, ser del tipo coaxial, óptico o de microondas. El costo adicional de los Hub indica ser una solución no atractiva.

Arquitectura FBB

En la arquitectura de fibra troncal FBB (Fiber Backbone), se divide la red en zonas de servicio mas pequeñas cada una de las cuales es alimentada desde la cabecera mediante un enlace óptico de alta calidad a un nodo óptico receptor, con cuatro u ocho amplificadores.

En el caso de rutas coaxiales existentes con gran cantidad de amplificadores se podrán invertir el sentido de los amplificadores para proporcionar señales de retorno (Fig. 19).



Fig. 19 - Arquitectura de fibra troncal (FBB)

Arquitectura CAN

La arquitectura denominada cable al área de red CAN (Cable Area Network), es similar a la FBB  con la salvedad que se crean recorridos distintos a los cables coaxiales, hasta alcanzar los nodos ópticos receptores, manteniendo estos vínculos existentes como elementos de reserva, a ser con- mutados en los nodos en caso de emergencia. Por esta razón no se dispone de señales de retorno.  Arquitectura FTF

Para reconstrucciones completas o construcciones nuevas la arquitectura de fibra al cable alimen- tador FTF (Fiber to the Feeder), es la mas adecuada, ya que no hace uso de amplificadores tronca- les y utiliza mayor cantidad de fibra óptica que los modelos anteriores.

En la técnica de CATV se suele llamar cable alimentador al que comúnmente designado como dis- tribuidor, por esta razón a esta arquitectura se la designa como fibra hasta el alimentador (Fig. 20).



Fig. 20 - Arquitectura de fibra al cable alimentador FTF

A. 11. 3. 6. Arquitectura celular

El acelerado avance tecnológico dio origen a rápidos estudios de alternativas de redes y a sus va- riadas arquitecturas, con una alta variedad de términos, que provoca una severa confusión. Podre- mos nombrar las distintas arquitecturas empleadas tanto en redes de CATV, como en redes mixtas. Las mismas se podrán ordenar según la extensión de la red óptica:

CAN	: Cable Area Network	Cable al área de red de distribución. FBB	: Fiber Backbone	Fibra como troncal.
FSA	: Fiber to the Service Area	Fibra al área de servicio. FTTF	: Fiber to the Feeder	Fibra al alimentador.
FTB	: Fiber to the Bridge	Fibra al cable puente.
FTN	: Fiber to the Node	Fibra al nodo de distribución. FTLE	: Fiber to the Line Extender	Fibra al amplificador distribuidor. FTLA	: Fiber to the Last Amplifier	Fibra hasta el último amplificador. FTTC	: Fiber to the Curb	Fibra hasta la acera o al pedestal. FTT	: Fiber to the Tap	Fibra al distribuidor de acometida. FTTH	: Fiber to the Building	Fibra hasta el edificio.
FTTH	: Fiber to the Home	Fibra hasta el hogar.

Para evitar este embrollo de nombres, será conveniente definir las arquitecturas en términos de celdas, lo que permitirá una mayor eficiencia en la planificación. Cada celda será un área de servi- cio alimentada desde un nodo óptico y con un centro de distribución propio.

El tamaño de la celda deberá tener en cuenta cubrir la demanda futura y con la flexibilidad de posi- bilitar su subdivisión por incremento de la demanda, para lo cual será provisto de fibras oscuras (de reserva), para evitar cableados adicionales.

La aproximación celular simplifica el diseño debido a su flexibilidad en relación a la introducción de nuevas tecnologías. A medida que disminuye el tamaño de las celdas crece el rendimiento y la can- tidad de servicios que se podrán ofrecer. Todos los sistemas son alimentados por señales prove- nientes de una única estación cabecera.

A fin de subdividir las celdas, se adoptarán niveles celulares superiores. Los enlaces de los distintos sistemas se logrará utilizando un nivel de supercelda que vinculará la estación cabecera mediante fibra óptica, microondas, enlace satelital o una combinación de estas técnicas. Un anillo con doble sentido de transmisión, podrá brindar redundancia beneficiosa para el caso de una falla.

Superceldas

Las superceldas han sido definidas en los años de 1980, debido al alto costo de la optoelectrónica, lo que permitía compartir los equipos entre gran cantidad de abonados, de 7000 á 9000 hogares. Surgieron luego las arquitecturas FBB y CAN permitiendo reducir el número de amplificadores y mejorar la CNR y la distorsión (Fig. 21).



Fig. 21 - Red de superceldas

Celda standard

Cada celda estándar sirve a 2000 ó 2500 hogares. Entre las arquitecturas utilizadas se encuentran las FTLE y FTTF. Los cables de fibra óptica utilizados en este tipo de celdas incluyen dos o tres veces el número de fibras que aquellos para las superceldas.
Debido a la reducción de la cantidad de amplificadores en cascada en la red de distribución se logra un rendimiento mejor distribuido (Fig. 22).

Minicelda

Una minicelda posee de dos a cuatro amplificadores de distribución en cascada, lo que puede servir a 500 ó 650 hogares. En áreas urbanas densas las cascadas pequeñas permiten anchos de banda de 1 GHz. En áreas rurales 750 MHz es el valor característico, ya que aquí las áreas serán mas extendidas. Las arquitecturas mas utilizadas son las FTLE y FTTF.



Fig. 22 - Red de celdas standard

Microceldas

Las microceldas tienen un tamaño aproximado a la cuarta parte de una minicelda. En áreas urba- nas no incluyen equipamiento activo luego del nodo óptico. En áreas rurales se requerirán uno o dos amplificadores de distribución para cubrir de 100 á 150 hogares.
Las arquitecturas utilizadas son FTTF y FTB, aunque también las FTN y FTLA o asimismo las FTTC y FTT, como equivalentes. En zonas muy densas es conveniente el uso de técnicas WDM para evitar el empleo de gran cantidad de fibras ópticas.

Picoceldas


Las picoceldas están constituidas con menos de 50 hogares por celda. Lo que resulta una red su- mamente costosa. Las arquitecturas FTTH, FTTC y FTT.

CARACTERÍSTICAS DE LOS DISTINTOS TIPOS DE CELDAS


Supercelda
Celda Estándar
Minicelda
Microcelda
Picocelda
Abonados pa- sados
7000 á 9000
2000 á 2500
500 á 650
100 á 150
hasta 50
Ancho de ban- da de distribu- ción
54 á
550 MHz
54 á
700 MHz
50 á
1000 MHz
50 á
1000 MHz
50 á
1000 MHz
Ancho de ban- da de retorno
5 á 30 MHz
5 á 30 MHz
5 á 30 MHz
5 á 30 MHz
5 á 30 MHz
Nº de amplifi- cadores de distribución
10 á 15
4 á 8
2 á 4
1 á 2
0
CNR
45 á 47 dB
48 á 50 dB
50 á 51 dB
50 á 52 dB
49 á 53 dB
CTB
-51 á -53 dB
-53 á -55 dB
-55 á -59 dB
-60 á -62 dB
-61 á -63 dB

A. 11. 4. Bandas del espectro de frecuencias
Haciendo uso de multiplexación por división en frecuencia (FDM), el espectro de frecuencias esta distribuido en canales, para el empleo del medio, tanto aire como cable.

PARÁMETROS DE LAS NORMAS M Y N

Parámetro
M
N
Línea/cuadro
525
625
Cuadro/segundo
60
50
Ancho de banda (MHz)
6
6
Separación portadora de sonido (MHz)
+4.5
+4.5
Distancia de portadora al canal cercano (MHz)
-1.25
-1.25
Ancho de banda lateral principal (MHz)
4.2
4.2
Ancho de banda lateral vestigial (MHz)
0.75
0.75
Modulación de video
A5C negativa
A5C negativa
Modulación de audio
F3
F3

La UIT - R divide al mundo en que vivimos, según zonas. Cada una de ellas dispone un uso distinto del espectro radioeléctrico.

En la zona que comprende al continente americano se reservó 3 bandas de frecuencia, 2 en VHF y una en UHF para la distribución de TV por aire, asignando un ancho de banda por canal de 6 MHz.

Con ello se definen normas N ó M, según la frecuencia de la red eléctrica ya sea de 50 Hz ó 60 Hz, para todos sus países aún que empleen normas de transmisión NTSC o PAL.

A. 11. 4. 1. Espectro de frecuencias en TV por aire

Los mayores inconvenientes se tienen en las bajas frecuencias hasta aproximadamente 40 MHz, donde se presentan ruidos e interferencias de los distintos artefactos domésticos, teléfonos, moto- res, etc., luego en la práctica se parte desde los 50 MHz. La transmisión por aire de TV hace uso de tres bandas de RF, dos de VHF y una de UHF, para transmitir un máximo de 82 canales.

La primer banda de VHF corresponde a los canales 2 al 6, con frecuencias entre 54 y 88 MHz, sal- vo la banda entre 72 á 76 MHz que separa al canal 4 del 5, reservada para astronomía y astrona- vegación. La segunda banda VHF corresponde a los canales 7 al 13, con frecuencias entre 174 y 216 MHz. Mientras que la banda UHF corresponde a los canales 14 al 83, con frecuencias entre 470 y los 890 MHz (Fig. 23).

VHF	FM	VHF	UHF


54 MHz    72  76	88	108	174	216	470	890

Fig. 23 - Espectro de RF para TV por aire

A. 11. 4. 2. Espectro de frecuencias en TV por cable

La TV por cable (CATV), para una red híbrida de fibra óptica y cable coaxial, por razones de com- patibilidad heredó parte de la asignación anterior, aunque se aprovechó en agregar el uso de todas las bandas intermedias (entre el canal 6 y el 7 en VHF y por encima del canal 13 en VHF/UHF. Así se adicionan 11 canales numerados A-2, A-1, y del 14 al 22 en la banda de 108 a 174 MHz, con opción de agregar tres mas, A-3, A-4 y A-5, de no usarse la banda de 88 MHz a 108 MHz para la transmisión de FM.

Se añaden también canales numerados a partir del 23 en 216 MHz, espaciados en 6 MHz entre sí. El canal superior no está establecido dependiendo del ancho de banda de la red y de número de canales que el operador quiera proveer. La numeración asignada por la asociación nacional de te- levisión por cable (NCTA), de USA, como así, la asignación estándar y el detalle de los servicios posibles de brindar, de acuerdo al espectro de RF se muestra en la Fig. 24 y la tabla adjunta.



(1)

(2)

(3)

(4)

(5)

(6)

(7)

(8
)

(9)

(10)

(11)

(12)

(13)
5 MHz  5,75  11.75  17.75  23.75  29.75  35.75	42	54	450	550	750	850	1000
Referencias:
Fig. 24 - Espectro de RF para CATV (Red FO / Cx)
(1) Retorno para el monitoreo de la red (vía ascendente)
(2) Retorno del servicio de vídeo interactivo (vía ascendente)
(3) Retorno del servicio de conmutación de paquetes (vía ascendente)
(4) Retorno del servicio PCS (vía ascendente)
(5) Retorno del servicio de telefonía (vía ascendente)
(6) Retorno del servicio de telefonía y datos (vía ascendente)
(7) Retorno del servicio de datos (vía ascendente)
(8) Cruce filtro duplexor
(9) Servicio de canales analógicos - 2 a 61 (vía descendente)
(10) Servicio de canales analógicos - 62 a 78 (vía descendente)
(11) Servicios digitales al cliente (vía descendente)
(12) Cruce filtro duplexor
(13) Retorno futuro (vía ascendente)


DETALLE DEL ESPECTRO DE RF PARA TV POR AIRE

	

	

A. 11. 4. 3. Parámetros en canales descendentes

De los parámetros que caracterizan a una red de CATV el mas importante es el ancho de banda. Las redes en uso actualmente disponen de 450 MHz con un máximo de 65 canales de CATV, de 550 MHz con un máximo de 79 canales de CATV o de 750 MHz.


En esta última categoría se divide el ancho de banda disponible en dos partes, hasta 550 MHz para difusión de TV, correspondiente a los 79 canales de CATV y la banda desde 550 MHz hasta 750 MHz para servicios digitales.

A. 11. 4. 4. Espectro de frecuencia en una red fibra/coaxial

Resulta entonces el espectro de frecuencia para una red fibra / coaxial:

Banda de 5 a 42 MHz, para canales de retorno (ascendente).
Banda de 54 a 450 MHz, para 60 canales TV analógica y 5 de FM (descendente).
Banda de 552 a 750 MHz, servicios digitales al cliente (descendente).
Banda de 850 a 1 GHz, canales de retorno futuros (ascendente).
Ancho de banda total, de 750 KHz, que podrá llegar a 1GHz.

Dado que cada canal analógico que se agrega contribuye a empeorar la relación portadora / distor- sión en los restantes canales, el limitar la cantidad de canales analógicos evita incrementar esta distorsión. Por ello se prescinde de utilizar la banda comprendida desde 452 a 552 MHz.

Codificando los canales en forma digital, en la banda de 200 MHz disponible (550 a 750 MHz), se posibilitará transmitir hasta 500 canales de TV, accediendo mediante el aparato conversor de TV, el STB (Fig. 25).



Fig. 25 - Espectro de frecuencia para una red fibra / coaxial

A. 11. 4. 5. Parámetros de la red en canales de retorno

Un segundo parámetro de importancia en las redes de CATV, es la presencia de un canal ascen- dente, es decir de retorno y de la banda de frecuencia que se le asigna. Para brindar servicios in- teractivos es preciso que exista un camino de comunicación desde el domicilio del cliente hasta la cabecera. Las redes de CATV reservan típicamente la banda de bajas frecuencias, desde 5 MHz hasta 42 MHz para implementar el canal de retorno. También se considera para el canal de retorno, el uso de la banda de UHF, de 750 MHz hasta 1 GHz.
Las principales funciones de este canal de retorno son, monitoreo de los equipos, transmisión des- de el STB hacia la cabecera para servicios de PPV, mediciones de audiencia (rating), control de calidad de la señal, etc.

A. 11. 5. Conceptos técnicos


Hemos estudiado los componentes de red en forma genérica por lo que nos cabe detallar algunas consideraciones técnicas en particular. Trataremos los siguientes tópicos:

Capacidad portable de canal.
Sistemas de transporte de la señal.
Regulación FCC del espectro.
Medios de incrementar la capacidad de canales.
Mantenimiento de la planta CATV y minimizar deterioros.
Método de codificación.
Interfaz cable equipos del abonado.
Práctica de cable de fibra.

A. 11. 5. 1. Capacidad portable de canal

La capacidad portable de canales esta basada en el ancho de banda de radiofrecuencia (RF). El rango de frecuencias de operación, en sentido de la señal hacia el abonado, es caracterizada según tres tipos de sistemas, pequeño, mediano y grande.

En USA, los sistemas mas pequeños que trabajan desde 50 á 220 MHz, han sido construidos des- de mediados de los años 50 hasta los últimos de los 70. Cerca de los 220 MHz se utilizó para las áreas rurales o racimos de pequeños establecimientos comunitarios.

Son áreas servidas a mas de 30 Km, con 50 o mas amplificadores en cascada, extendiéndose des- de 80 á 800 m con un promedio de 10 000 usuarios. Estos sistemas  intervienen en menos del 10
% de sus redes.

RANGO DE FRECUENCIAS DE OPERACIÓN - SEÑAL AL ABONADO

Sistema
Ancho de banda (MHz)
Rango de RF (MHz)
Cantidad de canales
Pequeño
170 MHz
50 MHz-220MHz
12 á 22 (1 coaxial)

220 MHz
50 MHz-270MHz
30 (1 coaxial)
Mediano
280 MHz
50 MHz-330MHz
40 (1 coaxial)

350 MHz
50 MHz-400MHz
52 (1 cx.) -104 (2 cx)
Grande
400 MHz
50 MHz-450MHz
60 (1 cx.) -120 (2 cx)

500 MHz
50 MHz-550MHz
80 (1 coaxial)

700 MHz
50 MHz-750MHz
110 (1 coaxial)

950 MHz
50 MHz-1GHz
150 (1 coaxial)

Los sistemas de capacidad media operan en 330 y 400 MHz, con ancho de banda de 280 MHz y 350 MHz respectivamente. Los sistemas con 330 MHz permiten 40 canales mientras que 400 MHz libra 52 canales. Estos sistemas intervienen en cerca del 75 % de las redes de USA.


Sirven un ancho rango de comunidades, desde las áreas rurales con población de 5000 habitantes, hasta algunos de los mas grandes sistemas construidos a los fines de los años 70, con 50 000 habitantes. Los sistemas mas amplios cuentan en los ramales troncales con hasta 37 amplificado- res en cascada.

Los sistemas de gran capacidad operan con alto número de canales. Redes coaxiales de 400 MHz disponen hasta 52 canales o en 550 MHz con 80 canales. Se incluyen edificios con sistemas de 750

MHz con 110 canales. Con cables duales se encuentran hasta 120 canales El sistema Time Warner en Queen, New York, tiene una capacidad de 150 canales con 1 GHz.

Los sistemas de gran capacidad operan el 15 % de sus redes. Con sistemas de cables duales, de 450 MHz, sirven desde 50 000 a 150 000 clientes, con redes que se extienden desde 640 á 3200 Km. Se han realizado actualizaciones de sistemas de 550 ó 750 MHz, con 20 000 clientes, debido a la política competitiva.

Se espera el desarrollo de la televisión avanzada ATV (Advance Television) y de alta definición (HDTV), aunque la TV digital tomará un incalculable auge en el año 2010, fecha desde la cual será obligatoria la emisión digital en USA y Japón. Se piensa que similar actitud se tomará en el año 2006, por Europa.

Los sistemas troncales con gran número de amplificadores en cascada, obsoletos al momento son reemplazados por troncales de fibra óptica, que laboran a 1 GHz. Un ancho de banda de 1 GHz contiene 160 slots de 6 MHz. Cada uno que pueden ser utilizados para HDTV y nuevos servicios, como ser NVoD (con la repetición cada 15 minutos de una película de estreno).

A. 11. 5. 2. Normalización de las frecuencias de canales

Existen tres planes de normalización de frecuencias por canales:

El primer plan implica la emisión de televisión VHF.

El segundo plan, llamado Incrementally Related Carrier (IRC) fue desarrollado para eliminar los efectos de distorsión de tercer orden generados por los amplificadores repetidores de la señal de televisión al pasar a la planta de cable.

El tercero plan es el llamado Harmonical Related Carrier (HRC), creado para reducir fuertemente la distorsión dada por los amplificadores.
En general estos planes reducen las distorsiones producto de las interferencias de las frecuencias de sincronismo a la señal de portadora.

Varios son los métodos para transportar las señales desde las estaciones cabeceras a estaciones remotas o concentraciones:

Enlace por microondas modulado en amplitud (AML).
Enlace por microondas modulado en frecuencia (FML).
Enlace por coaxial modulado en frecuencia (FMCL).
Enlace por coaxial modulado en amplitud (AMCS).
Enlace por cable de fibra óptica.

El sistema de enlace de microondas modulado en amplitud AML (Amplitude Modulated Microwave Link) logra cubrir la recepción descendente en microondas, hasta 16 Km. Se requiere una antena receptora de microondas montada en una pequeña torre o poste telefónico suficiente. El receptor de microondas AML, convierte cada canal de televisión a su correspondiente frecuencia.

El sistema de enlace por microondas modulado en frecuencia FML (Frecuency Modulated Microwa- ve Lik) se emplea para cubrir distancias mayores que el sistema AML y donde se requiere que la relación señal ruido SNR (Signal to Noise Radio) exceda de 56 dB. La modulación por VSB-AM (Vestigial Sideband Amplitude) debe realizarse antes que la señal ingrese al cable de planta. Este sistema se utiliza cuando se desea transmitir programas locales a estaciones remotas.

El sistema de enlace por coaxial modulado en frecuencia de FMCL (Frecuency Modulated Coaxial Link) es una adaptación del enlace por microondas modulado en frecuencia FML adecuado a la transmisión por cable coaxial. Aquí también la modulación VSB-AM debe realizarse antes que la

señal ingrese al cable de la red de distribución. Su uso se refiere al transporte de programación desde su origen a las estaciones cabeceras, con señales de “calidad estudio” (broadcast).

El sistema por coaxial modulado en amplitud AMCS (Amplitude Modulated Coaxial Supertunks) se emplea para los enlaces con señales comprimidas por modulación lateral vestigial VSB-AM. Se usa entre estaciones cabeceras o cables troncales principales.

La aplicación inicial de un enlace por cables de fibras ópticas se realizó en 1988, en Orlando, Flori- da, USA, como alternativa de un enlace de microondas ya que las intensas lluvias perjudicaban la transmisión. Se utilizo la modulación FM. Una red de fibra análoga transporta de 6 á 12 canales por fibra. La meta es portar 60 canales en amplitud modulada superando los 20 Km. Las transmisiones digitales se utilizan cuando la señal de video es digital.

Las frecuencias utilizadas en USA, reguladas por la FCC a fin de cuidar que no se solapen con las utilizadas por la Federal Aviation Administration (FAA) de USA.

Estas van desde 108 á 137 MHz y desde 225 á 400 MHz. Puesto que la planta de CATV no es un sistema completamente sellado la FCC y la FAA desean mantener una separación de frecuencias entre señales portadas entre ambos sistemas. Los sistemas de 400 MHz afectados a operaciones sobre 30 canales condicionados por el FCC.

A. 11. 5. 3. Incrementar la capacidad de canales

Veremos varios medios para incrementar la capacidad de los canales y sus posibles servicios a brindar, pero ante todo la red deberá presentar características permisibles para ello.
Antes que nada se deberá efectuar el estudio de la red existente, diagnosticando sus carencias y programando sus nuevos desarrollos.

Actualización y reconstrucción de planta

Si el cable en uso esta en optimas condiciones, modificar o reemplazar los amplificadores troncales o de distribución ampliará la capacidad. Si el cable sufre grandes deterioros la planta deberá ser relevada completamente.

La actualización será definida como proceso de rehabilitación de la planta existente. Es el resultado de cambiar o modificar amplificadores y elementos pasivos tal como los splitters, los acopladores direccionales y cajas de derivación con tomas múltiples (multi-taps).

Podrá incluir, agregar fibra óptica desde la cabecera hasta algún barrio, conectar transformadores híbridos o renovar todos los elementos en el sistema conservando los cables alimentadores a los barrios. El objetivo será expandir el sistema incrementando la capacidad de canales en zonas ale- targadas, al menor costo.

La reconstrucción involucra el reemplazo de toda la planta externa. Es la solución mas costosa, sin embargo al estar la vieja red activa permite disminuir los trastornos a los clientes, conmutando en una sola operación directamente de la vieja a la nueva red. Una vez que la red ha sido actualizada o reconstruida, el cliente podrá agregar nuevos aparatos adicionando nuevas capacidades y servi- cios.
Factor de ruido y de distorsión

El diseño de la red debe tener en cuenta que cada elemento activo adicionado agrega un cierto grado de distorsión y ruido a la señal. Cualquier elemento no lineal, como ser un conector bimetáli- co produce distorsión. La principal contribución de distorsión es producida por los amplificadores puesto que estos al estar conectados en cascada acumulan sus perniciosos efectos sobre la señal.

El ruido podrá provenir de variadas fuentes. La mayor fuente de ruido esta constituida por el movi- miento térmico aleatorio en componentes electrónicos (ruido Random). Para un cable a 20ºC de

temperatura ambiente, el ruido térmico en un canal será de 1.1 mV ó -59.1 dBmV. Éste es conside- rado el mínimo valor de ruido o nivel de piso.

La relación en Watt permite la comparación de sistemas en señal ruido (S/R). Es mas frecuente el uso de la relación de la señal de portadora del canal al ruido (C/R), pues indica la incidencia del de ruido como efecto de interferencia sobre la porción de señal de video, del canal. En las siglas ingle- sas la relación de señal a ruido se expresa como SNR (Signal to Noise Ratio) y la relación de por- tadora a ruido como CNR (Carrier to Noise Ratio). Cuando el valor de C/R decrece la señal sobre pantalla se presenta como una lluvia. Un buen diseño mantendrá un valor a 46 dB, preferentemente entre 48 y 50 dB, cuando la relación es de 40 dB la señal es cuestionable. El incremento de la señal aumenta esta relación pero desafortunadamente también incrementa el nivel de distorsión.

La distorsión, causada en los amplificadores con electrónica sólida, afecta la señal de salida en su ancho de banda. Los modernos amplificadores cancelan las distorsiones por batidos de segundo orden compuesto CSO (Composite Second Order Beats), siendo entonces preponderante la distor- sión llamada de batido triple compuesto CTB (Composite Triple Beats).
Sus parámetros determinan la cantidad de amplificadores a colocar en cascada y la distancia a cu- brir. Luego el sistema de cable estará limitado por un piso dado por el factor de ruido, definido como portadora / ruido (CNR) y un techo dado por un factor de distorsión, definido como batido triple compuesto (CTB). Ello acota principalmente, la cantidad de amplificadores a colocar en cascada (Fig. 26).



Fig. 26 - Convergencia de los factores de distorsión y ruido

Para un sistema a 300 MHz con 46 dB de CNR y 53 dB de CTB permite colocar 46 amplificadores troncales. La relación señal a distorsión SDR (Signal to Distortion Ratio) determina las condiciones de los ramales de distribución, mientras que la relación señal ruido afecta al diseño de los cables troncales, aun cuando estos son actualmente suplantados por diseños con fibras ópticas.

Otros factores limitan el diseño del cable, como ser la distorsión en función de la carga de canales y el incremento de la atenuación en función de la frecuencia.

Retardo de grupo

Los amplificadores bidireccionales muestran un retardo de grupo como resultado del filtrado de banda. Tal filtrado ocurre en las estaciones cabeceras y concentraciones en los procesos de modu- lación de los canales. El efecto visible es la pérdida de resolución en pantalla. Los canales 2, 3 y 4 son especialmente afectados.

Reflexiones de las señales

Las reflexiones de la señal ocurren en la planta y en las llamadas microreflexiones. Están causadas por desadaptaciones de impedancias y se miden hallando los valores de pérdidas de retorno. Sus efectos son visibles en pantalla como una segunda imagen, llamadas imágenes fantasmas.

Ruido de fase

El ruido de fase es adicionado a la señal original a través de la modulación y conversión de fre- cuencias. Este ruido de fase, medido a 20 KHz desde la portadora de video en el canal de TV, pro- duce variación en el nivel de iluminación y cromatismo, apareciendo en pantalla un rayado de líne- as.

A. 11. 6. Alta capacidad en cable
La principal contribución al progreso en cable ha sido dada por la arquitectura híbrida fibra coaxial, el video digital y la compresión de video digital, que ha permitido el gran incremento de la cantidad de canales en cable dentro de un mercado de alta competición.

A. 11. 6. 1. Arquitectura híbrida fibra - coaxial

El estudio de la red de acceso para servicios de CATV se podrá efectuar dividiendo la misma en  tres sectores, el troncal, las ramas distribuidoras también llamadas de alimentación y el de bajada o acometida a la casa del abonado. El sector troncal está diseñado para cubrir extensas distancias, alrededor de los 15 Km. Cuando esta proyectado bajo cables coaxiales, se requiere instalar amplifi- cadores cada 600 m, y corresponde un metraje del 12% de la red.

El sector de distribución soporta las cajas de conexionado a los abonados. Tiene una longitud máxima de 2.4 Km. Debido a la energía perdida en la distribución, el nivel de potencia de alimenta- ción deberá ser aquí relativamente bajo. Esta potencia afecta a la región no lineal de los amplifica- dores, como resultado pocos amplificadores podrán ser utilizados en estos ramales antes que la distorsión produzca la degradación de la señal en la pantalla de los televisores. Estos ramales in- sumen cerca del 38% de cables de esta red.

Los cables de bajada están constituidos por delgados y flexibles cables coaxiales. Se instalan con una longitud máxima de 120 m, aunque típicamente tienen de 45 á 60 m. Corresponde en metraje a cerca del 50 % de la red. El sector de acometidas es la zona de mayor actividad pues es donde los abonados producen los constantes cambios de altas y bajas de servicio. Aproximadamente el 20% de los norteamericanos se mudan a una nueva residencia, todos los años.

Se calcula como promedio 53 % de penetración, con 75 hogares por cada 1609 m (una milla), con acometidas promedio de 48 m de longitud. Cerca del 45 % de los reclamos por mal servicio esta dado en las acometidas y sus conexiones. El 10 % de las acometidas son reemplazadas anualmen- te en USA.

Además las cajas distribuidoras no tienen perfectas impedancias de adaptación al cable, por lo tan- to la señal es reflejada produciendo sombras en las pantallas. Este efecto se reduce diseñando los ramales de forma tal que aparezcan pocos amplificadores en serie (Fig. 27).



Fig. 27 - Planta con superdistribución
Por otra parte, las redes troncales de fibra óptica alimentan sistemas coaxiales que podrán disponer desde algunos cientos hasta un par de miles de abonados. Estas redes híbridas permiten disponer mayor cantidad de abonados y servicios a un costo de instalación y mantenimiento reducido. No se limita el ancho de banda por los amplificadores en cascada de los troncales coaxiales y permite llegar con un alto nivel de la señal a los ramales alimentadores, evitando así que estos entren en la zona de distorsión de la señal.

Otra razón de conveniencia de las redes híbridas se aprecia con la disposición de fibra en longitu- des extensas del troncal (backbone).

Ello evita el ingreso de interferencias en un sector amplio de la red. Por otra parte la fibra permite aislar las interferencias producidas en cualquier ramal, entre ellos y hacia la estación cabecera (Fig. 28).



Fig. 28 - Sistema de CATV con troncal óptico

El sistema de cable Time Warner Quantum ha explotado una red de este tipo por varios años, con
1.047 GHz y 150 canales análogos. Por su costo muchas empresas mantienen aún sistemas de 750 MHz.

A. 11. 6. 2. Compresión digital


La compresión digital de video DVC (Digital Video Compresión) junto con la modulación digital ha dado máxima capacidad al cable. Su desarrollo se produjo al introducirse la TV de alta definición HDTV o la ATV.

La HDTV consume 30 MHz por cada color, rojo, azul y verde, es decir cerca de 100 MHz señales analógicas, lo que en transmisión de TV digital consume mas de un Gb/s. Como el FCC permite solo el uso de 6 MHz por canal, del espectro, luego ha sido necesario remover todas las señales redundantes para la formación de una imagen en movimiento. El resultado fue la creación del estándar de compresión denominado SDTV (Standard Definition Television).

Esta técnica es utilizada actualmente en transmisiones satelitales, con el estándar de compresión versión MPEG (Moving Pictures Expert Group). Este estándar parte de la premisa que el ojo huma- no es mas sensible al desplazamiento horizontal que al desplazamiento vertical. Parece ser que la actividad ancestral de la caza, sensibilizo esta aptitud. El que no disponía de esta cualidad no co- mía, llegando así estos genes a nuestros días.

La compresión de vídeo, se basa en las imperfecciones de la visión humana, para reducir la infor- mación cambiante por cuadro a enviar.

Los organismos reguladores ISO y UIT han normalizado tres métodos de compresión de imágenes: JPEG, MJPEG 1 y MPEG 2. Esta última norma se adopta para implementar la televisión digital. Los últimos niveles de calidad, alcanzan compresión de 1.5 Mb/s, equivalente a la televisión analógica. Comprende la compresión en vídeo, audio y multiplexación de distintos flujos de datos en un único almacenamiento y transmisión.

Otro factor que permitió el apretujar la densidad de la señal de un canal de TV en 6 MHz fue el método de modulación. La modulación de amplitud en cuadratura QAM (Quadrature Amplitude Mo- dulation) y la modulación con supresión de portadora denominada como lateral vestigial VSB, son los principales métodos utilizados. Estos métodos se conforman en distintas velocidades. Las dos mas utilizadas en CATV son la 254 QAM ó la 16 VSB.

Las frecuencias inferiores a 50 MHz se utilizan par las señales de retorno o canales ascendentes, hacia la central, en sistemas interactivos. Los canales analógicos cubren el espectro bajo de 50 á 450 MHz (ó 550 MHz), permitiendo disponer de 400 MHz que aloje a 66 canales, de 6 MHz cada uno. Los canales digitales cubren desde 450 MHz (ó 550 MHz) hasta 750 MHz (ó 1 GHz), permi- tiendo disponer de 200 MHz, que aloje a 33 canales, o de 300 MHz con 50 canales, o 550 MHz con 91 canales, de 6 MHz cada uno.

A. 11. 6. 3. Mercado competitivo

Tanto las redes HFC como los métodos de compresión DVC están disponibles a todos los operado- res tanto de CATV como de telefonía que podrán entrar en competencia con las redes de CATV. La liberación de los servicios permite luego establecer varias fuentes de competición:

Emisión directa satelital.
Liberalizar cualquier servicio de video a los operadores telefónicos.
Construcciones de plantas alternativas.
Televisión celular.
Microwave Multipoint Delivery System (MMDS).
Emisiones.
Distribución física.

En los finales de los años de 1990 el panorama que presentaran los servicios de CATV fue de in- crementar la importancia que juega la regulación gubernamental. Este rol limito lo que podría ser el hecho. También las posibilidades y comportamiento de sus competidores.

Respecto a la TV directa al hogar recién en 1992 logro compartir la programación con CATV. Su política es brindar inicialmente la misma programación, con mayor calidad de señal y casi al mismo precio, adicionando nuevas ideas y alternativas.

A. 11. 7. Servicios adicionales a la CATV

Aprovechando el ancho de banda de la red cableada híbrida coaxial - fibra óptica, infinidad de ser- vicios se podrán ofrecer a los clientes de TV por cable. Sobre esta red se podrán proporcionar va- rios servicios adicionales al inicial de distribución de TV, VoD ó el PPV.

Un servicio de juegos interactivos y el servicio de Internet son los inmediatos a ofrecer, presente en el mercado. Una vez conectada la computadora a la red CATV, le sigue el formidable servicio de telecomunicaciones, competición directa a los operadores de telefonía básica. Luego, le siguen los servicios de datos de alta velocidad, de video interactivo, la telemedición, televigilancia, teleacción, telecompras y telebancos (homebanking).

Completado el panorama divisado al momento, seguramente surgirán muchos mas servicios en un futuro cercano.

A. 11. 7. 1. Servicio de telefonía

El servicio de telefonía es brindada por medio de la red de CATV, para lo cual se requiere instalar equipos en la cabecera y en la casa del cliente. En la cabecera se deberá vincular sus equipos a los equipos de la red pública de telefonía conmutada PSTN (Public Switching Telephone Network) ubi- cados en la central mas próxima. Este vínculo se constituirá de acuerdo al tráfico a transmitir, por cable coaxial o de fibra óptica.

En la casa del cliente se deberá instalar un aparato telefónico y un módem telefónico. El cliente recibe la señal de CATV y de telefonía a través del mismo cable. El problema reside en la alimenta- ción de energía eléctrica al aparato telefónico para la señal de llamada, etc. Para ello el tramo de fibra óptica deberá ser cubierto por un cable de energía paralelo al mismo, mientras que en el tramo distribuidor de coaxial, que ya requiere de telealimentación para sus amplificadores y de bajada coaxial al domicilio será solventado sobre la misma red, tal como es habitual en estas redes.

Otra posibilidad es la de tomar esta alimentación de la red de distribución de energía o de la misma casa del cliente. Ello tiene distintas desventajas, la legal del pago adicional de este gasto y la de baja confiabilidad, por no continuidad del mismo. Debido a tales razones no se usa esta posibilidad. En los casos de toma de energía centralizada se aumenta la eficiencia económica y asegura la con- tinuidad de la conexión por medio de un tren de baterías eléctricas.

La señal de telefonía deberá ser compatible con las señales del servicio de CATV, para evitar pro- ducir interferencias entre ellas. Para ello, se emplean esquemas de modulación que hacen apare- cer esta señal como ruido blanco, el que será fácilmente suprimido o atenuado.

Se emplean modulaciones tal como la QAM, modulación por amplitud en cuadratura y QPSK modu- lación por desplazamiento de fase en cuadratura, en ambos casos con supresión de portadora (modulación vestigial).

Otro problema reside en la asignación del ancho de banda. La ocupación promedio de un servicio telefónico es muy baja, menos de 20 minutos por día, ello nos indica que los canales asignados en forma estática estarán casi todo el tiempo sin utilizar.

Para evitar esto se requiere implementar la asignación dinámica de los canales telefónicos disponi- bles. Se podrá brindar así adicionalmente a la telefonía básica, el acceso básico de RDSI y la transmisión de datos a distintas velocidades.

El ancho de banda empleado es de 6 MHz en el canal descendente y de 6 MHz en el de retorno. Se emplean moduladores dinámicos, de manera de ubicar estas bandas en cualquier canal de TV o para aplicaciones digitales no ocupado.

A. 11. 7. 2. Servicio de datos de alta velocidad

La transmisión de datos a alta velocidad, del orden de 10 Mb/s, por una CATV, brinda la posibilidad de poder vincular eficazmente computadoras e incluso redes de área local LAN creándose redes locales virtuales VLAN, extendidas a una ciudad (Cap.22).

Estructuralmente la red de CATV tomaría la forma y uso del tipo Ethernet, punto a punto o conmu- tada. En ella se utilizan bridges, routers, y gateway, como una red LAN. El control de acceso se realiza mediante protocolos específico, 802.14 de la IEEE.

El acceso a servicios como el Internet, en alta velocidad, permite mejorar su utilización respecto al acceso telefónico de banda ancha. Al igual que el servicio de telefonía en redes CATV, para evitar producir interferencias se deberá adoptar modulación QAM. También como en ese caso, el ancho de banda empleado es de 6 MHz en el canal descendente y de 6 MHz en el de retorno, empleándo- se moduladores dinámicos.

A. 11. 7. 3. Servicio de vídeo interactivo

La televisión interactiva (TVI), también llamada Vídeo Dial Tone (VDT), es un contenedor de servi- cios, siendo su función en esencia la de permitir a todos ellos, convivir en un solo sistema. Además la TVI permite al cliente el control del contenido y el telemanejo con su proveedor. En realidad es una familia de servicios: vídeo bajo demanda (VoD), o de cuasi video sobre demanda NVoD , tele- bancos, juegos interactivos, telecompras, teleaprendizaje, periódico interactivo, noticias bajo de- manda, etc. Se trata de la evolución de la computación interactiva y de la CATV.

El servicio de VoD permite la recepción de una película del tipo premier (premium) o cualquier pro- grama codificado, en cualquier tiempo, con completa función virtual. Esto es poder adelantar, atra- sar, poner en estado de pausa, etc. un programa solicitado, tal como si se dispusiese de una video- casetera en el hogar del abonado.

El servicio de NVoD se acerca a las funciones de VoD. El NVoD se basa en la selección de los 5 títulos mas solicitados, generalmente de estreno. La transmisión de estos programas comenzará cada media hora, por distintos canales. Con una duración de 2 horas se requerirán solo 4 canales. El abonado tendrá que esperar un promedio de 15 minutos, menor tiempo al requerido para alquilar y devolver una película.

Tomando la posibilidad de utilizar un ancho de banda desde 450 á 750 MHz, es decir de 300 MHz, se dispone de 50 canales de 6 MHz. Si comprimiésemos las películas con un esquema de modula- ción tal como el VSB, 256 QAM ó el 64 QAM nos permitirá obtener respectivamente 38.5, 38.5 ó 27 Mb/s y con tramas de datos a 3 Mb/s será posible portar respectivamente 12, 12 ó 9 películas en cada ranura de frecuencia (slot). Puesto que disponemos de 50 canales, es decir 50 slot, habrá un total respectivamente de 600, 600 ó 450 películas ó canales virtuales.

Todos estos servicios son bidireccionales los que en muchos casos son asimétricos.

En dirección descendente la modulación típica es de amplitud en cuadratura, con una constelación de 64 puntos (64 QAM), también el 256 QAM o el banda lateral vestigial 8 VSB. Se obtiene veloci- dades de comunicación de mas de 20 Mb/s en un canal de 6 MHz de ancho de banda.
En la dirección de retorno no se permite moduladores de alta eficiencia espectral por  ser este canal altamente ruidoso. Típicamente se utiliza la modulación QPSK, por desplazamiento de fase en cua-

dratura, con pequeña eficiencia espectral de 2 bit / Hz. Ello no representa problema debido al flujo de información relativamente bajo.

La técnica que solvente la interconexión de equipos servidores de distintas jerarquías y entre distin- tos proveedores de servicios será el modo de transferencia asincrónico ATM. Esta es una técnica de conmutación de información, organizada en forma de celdas de tamaño reducido que alcanza grandes velocidades. Sin embargo, el auge de Ethernet y del IP, están reemplazando el empleo de la tecnología ATM, mucho mas onerosa.

La telefonía iteractiva hace uso de aparatos con alta capacidad de almacenamiento, como servido- res de vídeo. Estos equipos son los Set Top Box, STB, como símil computadoras que reemplazan el teclado por el control remoto y el monitor por el televisor. Estos equipos están optimizados para el manejo de interfaces gráficas y descompresión de vídeo digital en tiempo real.

Cabe la discusión si este camino a seguir por la TVI es la correcta, es decir llevar la computación al aparato de TV o si por el contrario será llevar la TV, dentro de las PCs (Personal Computers). Por una parte la TV ha sido creada como entretenimiento y disipación tanto individual como familiar, mientras que la TVI es al igual que la PC un servicio enteramente individual, corresponderá la se- gunda opción. También el manejo, formación y conformación de los datos de computación difíciles de llevar al televisor, nos indica tal conveniencia. Ciertamente ambas opciones prevalecerán, la TV con su STB, cada vez mas complejo y los multimedios en la PC.

A. 11. 7. 4. Servicio de Internet

El servicio de Internet por cable tiene uno de los mas ambiciosos desarrollos. Permite simétrica capacidad en ambas direcciones. El abonado podrá operar obteniendo todos los servicios de video, voz y datos que desee y además enviarlos a quien él desee.

Para proveer comunicaciones de datos sobre redes de CATV, se utiliza un canal de 6 MHz sobre cable coaxial, equivalente a un canal de TV. El servicio Internet, al igual que la TV, estará disponi- ble en cuanto se encienda la computadora, sin necesidad de marcar número alguno. Cada módem para cable de fibra óptica o coaxial tiene una salida a esa red y un conector (port) tipo Ethernet, para su acoplamiento a una red LAN o una computadora con software para red Ethernet. Para su conexión a Internet además deberá disponer de software TCP/IP.

Los módem diseñados para redes de TV, toman la señal de salida de una computadora u otro dis- positivo de datos y la transmite por la red en alta velocidad, mucho mayor que por la red telefónica. Se podrá implementar el servicio interactivo Internet por cable coaxial y / o fibra óptica, con altas velocidades simétricas en ambas direcciones. No obstante podrá optarse por velocidades descen- dentes (hacia el abonado), que varíen entre 64 Kb/s á 30 Mb/s, mientras que las ascendentes (hacia el operador del servicio) variar desde 64 Kb/s á 10 Mb/s. Estas señales son compartidas por los usuarios de la red según multiplexación estadística.

Por poseer tanto el sentido descendente como ascendente, el carácter de acceso múltiple, las re- des HFC detentan la necesidad de disponer convenientes medidas de seguridad.

El servicio de correo electrónico, e-mail, por Internet tiene un uso importante en oficinas y el hogar. Así como el facsímil digital, que podrán estar siempre activo sin requerir el uso de escáner para el ingreso de gráficos o textos impresos, a transmitir por la red de CATV.

A. 11. 7. 5. Otros servicios

Algunos servicios, por ejemplo telemedición, televigilancia o teleacción requieren de un ancho de banda mínimo, en cada sentido.

El servicio de telemedición consiste en tomar a distancia la medición de consumo de energía eléc- trica, de gas o agua, mediante un equipo ubicado en la central cabecera que interroga a cada me- didor domiciliario. La información digital es modulada en QPSF.


El servicio de televigilancia consiste en controlar equipos en forma remota. También los sistemas de alarmas podrán disponer de estos servicios. Los mismos se podrán complementar con cámaras de vídeo de baja resolución colocados en bancos y monitores en oficinas de control o comisaría.

El servicio de teleacción agrega la posibilidad de accionar a distancia válvulas de control de flujo de gas, desconectar aparatos de aire acondicionado o la operación de cualquier otro equipo o disposi- tivo en forma remota. El porcentaje satisfecho para este servicio es realmente alto, respecto a los hogares por los que pasa la ruta del cable es decir que cuenta con posibilidad cierta de servicio, hogares pasados.

A. 11. 8. Diseño de CATV en redes ópticas
Bajo el concepto de integración de las redes y el pasaje de red de CATV analógica a digital, efec- tuaremos el análisis de una red con el estudio como red de enlace digital.

Sus conceptos y método de cálculo son similares al tratamiento analógico, aunque incluiremos las diferenciaciones de características para los cálculos como ser, las pérdidas por inserción introduci- das en divisores y derivadores ópticos, para constituir las ramas de derivación de estas redes.

A. 11. 8. 1. Diseño de los enlaces ópticos

En los sistemas de enlaces ópticos se parte básicamente de los datos de la longitud a cubrir entre terminales y la capacidad de información a transportar, que a su vez define la cantidad de canales a implementar, por ende la velocidad a transmitir y su ancho de banda.

En el diseño se debe tener en cuenta varios factores:

a) Para garantizar la calidad de un sistema de fibra, se debe determinar la sensibilidad del receptor, es decir la cantidad mínima de luz que éste pueda detectar, según la energía entregada por el emisor óptico.

b) La diferencia entre la potencia del transmisor y la sensibilidad del receptor nos definirá la ganan- cia del sistema, que a su vez indica las pérdidas admisibles para un trayecto dado. Las pérdidas en el sistema están comprendidas por las pérdidas que introduce la fibra en su recorrido, los empalmes y conectores.

c) La ganancia del sistema establecida, depende de la velocidad de transmisión y de la longitud de onda, pues el emisor tiene un rango de longitudes de onda de trabajo dado y la sensibilidad del receptor depende de la velocidad de transmisión.

d) La velocidad binaria máxima que se pueda transmitir por el sistema dependerá del rango espec- tral del emisor, de la dispersión cromática de las fibras y de la longitud del trayecto.

e) Otros factores a tener en cuenta son las pérdidas ocasionadas en el medio ambiente, por ejem- plo las condiciones de temperatura, el envejecimiento de los materiales, las reparaciones even- tuales a realizar durante su vida útil y el posible crecimiento de canales con uso de sistemas múltiples, empleando multiplexado por división de longitud de onda, WDM.

El diseñador debe tener en cuenta también consideraciones de funcionamiento reales como ser el funcionamiento del emisor a longitudes de onda distintas a las óptimas para el cual fue diseñado, o velocidades de transmisión mayores, que hacen disminuir la sensibilidad de los receptores.

Es necesario definir los valores de ganancia del sistema y los puntos de medición exactos, por ejemplo los terminales ópticos en bastidores de la central, para evitar así omitir pérdidas o se cuen- ten en forma repetida.

Para los cálculos se podrán distintos criterios  según se trate de equipos de jerarquía digital plesió- crona, PDH (Pleisocronous Digital Herarchy) o equipamiento de jerarquía digital síncrona, SDH (Synchronous Digital Hierarchy). En caso de red plesiócrona PDH el criterio se refiere a tomar las especificaciones de equipos en las peores condiciones de trabajo o adoptando un método de elec- ción estadística.

En caso de red síncrona SDH el criterio se refiere a tomar las especificaciones de equipos y fibra para el peor caso. Esto para cumplimentar la compatibilidad longitudinal como la transversal. La primera considera la compatibilidad de equipos con el plantel de fibra, mientras que el segundo considera la compatibilidad de los entre diferentes proveedores. Esta posición es muy conservado- ra, pues presupone la degradación de todos los componentes del sistema, que se degraden al ex- tremo y todos en forma simultánea.

Casos particulares

Se ha desarrollado el método de cálculo en la posición de peor condición, para enlaces limitados por atenuación o dispersión. Se podrá efectuar la alternativa de cálculo tomando valores estadísti- cos de rendimientos de equipos y fibra proporcionados por los proveedores de equipos y del fabri- cante de los cables. Estos criterios son dados para casos particulares de excepción.

Calculo de un trayecto

Fundamentalmente para obtener el máximo alcance del trayecto se debe verificar:

Balance de potencia óptica en el sistema óptico, con estudio de las atenuaciones del pulso ópti- co originado a lo largo del trayecto.
Concordancia entre los valores de dispersión cromática generados por el trayecto óptico y los tolerados por el equipo receptor.

Recomendaciones de la UIT-T

Se respetará en los cálculos, los valores típicos o límites recomendados por la UIT-T.  Recomendación G.651, características de cables multimodo 50 / 125 m.
Coeficiente de atenuación entre 0.5 y 0.8 dB / Km a 1300 nm.
Coeficiente de dispersión cromática típica menor a 6 ps / (nm Km) a 1300 nm.  Recomendación G.652, características de cables monomodo.
El valor nominal del diámetro del campo modal a 1300 nm está comprendido entre 9 y 10 nm, de- pendiendo si se trata de revestimiento adaptado o con depresión.

Coeficiente de atenuación entre	0.3 y 0.4 dB / Km a 1300 nm.
0.15 y 0.25 dB / Km a 1550 nm. Coeficiente de dispersión cromática máximo de:
3.5 ps / (nm Km) entre 1285 y 1330 nm.
6	ps / (nm Km) entre 1270 y 1340 nm.
20  ps / (nm Km) en 1550 nm.  Recomendación G.653, características de cables monomodo con dispersión desplazada.
El diámetro del campo modal a 1550 nm está comprendido entre 7 y 8.3 nm. Coeficiente de atenuación entre 0.19 y 0.25 dB / Km en 1550 nm.

Coeficiente de dispersión cromática de 3 ps / (nm Km) entre 1525 y 1575 nm
Recomendación G.654, características de cables monomodo con perdidas minimizadas a 1550 nm. Coeficiente de atenuación entre 0.15 y 0.20 dB/Km.
Coeficiente de dispersión cromática máximo de 20 ps / nm Km.

Estudio de las atenuaciones

Se efectúa un balance de potencia óptica a la del trayecto, considerando las pérdidas introducidas en la fibra, empalmes y conectores, estimando los márgenes y penalidades a aplicar.

Los márgenes en el equipo consideran las degradaciones por efecto del tiempo como ser las varia- ciones de la sensibilidad del receptor. Se podrá establecer 3 dB para sistema con laser de tempera- tura estabilizada y detectores PIN. En los sistemas que utilizan diodos electroluminicentes o laser  no estabilizados el margen se puede aumentar en 2 dB.

Los fabricantes determinan las pérdidas máximas admisibles para sistemas, según la velocidad de trabajo, el tipo de emisor y la velocidad de transmisión, para una tasa de error de 1011. A este valor se le debe adicionar los márgenes respectivos.

Los márgenes se establecen principalmente:

En el cable, para prever posibles intervenciones a fin de repararlo. En los equipos, tener en cuenta degradaciones por envejecimiento.
En conectores y empalmes, por posibles degradaciones que pudiesen producirse.

Las penalidades se refieren a posibles desensibilización que pudiesen producirse en el receptor.

VALORES TÍPICOS DE PÉRDIDA

Velocidad Mb/s
Longitud de onda nm
Pérdida máxima dB
4 x 140
1300
24
4 x 140
1550
26
622
1300
21
622
1550
21
622
1550
37
2.5
1300
17
2.5
1300
25
2.5
1550
17
2.5
1550
25



Condición de balance óptico

La condición numérica que debe cumplir un enlace entre dos puntos, para satisfacer el balance óptico del sistema está dada por la relación:

Gsistema   Pa fibra + Pa conectores + Pa empalmes + Márgenes + Penalidades   [1]

Con G como ganancia y Pa como perdidas por atenuaciones.

La atenuación en la fibra es proporcional a la atenuación de la fibra por unidad de longitud y a la longitud del cable Lc :

Pa fibra  =  fibra Lc   [2]

La atenuación introducida por los conectores y empalmes es proporcional a la cantidad presente de estos en la línea. Se toman los puntos de referencia S y T (Fig. 29).



Fig. 29 - Puntos de referencia para el cálculo óptico

La expresión [1] se podrá utilizar para la verificación de un enlace mientras que si reemplazamos en ella la [2], los valores de la curva característica de margen en el cable respectivo y despejamos el valor de Lc, podremos obtener el máximo alcance para un sistema dado de fibra y equipos

Lmáx at  = (Gsistema - Pa conectores - Pa empalmes + Márgenes + Penalidades) / fibra	[3]
Los márgenes del cable están dados por curvas características (Fig. 30).









Mo







Inclusión de atenuadores
Fig. 30 - Característica de margen en el cable

Para los ramales cortos debe satisfacerse la condición que la potencia de recepción, tomando to- dos los márgenes y penalidades como nulos, sea menor a la que provoque la sobrecarga del recep- tor.

Si aconteciese tal situación cabe dos soluciones:

Optar por un emisor de menor potencia.
Intercalar un atenuador óptico en el trayecto.

Estos casos será conveniente verificarlos en obra, de acuerdo a las mediciones realizadas.

Estudio de las dispersiones

El haz de luz tiene un determinado ancho espectral, que define el ancho del pulso originado. A me- dida que el pulso de luz se desplace por el trayecto se ensancha y ocupa el espacio entre símbolos consecutivos (intersímbolo). Llega un momento que un receptor no podría distinguir si se trata de un símbolo 1 ó 0.

Este efecto se denomina de dispersión, los componentes del efecto de dispersión son para una fibra multimodo modal y del material, mientras que en una fibra monomodo del material y de guía de ondas, haciéndose despreciable la modal. La suma cuadrática de las dispersiones debidas al material y guías de onda se denominan dispersiones cromáticas.

La velocidad de propagación es función de la longitud de onda. Si suponemos transmitir con una única longitud de onda, aún en transmisión monomodo, se produce superposición de modos de esa misma longitud de onda, por tomar vías de propagación diferentes. Este efecto produce también ensanchamiento del pulso, llamado efecto de dispersión cromática.

Se expresa en ps de ensanche del pulso por nm del ancho espectral del transmisor y por Km de recorrido en el trayecto de la fibra. Se podrá adoptar como valor aceptable de dispersión cromática, un cuarto del intervalo del pulso T.

0.25 T > Coef. dispersión [ps / (nm Km)] x long. [Km] x ancho espectral fuente [nm]


Condición de dispersión cromática

A fin de que el enlace funcione correctamente debe cumplirse la relación:

[4]
donde:

fibra  = ensanchamiento del pulso óptico por dispersión L	= longitud total del cable
Dc  = dispersión cromática
= ancho espectral de la fuente

Considerando un equipo dado, es fijo luego puede escribirse

[5]

Donde:

Dmáx es un parámetro característico del equipo e indicativo del grado de dispersión que tolera. Su magnitud se expresa en:

[ Dmáx ] = ps / nm

La expresión [5] se utiliza para verificar el cumplimiento de la dispersión cromática en un enlace. En cambio para calcular el máximo alcance en términos de dispersión, podrá emplearse la relación:



Donde:

Lmáx es la longitud máxima que podrá tener un cable, suponiendo que el mismo está
limitado por dispersión.

La longitud máxima del cable adoptado será dada por la longitud resultante por atenuación [3] o la longitud resultante por dispersión [6], donde se adopta la menor de ellas.

Estudio de los códigos de línea

Básicamente se utilizan tres tipos de código de línea, el NRZ, el RZ o su variación denominada mb/nb. Si el sistema esta limitado en atenuación se usará NRZ y si está limitado en dispersión se usará el código RZ.

Para cada código de línea a utilizar se deberá cumplir la condición:


Donde:

VTx = Velocidad de transmisión.
L	= Longitud del trayecto.
K	= Constante según código a utilizar.  Dc   = Coeficiente de dispersión cromática.
= Ancho espectral de la fuente.

1) Para el código NRZ se adopta K = 250.
[7]

2) El código RZ permite el doble de dispersión que el código NRZ, luego K = 500.

3) Si se utiliza un código Manchester o el código por inversión de señal CMI (Code Mark Inversion) se tiene una pérdida mayor, luego el receptor deberá detectar pulsos ópticos igual a la mitad respecto al código NRZ. Se debe adoptar entonces K = 125.

Metodología de cálculo

Vimos que dos son las limitaciones atenuación y dispersión para cubrir un trayecto óptico. Debere- mos verificar para nuestro caso cual de los dos es el mas limitativo.

La metodología practica a utilizar será hallar los valores limitantes de dispersión y atenuación, con- siderando el código de línea a emplear en el sistema y aplicando las condiciones dadas por [7], donde intervienen la velocidad de transmisión, el coeficiente de dispersión cromática y el ancho espectral de la fuente.


Análisis de las limitaciones por atenuación

Sabiendo que


Con:

G = ganancia

M = márgenes
Pa = pérdidas por atenuación
Los márgenes de seguridad para la atenuación en la fibra se puede considerar adicionar 0,04 dB / Km para 1300 y 1550 nm por degradación, mas 0.05 dB / Km para compensar las variaciones de la longitud de onda. Además se considerarán los correspondientes a temperaturas extremas dadas por el fabricante si ese fuese el caso.

Igual condición para los empalmes, pudiéndose tomar 0.02 dB / Km. Las pérdidas en el terminal óptico, como en cada conector, tipo FC / PC, podrán ser de 0.5 dB. También se considerarán las pérdidas por empalmes y longitudes de cable extra. La adopción futura de sistemas WDM, podrán introducir 1.5 á 2.5 dB.

Análisis de las limitaciones por dispersión

Si se conoce la distancia del trayecto se calcula velocidad de transmisión x distancia y se elige una fibra y emisor que cumpla ese producto, para un código de línea especificado. Inversamente si se quiere conocer la distancia a cubrir, al producto velocidad de transmisión x distancia determinado por el tipo de fibra y emisor dado, se divide por la velocidad de transmisión.

Ejemplos de aplicación

Consideremos el cálculo de la distancia máxima entre repetidores para un enlace óptico con fibra trabajando en tercera ventana 1550 nm, garantizando una probabilidad de error de 10-11.

Los parámetros utilizados son:

Velocidad de transmisión, VTx=		622 Mb/s. Potencia de transmisión, PotTx =	-5 dBm.
