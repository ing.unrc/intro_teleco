# Tabla de Rutas

Dado que la configuración de la tabla de rutas se realiza de forma global al equipo, los comandos se ejecutan directamente desde la raiz.

## Visualización de puerta de enlace/tabla de rutas

```bash
<Huawei> system-view
[Huawei] display ip routeing-table protocol static
```

## Configuración de puerta de enlace (Gateway)

```bash
<Huawei> system-view
[Huawei]ip route-static 0.0.0.0 0.0.0.0 "z.z.z.z"
```

Donde: | |
-- | --
**z.z.z.z** = dirección IP de la puerta de enlace |

## Carga de rutas en forma estática

```bash
<Huawei> system-view
[Huawei]ip route-static "x.x.x.x" "y.y.y.y" "z.z.z.z"
```

Donde: | |
-- | --
**x.x.x.x** = dirección de red a alcanzar | **y.y.y.y** = máscara de red
