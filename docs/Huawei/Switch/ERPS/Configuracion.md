# Configuración ERPS

Guía de referencia [enlace](https://support.huawei.com/enterprise/en/doc/EDOC1000178415/2d977e91/creating-an-erps-ring)

## Creación de un anillo ERPS, configuración de una VLAN de control y asignación de instancias

En el anillo ERPS, una vlan de control (compartida en todos los equipos) es utilizada sólo para el intercambio de RAPS PDUs, no para datos. Distintos anillos pueden usar diferentes vlans de control.
Esta vlan no tiene que haber sido creada o usada.

```
<Huawei>system-view
[Huawei]erps ring "#r"
[Huawei-erps-ring"#r"]control-vlan "#vc"
[Huawei-erps-ring"#r"]protected-instance "#i"
``` 

| Donde: | |
| -- | -- |
| **#r** = número de anillo | **#vc** = identificador de vlan de control |
| **#i** = número de instancia / puede utilizarse "all" (todas)| |

* Si el anillo ERPS contiene puertos, la vlan de control no puede cambiarse. para ello, primero se debe eliminar la configuración erps. 

###  Creación de instancias y mapeo con VLANs

```bash
<Huawei>system-view
[Huawei] stp region-configuration
[Huawei-mst-region] instance "#i" vlan "#v"
[Huawei-mst-region] active region-configuration
[Huawei-mst-region] quit
```

Donde: ||
-- | -- |
**#i** = número de instancia | **#v** = número de vlan |

## Adición de un puerto a un anillo y configuración del rol

Prerrequisitos:

Si la interfaz es un puerto de capa 3, ejecutar el comando **switchport** para cambiarlo al modo capa 2.
The port is not a Layer 3 port. If the port i

Los protocolos Spanning Tree Protocol (STP), Rapid Ring Protection Protocol (RRPP), Smart Ethernet Protection (SEP) o Smart Link deben estar deshabilitados en el puerto:

* Si STP está habilitado, ejecutar **stp disable** dentro de la interfaz.

* Si RRPP está habilitado, ejecutar **undo ring ring-id** dentro del dominio RRPP.

* Si SEP está habilitado, ejecutar **undo sep segment "segment-id"** dentro de la interfaz.

* Si Smart Link está habilitado, ejecutar **undo port** dentro en el grupo Smartk Link.

The control-vlan command has been executed to configure a control VLAN and the protected-instance command has been executed to configure an ERP instance.

### Deshabilitar stp y configuración de modo troncal

```
<Huawei>system-view
[Huawei]interface "tipo" "#i"
[Huawei-interfaz#i]port link-type trunk
[Huawei-interfaz#i]port trunk allow-pass vlan "#v"
[Huawei-interfaz#i]erps ring "#r" (rpl owner)
[Huawei-interfaz#i]stp disable
[Huawei-interfaz#i]undo shutdown
```

| Donde: | |
| -- | -- |
| **tipo** = tipo de interfaz | **#i** = identificador de la interfaz | 
| **#v** = Número de vlan | **#r** = número de anillo |
| (rpl owner) en caso que sea el puerto a deshabilitar| |

### 
