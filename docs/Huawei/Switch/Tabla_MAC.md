# Tabla MAC
## Visualización Tabla MAC

```bash
<Huawei>system-view
[Huawei]display mac-address
MAC address table of slot 0:
-------------------------------------------------------------------------------
MAC Address          VLAN/       PEVLAN CEVLAN  Port        Type     LSP/LSR-ID  
                     VSI/SI                                          MAC-Tunnel  
-------------------------------------------------------------------------------
"xx:xx:xx:xx:xx:xx"  "#"           -      -     "interfaz"   "tipo"      
-------------------------------------------------------------------------------
```

Donde: | |
-- | --
**xx:xx:xx:xx:xx:xx** = dirección MAC | **#** = identificador de vlan
**interfaz** = interfaz de red desde la que se alcanza la dirección MAC | **tipo** = indica si la entrada en la tabla es estática o dinámica

## Configuración estática en Tabla MAC

```bash
<Huawei>system-view
[Huawei]mac-address static "dirección mac" "tipo" "número de interfaz" vlan "#"
```

Donde: | |
-- | --
**dirección mac** = Dirección MAC del dispositivo conectado | **tipo** = Tipo de interfaz
**número de interfaz** = Número dentro del equipo | **#** =  Número de vlan
