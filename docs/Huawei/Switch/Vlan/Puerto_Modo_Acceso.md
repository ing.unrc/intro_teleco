# Configuración modo Acceso

## Asignación a una interfaz

Para esta configuración, es necesario primero indicar que la interfaz es modo acceso y luego asignarle una vlan:

```bash
<Huawei>system-view
[Huawei]interface "tipo" "#i"
[Huawei-interfaz#i]port link-type access
[Huawei-interfaz#i]quit

[Huawei]vlan "#v"
[Huawei-vlan#]port "tipo" "#i"
```

Donde: || |
-- | -- | -- |
**tipo** = tipo de interfaz | **#i** = identificador de la interfaz | **#v** = número de vlan |

## Asignación a un grupo de interfaces

```bash
<Huawei>system-view
[Huawei]port-group "nombre"
[Huawei-port-group-nombre]group-member "tipo" "#i" to "tipo" "#i"
[Huawei-port-group-nombre]port link-type access
[Huawei-port-group-nombre]quit

[Huawei]vlan "#v"
[Huawei-vlan#]port "tipo" "#i" to "#i"
```

Donde: ||
-- | -- |
**nombre** = nombre asignado al grupo | **tipo** = tipo de interfaz |
 **#i** = identificador de la interfaz | **#v** = número de vlan |
