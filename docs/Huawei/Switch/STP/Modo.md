## Modos de funcionamiento:

Dentro de los modos de funcionamiento se encuentran disponibles:

* mstp - Multiple Spanning Tree Potocol (por defecto)
* rstp - Rapid Spanning Tree Potocol
* stp - Spanning Tree Potocol

Por defecto, se implementa **mstp**

```bash
<Huawei>system-view
[Huawei]stp mode "stp/rstp/mstp"
```

Donde: | |
-- | -- 
**stp/rstp/mstp** = modo de funcionamiento | 
