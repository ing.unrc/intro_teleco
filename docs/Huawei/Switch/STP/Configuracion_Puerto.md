# Configuración de STP en puertos

## Habilitar/Deshabilitar STP

PRECAUCIÓN: Deshabilitar STP en interfaces puede causar bucles dentro de la red a nivel de capa 2 y con ello su colapso. 
Por defecto, las interfaces cuentan con STP habilitado.

```bash
<Huawei>system-view
[Huawei] interface "tipo" "#"
[Huawei-interfaz#i] stp "disable/enable"
```

Donde: ||
-- | -- |
**tipo** = tipo de interfaz | **#i** = identificador de la interfaz |
**disable/enable** = habilita/deshabilita STP |

## Costo

```bash
<Huawei>system-view
[Huawei] interface "tipo" "#"
[Huawei-interfaz#i] stp "costo"
```

Donde: ||
-- | -- |
**tipo** = tipo de interfaz | **#i** = identificador de la interfaz |
**costo** = costo asociado a la interfaz |

## Prioridad

```bash
<Huawei>system-view
[Huawei] interface "tipo" "#"
[Huawei-interfaz#i] stp port priority "prioridad"
```

Donde: ||
-- | -- |
**tipo** = tipo de interfaz | **#i** = identificador de la interfaz |
**prioridad** = prioridad asociada al puerto (0-240) |
