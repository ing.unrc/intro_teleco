# Opciones de visualización de STP

## Visualización del estado de STP

```bash
display stp brief

MSTID   Port         Role    STP State   Protection
        "Interfaz"   "Rol"   "Estado"    
```

Donde: | |
-- | -- 
**Interfaz** = Interfaz | **Rol** = rol asignado a la interfaz
**Estado** = | 

## Visualización de STP por interfaz

```bash
display stp interface "tipo" "#i"
```

Donde: ||
-- | -- |
**tipo** = tipo de interfaz | **#i** = identificador de la interfaz |

## Visualización de STP por vlan

```bash
display stp vlan "#v"
```

Donde: ||
--|--
**#v** = número de vlan ||
