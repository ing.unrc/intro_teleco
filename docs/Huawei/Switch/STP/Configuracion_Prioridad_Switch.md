## Configuración Básica Prioridad Switch

```bash
<Huawei>system-view
[Huawei]stp root "primary/secondary"
```

Donde: | |
-- | -- 
**primary/secondary** = asigna prioridad primaria o secundaria | 
