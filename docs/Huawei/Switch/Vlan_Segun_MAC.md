# Asignación de vlans según dirección MAC

La asociación de vlans en este caso no se realiza según puertos del switch, si no según la dirección MAC de las tramas.
Para esto, es necesario acceder a la sección vlan. La vlan en cuestión puede ser creada con anterioridad o al momento de hacer la asignación. 
Dentro de la vlan se cargan cuales son las direcciones MAC asociadas y la proioridad asignada a las mismas.
Luego es necesario habilitar "mac-vlan" dentro de la interfaz a la que está conectado cada dispositivo (esta interfaz funciona en modo híbrido):

```
system-view
vlan "#"
mac-vlan mac-address "xxxx-xxxx-xxxx" priority "prioridad"
quit

interface "tipo" "numero interfaz" 
port hybrid untagged vlan "#"
mac-vlan enable
```

Donde: ||
-- | --
**#** = Número de vlan |
**xxxx-xxxx-xxxx** = Dirección MAC (Agrupar de a dos octetos) | **prioridad** = prioridad (802.1p) - opcional -
**tipo** = tipo de interfaz | **numero interfaz** = número de la itnerfaz en el sistema