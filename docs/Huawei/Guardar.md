# Guardar configuración

Para guardar la configuración, una opción es utilizar el comando "save" de la forma:

```bash
[Huawei]quit
<Huawei>save
The current configuration will be written to the device.
Are you sure to continue?[Y/N]y
```
