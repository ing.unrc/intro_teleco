#Asignación de nombre al dispositivo

Para identificar fácilmente el un equipo, se recomienda cargarle un nombre, el cual luego será que lo identifique en la consola:

```bash
<Huawei> system-view
[Huawei] sysname "NombreEquipo"
["NombreEquipo"]
```

Donde: |
-- |
**NombreEquipo** = Nombre que se le asigna al dispositivo |
