#Cliente DNS 

```
Router> enable
Router# configure terminal
Router(config)# ip name-server "d.d.d.d"
Router(config)# ip domain lookup
```

Donde: | |
-- | --
**d.d.d.d** = dirección IP del servidor DNS | |
