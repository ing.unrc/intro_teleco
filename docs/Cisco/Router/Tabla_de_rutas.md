# Tabla de Rutas

## Configuración de puerta de enlace (Gateway)

```
Router> enable
Router# configure terminal
Router(config)# ip route 0.0.0.0 0.0.0.0 "z.z.z.z"
```

Donde: | |
-- | --
**z.z.z.z** = dirección IP de la puerta de enlace |

## Carga de rutas en forma estática

```
Router> enable
Router# configure terminal
Router(config)# ip route "x.x.x.x" "y.y.y.y" "z.z.z.z"
```

Donde: | |
-- | --
**x.x.x.x** = dirección de red a alcanzar | **y.y.y.y** = máscara de red

## Visualización de puerta de enlace/tabla de rutas

```
Router> enable
Router# show ip route
```
