# Tabla MAC

## Visualización Tabla MAC

```
Switch# show mac address-table
Mac Address Table
-------------------------------------------------
Vlan     Mac Address           Type         Ports
----     -----------           --------     -----
"#v"     "xx:xx:xx:xx:xx:xx"   "tipo"       "interfaz"
```

Donde: | |
-- | --
**#v** = identificador de vlan | **xx:xx:xx:xx:xx:xx** = dirección MAC
**tipo** = indica si la entrada es estática o dinámica | **interfaz** = interfaz desde la que se alcanza la dirección MAC
