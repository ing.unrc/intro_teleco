## Visualización puertos en modo acceso

```
Switch# show vlan
VLAN     Name       Status     Ports
----     ------     ------     -------------------------------
"#v"     "nombre"   "estado"   "puertos"
```

Donde: | |
-- | --
**#v** = número de vlan | **nombre** = nombre de la vlan 
**estado** = activo/inactivo | **puertos** = interfaces asociadas a cada vlan

**Importante:** en caso de utilizar un equipo Etherswitch Router Cisco 3725, el comando es: **show vlan-switch**

## Visualización puertos en modo troncal

```
Switch# show interfaces trunk 
Port        Mode         Encapsulation  Status        Native vlan
"puertos"   on           "protocolo"    trunking      1
```

Donde: | |
-- | --
**puertos** = interfaces asociadas a cada vlan | **protocolo** = protocolo de encapsulación |
