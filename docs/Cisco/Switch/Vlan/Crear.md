## Crear una vlan

Por medio del comando que se presenta a continuación, es posible crear una vlan, a partir de un número de identificación y asignarle un nombre:

```
Switch(config)# vlan "#v"
Switch(config-vlan)# name "nombre"
```

Donde: | |
-- | --
**#v** = número de vlan | **nombre** = nombre de la vlan
