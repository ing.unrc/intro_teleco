# Puertos en modo troncal

En este caso, inicialmente hay que indicar que el puerto será troncal (que permite el paso de VLANs etiquetadas) y luego habilitar cuál/es VLANs pueden ser traficadas:

## Configuración de puerto en modo troncal

Inicialmente se debe indicar el puerto o rango de puertos que se ha de configurar en modo troncal y el tipo de encapsulación:

```Cisco
Switch> enable
Switch# configure terminal
Switch(config)# interface "interfaz" "#i" 
* Switch(config-if)# switchport trunk encapsulation "encapsulacion"
Switch(config-if)# switchport mode trunk
```

\* este paso depende del modelo del equipo. En switches Cisco 2950 no se aplica

Donde: | |
-- | --
**interfaz** = nombre lógico de la interfaz a configurar | **#i** = número de interfaz
**encapsulacion** = modo de encapsulación: seleccionar **dot1q** (802.1Q)

### Permitir el paso de todas las VLANs en el puerto troncal

Luego de configurar un puerto en modo troncal, se puede permitir el paso de todas las VLANs configuradas en el equipo:

```Cisco
Switch(config-if)# switchport trunk allowed vlan all
```

### Indicar VLANs que pueden pasar por el troncal

En este caso se puede indicar solo 1 vlan, un conjunto de vlans (separadas por comas) o un rango (indicando el valor inicial y final, separado con un gión):

```Cisco
Switch(config-if)# switchport trunk allowed vlan add "#v"
```

Donde: | |
-- | --
**#v** = número de vlans que se permiten en la interfaz  |


### Quitar VLANs permitidas

En este caso se puede indicar solo 1 vlan, un conjunto de vlans (separadas por comas) o un rango (indicando el valor inicial y final, separado con un gión):


```Cisco
Switch(config-if)# switchport trunk allowed vlan remove "#v"
```

Donde: | |
-- | --
**#v** = número de vlans que se permiten en la interfaz  |

### Permitir todas excepto una VLAN

Se puede permitir cualquier trama etiquetada, salvo aquella que tenga la etiqueta indicada:

```Cisco
Switch(config-if)# switchport trunk allowed vlan exept "#v"
```

Donde: | |
-- | --
**#v** = número de vlans que se permiten en la interfaz  |

### No permitir el paso de VLANs

A partir del siguiente comando, no se permite el paso de tramas con etiquetado:

```Cisco
Switch(config-if)# switchport trunk allowed vlan none
```
