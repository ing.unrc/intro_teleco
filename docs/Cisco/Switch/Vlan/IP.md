# Asignación de una dirección IP a una VLAN

La asignación de una dirección IP puede ser de utilidad para tener acceso en caso de querer gestionar el dispositivo en forma remota o por medio de red.

## Asigna IP en forma manual

```
Switch(config)# interface vlan "#v"
Switch(config-if)# ip address "x.x.x.x" "y.y.y.y"
Switch(config-if)# no shutdown
```

Donde: | | |
-- | -- | --
**#v** = número de vlan | **x.x.x.x** = dirección IP | **y.y.y.y** = máscara de red | 

## Asigna configuración por DHCP

```
Switch(config)# interface vlan "#v"
Switch(config-if)# ip address dhcp
Switch(config-if)# no shutdown
```

Donde: | |
-- | --
**#v** = número de vlan |
