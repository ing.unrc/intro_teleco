# Configuración de parámetros en forma manual

## Configuración de prioridad en un  switch

```bash
Switch(config)# spanning-tree vlan vlan "#v" priority "prioridad"
```

Donde: | |
-- | --
**#v** = número de vlan a la que se le aplica la configuración | **prioridad** = se ingresa la prioridad indicada para ese switch. Múltiplos de 4096, de 0 a 61440.

## Configuración de port-priority

Configura la prioridad de un puerto Trunk para una VLAN:

```bash
Switch(config)# interface "interface-id"
Switch(config-if)# spanning-tree vlan "#v" port-priority port "prioridad"
```

Donde: | |
-- | --
**interface-id** = nombre de la interfaz | **#v** = número de vlan a la que se le aplica la configuración
**prioridad** = valor del prioridad (múltiplos de 16, de 0 a  240) |

## Configuración de PATH-COST

```bash
Switch(config)# interface "interface-id"
Switch(config-if)# spanning-tree vlan "#v" cost "costo"
```

Donde: | |
-- | --
**interface-id** = nombre de la interfaz a configurar | **#v** = número de vlan a la que se le aplica la configuración
**costo** = valor del costo (1 – 200.000.000) | 

## Configuración de tipos de puertos

Parámetro | Comando
-- | --
. | Switch(config)# interface "interface-id"
PORT FAST = inmediatamente forwarding | Switch(config-if)# spanning-tree portfast
ROOT GUARD | Switch(config-if)# spanning-tree guard root
BPDU GUARD | Switch(config)# spanning-tree portfast bpduguard default 
UPLINK FAST | Switch(config)# spanning-tree uplinkfast "tiempo"
BACKBONE FAST | Switch(config)# spanning-tree backbonefast

Donde: | |
-- | --
**interface-id** = nombre de la interfaz a configurar | **tiempo** = tiempo de espera para habilitarse
