# Priorizacion Básica

## Configuración de un switch como root primario

```bash
Switch(config)# spanning-tree vlan "#v" root primary "hello-time"
```

Donde: | |
-- | --
**#v** = número de vlan a la que se le aplica la configuración | **hello-time** = 1 a 10, por defecto 2 [segundos]. Intervalo en segundos entre la generación de los mensajes de configuración para un switch como Root.

## Configuración de un switch como secundario

```bash
Switch(config)# spanning-tree vlan "#v" root secondary "hello-time"
```

Donde: | |
-- | --
**#v** = número de vlan a la que se le aplica la configuración | **hello-time** = 1 a 10, por defecto 2 [segundos]. Intervalo en segundos entre la generación de los mensajes de configuración para un switch como Root.
