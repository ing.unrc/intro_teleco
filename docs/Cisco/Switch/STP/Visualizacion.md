# Visualización de configuración de STP

## Visualización general de STP

```bash
Switch# show spanning-tree
```

Como resultado se obtiene información de stp por cada vlan sobre:

* switch raíz.
* switch desde el cual se ejecuta el comando.
* estado de stp en cada una de las interfaces. 

## STP por vlan

Para visualizar información de stp en una vlan en particular se puede implementar el siguiente comando:

```bash
Switch# show spanning-tree vlan "#v"
```

Donde: | |
-- | --
**#v** = número de vlan a la que se le aplica la configuración |

Como resultado se obtiene información de stp de la vlan en particular sobre:

* switch raíz.
* switch desde el cual se ejecuta el comando.
* estado de stp en cada una de las interfaces. 


## STP por interfaz

Visualización del estado de stp para todas las vlans configuradas en la interfaz.

```bash
Switch# show spanning-tree interface "interfaz" "#i"
```

Donde: | |
-- | --
**interfaz** = nombre lógico de la interfaz a configurar | **#i** = número de interfaz

## Resumen

```bash
Switch# show spanning-tree summary 
```

Muestra un resumen de la configuración de stp en el equipo.
