# Cable de consola/serie

## Tipos de cables

A continuación se presentan dos tipos de cables que permiten la conexión al puerto **consola** de un dispositivo Cisco:

| Cable RJ45-DB9 | Cable adaptador RJ45-DB9 -> DB9-USB |
| :-: | :-: |
| ![](imagenes/rs232-usb.png)| ![](imagenes/rs232-usb-adaptador.png) |
| Figura 1 | Figura 2|

En la figura 1 se muestra un cable de consola genérico, el cual presenta un conector RJ45 en uno de sus extremos y un DB9 en su otro lado.
Por otra parte, en la imagen 2, se visualiza un adaptador DB9-USB, el cual se puede anexar al cable de la figura 1, de forma tal de permitir el acceso a una consola desde computadoras que no cuenten con conector serial.

## Conexión RJ45-DB9 (Cable de consola)

Este cable se puede utilizar para interconectar un dispositivo Cisco desde la interfaz **consola** al puerto **RS232** de una PC como se muestra en la siguiente imagen: 

![](imagenes/db9.png)

Figura 3 - Conexión cable de consola


## Conexión RJ45-DB9 -> DB9-USB (Cable de consola a USB)

El cable adaptador de DB9 a USB permite la conexión al puerto **consola** desde cualquier PC que tenga puertos **USB**:

![](imagenes/usb.png)

Figura 4  - Conexión cable de consola


## Parámetros de configuración

| Ventana inicial | Parámetros de conexión serial |
| :-: | :-: |
| ![](imagenes/putty1.png)| ![](imagenes/putty2.png) |
| Figura 5 | Figura 6 |

Para la conexión serial, dependiendo el tipo de cable, es la configuración que se ha de usar en el argumento "Serial Line":

| Tipo de cable | Serial Line | 
| :-: | :-: |
| RJ45-DB9 (figura 3) | /dev/tty**S**0 |
| RJ45-DB9 -> DB9-USB (figura 4) |  /dev/tty**USB**0 |

Dentro de "Serial", dependiendo el modelo del equipo, se deben indicar los siguientes parámetros:

| Parámetro | Valor |
| :-: | :-: |
| Speed | 9600 |
| Flow control | None |
