# Gestión de contraseñas

## Terminal de configuración

```
equipo>
equipo> enable
equipo# configure terminal
equipo(config)# enable secret "contraseña"
```

Donde: | |
-- | --
**contraseña** = contraseña asignada al terminal | |

## Consola - Contraseña compartida

```
equipo>
equipo> enable
equipo# configure terminal
equipo(config)#line console 0
equipo(config-line)#password "contraseña"
equipo(config-line)#login 
```

Donde: | |
-- | --
**contraseña** = contraseña asignada a la consola | |


