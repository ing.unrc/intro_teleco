# Asignación de un nombre un equipo

Es de utilidad asignar un nombre al dispositivo que se encuentra configurando, de forma tal de identificarlo con mayor facilidad al momento de tener una red con múltiples equipos. Esto se logra mediante los siguientes pasos:

```
equipo# configure terminal
equipo(config)# hostname "nombre"
nombre(config)#
```

Donde: | |
-- | --
**nombre** = nombre a configurar en el equipo | |
