# Discos virtuales

Extensiones de discos virtuales: 

* *.vmdk: discos virtuales que pueden utilizarse en el programa VMware o en VirtualBox.

* *.vdi: archivo de imagen de disco virtual creado por VirtualBox.

* *.vhdx: Windows 8 Virtual Hard Drive file. Funciona como un disco físico y se almacena en un sólo archivo dentro del disco físico. Puede ser utilizado para crear backups.

* *.qcow/*.qcow2: imágenes de discos usadas por el programa QEMU
