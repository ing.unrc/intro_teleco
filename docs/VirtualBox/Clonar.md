# Clonar una máquina virtual

Es recomendable clonar una Máquina Virtual, cuando se está haciendo pruebas de un sistema operativo o implementación, ya que se puede guardar una instalación limpias o avances sobre una configuración en particular.

Para ello, hacer click con el botón derecho sobre la máquina virtual y seleccionar la opción "clonar":

![](imagenes/Clon.jpg)
