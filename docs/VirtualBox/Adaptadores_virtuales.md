# Adaptadores virtuales

Virtual Box cuenta con la posibilidad de crear adaptadores virtuales en la PC anfitrión, que luego se pueden asociar a las máquinas virtuales.
Es importante tener presente que se tiene acceso a dicha interfaz virtual, con lo que por ejemplo, es posible capturar el tráfico en ella o brindar salida a otras redes e internet.

## Gestión de adaptadores virtuales

Para agregar, quitar o configurar adaptadores virtuales, acceder a "File" y seleccionar la opción "Host Network Manager" e interactuar con la siguiente ventana:

![](imagenes/vboxnet.jpg)

El nombre genérico que se utiliza es vboxnet**x** donde **x** toma el número de adaptador.

## Visualización del adaptador en la PC anfitrión

Se puede ver en la PC anfitrión cada una de las interfaces creadas desde VirtualBox. Para ello, se pueden utilizar los comandos:

```
ip link show
```

o

```
ifconfig -a
```

Por ejemplo con "ifconfig -a":

![](imagenes/vboxnet0.jpg)

Ejemplo en wireshark:

![](imagenes/vboxnet0_wire.jpg)
