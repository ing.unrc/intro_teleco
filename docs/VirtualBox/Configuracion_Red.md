#Modos de conexiones de red

Dentro de la configuración de cada máquina virtual se puede asociar hasta cuatro adaptadores de red:

![](imagenes/Network_Configuration.jpg)

## Bridged Adapter

Esta opción permite interconectarse a la interfaz inalámbrica o alámbrica del anfitrión y acceder a la red a las redes a asociadas a la misma.
Hay que tener presente que el huesped no puede controlar las interfaces del anfitrión, sólo los adaptadores virtuales que se le configuren.

Asociación a una interfaz inalámbrica:

![](imagenes/BridgedAdapterWlan.jpg)

Asociación a una interfaz alámbrica:

![](imagenes/BridgedAdapterEther.jpg)

## NAT

Implementa una red interna oculta al anfitrión, para todas las máquinas virtuales que se configuren y le da acceso a internet, en caso que la PC física tenga conexión:

![](imagenes/NAT.jpg)

## Host-only Adapter

VirtualBox cuenta con la posibilidad de crear interfaces virtuales en el anfitrión (vease sección: [gestion adaptadores virtuales](Adaptadores_virtuales.md)), los cuales pueden asociarse a los adaptadores de red de máquinas virtuales. 

![](imagenes/Host-only.jpg)

## Internal Network

Es posible crear múltiples redes virtuales con validez entre Máquinas Virtuales de la forma:

![](imagenes/Internal-Network.jpg)

Los huespedes se interconectan al utilizar el mísmo nombre de red interna.
