## Wireless

Artículo de referencia: https://netplan.io/examples

Como en la configuración estática de red, se puede realizar una conexión inalámbrica utilizando netplan.
Editar el archivo: /etc/netplan/01-netcfg.yaml (este nombre puede variar)

```bash
network:
  version: 2
  renderer: networkd
  wifis:
    "nombre de interfaz wireless":
      dhcp4: "yes/no"
      dhcp6: "yes/no"
      addresses: ["IP"/"máscara"]
      gateway4: "IPgw"
      nameservers:
        addresses: ["servidor1", "servidor2"]
      access-points:
        "Nombre de la red":
          password: "contraseña"
```

### Aplicar cambios
Después de editar el archivo es necesario reiniciar los servicios de red para que se ejecuten los cambios:
```bash
sudo netplan apply
```
