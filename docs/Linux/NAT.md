# NAT

NAT - Network Address Translation, permite realizar el mapeo de las direcciones IP origen, IP destino, puerto origen y puerto destino, de un paquete a la entrada y salida de un dispositivo.

## Visualización de tabla NAT

```
sudo iptables -t nat -n -L 
```

## Habilitar nat en el equipo

```
sudo modprobe iptable_nat 
```

## Enmascaramiento con una dirección IP dinámica

```
sudo iptables -t nat -A POSTROUTING -o "interfaz" -j MASQUERADE
```

Donde: | |
-- | --
**interfaz** = Interfaz de salida del paquete |

**IMPORTANTE: Prestar atención al nombre de la interfaz, ya que de ingresarla en forma incorrecta, Linux no informa en pantalla sobre el error.**

El comando puede leerse como:

en la **-t nat** tabla nat, **-A** adicionar **POSTROUTING** después del ruteo, a los paquetes que crucen la **-o** interfaz de salida, **-j masquerade** enmascarandola por la ip de la interfaz de salida.


Ejemplo: |
-- |
Trasladar las direcciones IP de origen de los paquetes que salgan por la interfaz enp4s0 a una dirección IP de dicha interfaz (puede utilizarse en caso que enp4s0 sea cliente dhcp): |
``` sudo iptables -t nat -A POSTROUTING -o enp4s0 -j MASQUERADE ``` |

## Enmascaramiento con una dirección IP estática

```
sudo iptables -t nat -A POSTROUTING -o "interfaz" -j SNAT --to "x.x.x.x"  
```

Donde: | |
-- | --
**interfaz** = Interfaz de salida del paquete | **x.x.x.x** = IP a utilizar | 

**IMPORTANTE: Prestar atención al nombre de la interfaz, ya que de ingresarla en forma incorrecta, Linux no informa en pantalla sobre el error.**

El comando puede leerse como:

en la **-t nat** tabla nat, **-A** adicionar **POSTROUTING** después del ruteo, a los paquetes que crucen la **-o** interfaz de salida, modificar **-j SNAT** la dirección ip de origen, **--to** enmascarandola con la IP **x.x.x.x**.

Ejemplo: |
-- |
Trasladar las direcciones IP de origen de los paquetes que salgan por la interfaz enp2s0 a la dirección IP 192.168.2.1: |
``` sudo iptables -t nat -A POSTROUTING -o enp2s0 -j SNAT --to 192.168.2.1 ``` |
