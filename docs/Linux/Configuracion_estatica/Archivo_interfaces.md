# Configuración en archivo /etc/network/interfaces

La configuración a partir del archivo **/etc/network/interfaces** se encuentra disponible para versiones anteriores a Ubuntu 18.04 o en distribuciones particulares de Linux.

La configuración de estos archivos permite:

* Asignar una dirección de IP a las placas de red de la PC (estática o cliente DHCP)
* Agregar rutas
* Definir servidores DNS

Sus ventajas son:

* Se configura todo desde un único archivo
* Fácil de editar

Para editar el archivo se puede utilizar cualquier editor de texto, los disponibles en Ubuntu:
**vi** / **nano**, editor en consola
**gedit** / **geany**, necesitan el entorno gráfico gnome

### Edición de archivo /etc/network/interfaces
Configuración de dirección IP ESTÁTICA + RUTAS + DNS a una interfaz

```bash
auto "interfaz"
iface "interfaz" inet static
address "x.x.x.x"        # IP de la interfaz a configurar
netmask "y.y.y.y"        # Máscara de la red
network "z.z.z.z"        # IP de la red a configurar
broadcast "b.b.b.b"      # Dirección de Broadcast de la red
gateway "g.g.g.g"        # IP de la puerta de enlace
nameserver "d.d.d.d"     # IP del servidor de DNS
```

Donde:||
-- | -- |
**interfaz** = nombre de la interfaz a configurar | **yes/no** = permite o no la implementación de cliente DHCP |
**x.x.x.x** = dirección IP estática | **y.y.y.y** = máscara de red |
**z.z.z.z** = dirección IP de la red a la que se conectará | **b.b.b.b** = dirección de broadcast de la red |
**g.g.g.g** = dirección IP de la puerta de enlace | **d.d.d.d** = dirección IP del servidor DNS |

#### Configuración de una interfaz como cliente DHCP
Para configurar que una interfaz obtenga configuración de red por DHCP, se debe ingresar la siguiente configuración

```bash
auto "interfaz"
iface "interfaz" inet dhcp
```

Donde:||
-- | -- |
**interfaz** = nombre de la interfaz a configurar como cliente DHCP |

#### Aplicación de los cambios
Después de editar el archivo es necesario reiniciar los servicios de red para que se ejecuten los cambios:

##### Ubuntu

```bash
sudo /etc/init.d/network-manager restart
```

##### Debian

```bash
sudo /etc/init.d/networking restart
```
