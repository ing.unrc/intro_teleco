# Cliente PPP
## Instalación de cliente PPPoE

Instalar el software mediante apt:

```bash
sudo apt update
sudo apt install -y pppoeconf pppoe
```

## Configuración de la conexión
Ejecutar el comando pppoeconf y completar los datos de usuario y contraseña

```bash
pppoeconf 
```
## Inicialización de la conexión

Ejecutar el comando y verificar que se haya establecido la conexión con el comando ip o ifconfig.

```bash
pon dsl-provider
```

## Cierre de conexión

Para finalizar la conexión, ejecutar el siguiente comando:

```bash
poff dsl-provider
```
