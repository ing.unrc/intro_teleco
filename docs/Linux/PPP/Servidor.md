# Configuración del equipo como Servidor PPPoE

## Instalación de servidor PPPoE 

Descargar de internet el archivo de instalación: rp-pppoe-x.gz, donde x es la versión del programa.

Descomprimir el archivo:

```bash
tar -xvzf rp-pppoe-x.tar.gz

#Acceder a la carpeta:
cd rp-pppoe-x/src 

#Compilar e instalar la aplicación:
./configure 
make 
make install
```

## Configuración de servidor PPPoE
Opciones del servidor
Editar el archivo /etc/ppp/pppoe-server-options: 

```console
require-chap 
lcp-echo-interval 10 
lcp-echo-failure 2 
netmask 255.255.255.255
ms-dns d.d.d.d 
# tipo de encriptación
# intervalos de arrendamiento de direcciones

# máscara de 32 bits 
# d.d.d.d = Dirección del servidor DNS
```

Donde: | |
-- | --
**require-chap**: indica que la autentificación de usuarios se hará mediante la encriptación CHAP. | **lcp-echo-interval 10**: establece el intervalo de tiempo de verificación del enlace.
**lcp-echo-failure 2**: cantidad de comprobaciones toleradas antes de terminar la sesión PPP. 

## Carga de clientes PPPoE

Cuando se utiliza autenticación CHAP, estas cuentas se encuentran definidas en el archivo /etc/ppp/chap-secrets. La sintaxis de este archivo es la siguiente: 

```bash
# Secrets for authentication using CHAP 
# client        server     secret             IP addresses 
"usuario1"      *          "contraseña1"      x.x.x.x
"usuario2"      *          "contraseña2"      * 
```

Donde: | |
-- | --
**x.x.x.x** = dirección IP estática asignada al usuario1 | **\*** = indica que al usuario2 se le asigna una dirección IP en forma dinámica.

Se recomienda restringir los permisos de acceso al archivo mediante el siguiente comando:
```bash
sudo chmod 600 /etc/ppp/chap-secrets 
```
### Pool de direcciones IP

Para ofrecer a los clientes IP en forma dinámica, es necesario reservar un pool de direcciones. Para ello, se debe crear un archivo dentro de la carpeta /etc/ppp/ y con listado de direcciones IP disponibles.

```bash
#nano /etc/ppp/direcciones 
```

Dentro de este archivo, se definen las direcciones a emplear, ingresando una dirección por línea o bien definiendo un rango de direcciones, como por ejemplo, 192.168.50.2-30. 
El nombre del archivo debe indicarse al iniciar el servidor (ver 10.4)

## Inicio del servidor

```bash
sudo pppoe-server -C n -L x.x.x.x -p PoolDeDirecciones -I interfaz -m 1412
```

Donde: | |
-- | -- 
**n** = nombre del servidor | **interfaz** = Interfaz donde se ofrece el servicio PPPoE (red LAN) 
**PoolDeDirecciones** = Ubicación del archivo que contiene el pool de direcciones | **x.x.x.x** = Dirección IP de la interfaz LAN

## Cierre del servidor

```bash
sudo killall pppoe-server   
```

## Análisis de funcionamiento desde los archivos log del sistema

Se puede utilizar los archivos log del sistema para ver problemas internos del servidor, como un arranque fallido. También pueden ser analizados desde un sniffers, siempre y cuando los paquetes salgan con la tarjeta de red y el problema no sea interno del server: 

```bash
sudo gedit /var/log/syslog
```
