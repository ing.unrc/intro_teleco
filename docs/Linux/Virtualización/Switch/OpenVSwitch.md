# Open vSwitch

Sitio Oficial [www.openvswitch.org](https://www.openvswitch.org/)

**ovs-vsctl** es un programa que permite la configuración de interfaces dentro de la base de datos de Open vSwitch.  

## Crear/Eliminar un bridge

```
ovs-vsctl "add-br/del-br" "NombreBridge"
```

Donde: ||
--|-- |
**add-br** = crear el bridge | **del-br** = eliminar el bridge |
**NombreBridge** = Nombre del bridge a crear||

## Agregar/Quitar interfaces a un bridge

```
ovs-vsctl "add-port/del-port" "NombreBridge" "NombreInterfaz"
```

Tener presente que no sólo se pueden incorporar interfaces físicas del dispositivo, sino también interfaces virtuales, como pueden ser las creadas con el comando **ip** como se muestra en el siguiente [enlace](../InterfacesVirtuales/comando_ip)

Donde: ||
--|-- |
**add-port** = agregar la interfaz al brige | **del-port** = quitar la interfaz del brige |
**NombreBridge** = Nombre del bridge al que se le modifica la interfaz | **NombreInterfaz** = nombra de la interfaz a modificar | 

## Visualización de configuración de bridges

Mediante el comando **ovs-vsctl** con el argumento **shoq** se puede observar los ovs presentes en el sistema y los puertos asociados:

```
ovs-vsctl show
```

## Visualización de la configuración de los puertos en un bridge

Mediante el comando **ovs-vsctl** con los argumentos **list port** se puede observar la configuración de cada uno de los puertos de los ovs:

```
ovs-vsctl list port
```

## Visualización de tabla MAC

```
ovs-appctl fdb/show "NombreBridge"
```

Donde: |
--|
**NombreBridge** = Nombre del bridge al que se le solicita la tabla MAC|
