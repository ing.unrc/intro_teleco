# Configuración de interfaz virbr0 - libvirt

Sitio oficial: <a href="https://libvirt.org/" target="_blank">libvirt.org</a>

La librería **libvirt** ofrece una interfaz virtual (cuyo nombre lógico suele ser **virbr0**), la cual implementa una red virtual interna.

En el archivo de configuración que se muestra en esta sección, se puede modificar modo de reenvío de paquetes, NAT, dirección IP e incluir un servidor de DHCP. Para modificar la configuración de la interfaz, se recomienda:

 1- Destruir la configuración actual.

 2- Editarar los parámetros en el archivo de configuración.

 3- Implementar los cambios.

## Destrucción de configuración

```bash
virsh net-destroy default
```

## Edición de configuración de la interfaz

Para configurar los distintos parámetros, se puede ejecutar el siguiente parámetro, el cual abre un archivo con extensión ".xml":

```bash
virsh net-edit default
```

Contenido del archivo:

```xml
<network>
  <name>default</name>
  <uuid>ab89d77a-c1f1-4594-85d9-36fd7cedaae4</uuid>
  <forward mode='modo'/>
  <bridge name='nombre' stp='on' delay='0'/>
  <mac address='xx:xx:xx:xx:xx:xx'/>
  <ip address='x.x.x.x' netmask='mascara'>
    <dhcp>
      <range start='IPinicio' end='IPfinal'/>
    </dhcp>
  </ip>
</network>
```

IMPORTANTE: Para evitar confusiones, se recomienda utilizar la opción: **forward mode='open'**.

Donde: | |
-- | -- |
**modo** = Opciones: **nat** / **route** / **open** / **bridge** / **private** / **vepa** / **passthrough** / **hostdev** ver <a href="https://labredes.gitlab.io/linux/libvirt_Forward" target="_blank">enlace</a> |
**nombre** = nombre de la interfaz virtual, por defecto "virbr0" | **xx:xx:xx:xx:xx:xx** = dirección mac |
**x.x.x.x** = dirección IP que se le asigna a la interfaz | **mascara** = máscara de red asociada a la interfaz |
**IPinicio** = Primera IP del rango | **IPfinal** = Última IP del rango |

### Archivo por defecto

A continuación se muestra un ejemplo de la configuración por defecto. Puede ser de utilidad para restaurar la configuración inicial de la interfaz en una PC en particular.

```bash
<network>
  <name>default</name>
  <uuid>85a1117f-facc-4adf-ba10-55a28a414cda</uuid>
  <forward mode='nat'/>
  <bridge name='virbr0' stp='on' delay='0'/>
  <mac address='52:54:00:3b:39:de'/>
  <ip address='192.168.122.1' netmask='255.255.255.0'>
    <dhcp>
      <range start='192.168.122.2' end='192.168.122.254'/>
    </dhcp>
  </ip>
</network>
```

## Implementación de configuración

```bash
virsh net-start default
```
