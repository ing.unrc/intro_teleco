# Creación de interfaz virual - comando ip

Las interfaces **veth** (virtual Ethernet) pueden utilizarse como:

* una simple interfaz virtual.
* un enlace virtual, interconectando namespaces o un namespace con una red física.

## Carga de una interfaz virtual

```
ip link add "NombreInterfaz" type veth
ip link set dev "NombreInterfaz" up
```

Donde: ||
-- | --
**NombreInterfaz** = Nombre asignado a la interfaz virtual que se ha de crear ||

## Carga de un enlace virtual

La asociación entre dos interfaces ya sea virtual a física o virtual a virtual, se realiza con el comando **ip link add** de la forma en que se muestra a continuación:

```
ip link add "Interfaz1" type veth peer name "Interfaz2"
ip link set dev "Interfaz1" up
ip link set dev "Interfaz2" up
```

Cabe destacar que es necesario habilitar las interfaces, ya que por defecto se inicializan dadas de baja.

Donde: ||
--|--
**Interfaz1** = nombre de interfaz a un extremo del enlace | **Interfaz2** = nombre de interfaz al otro exremo del enlace |

### Ejemplo de conexión entre dos namespaces

Considerando la integración de dos namespaces (ver concepto de namespace en el siguiente [enlace](../Namespaces.md)), crear un enlace virtual entre el namespace **h1** y el namespace **h2**, con las interfaces **eth1** y **eth2** respectivamente. Posterior a ello, configurar una dirección IP a cada entorno huesped y hacer ping entre ambos:

![](2namespaces.png)

```
# Carga de dos namespaces, h1 y h2:
ip netns add h1
ip netns add h2

# Carga del enlace virtual entre las interfaces virtuales que se asignaran a ambos namespaces:
ip link add eth1 type veth peer name eth2

# Asociacion de interfaces a los namespaces:
ip link set dev eth1 netns h1
ip link set dev eth2 netns h2

# Activacion de interfaces dentro de cada namespace:
ip netns exec h1 ip link set eth1 up
ip netns exec h2 ip link set eth2 up

# Asignacion de direcciones IP a cada interfaz dentro de los namespaces:
ip netns exec h1 ip address add 10.0.0.1/8 dev eth1
ip netns exec h2 ip address add 10.0.0.2/8 dev eth2

# Prueba de conectividad entre h2 y la IP de h1:
ip netns exec h2 ping 10.0.0.1
```
