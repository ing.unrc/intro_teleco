# Namespaces

**Namespace** es una copia lógica del stack de red aislada, con sus propias rutas, reglas de firewall y dispositivos de red.

Documentación de referencia: [enlace](https://manpages.ubuntu.com/manpages/impish/man8/ip-netns.8.html).

## Comandos

A continuación se listan los principales comandos para la implementación de namespaces en Linux.

### Crear un nuevo namespace

```
ip netns add "nombre"
```

Donde: |
-- |
**nombre** = nombre del namespace a crear |

### Listar namespaces existentes

Mostrar todos los nombres de espacios de red:

```
ip netns list
```

El comando retorna todos los valores disponibles en **/var/run/netns**.

### Eliminar uno o todos los namespaces

Para eliminar un namespace se utiliza el argumento **delete**. Si se desea eliminar todos los namespaces, se debe implementar la opción **-all**

```
sudo ip [-all] netns delete "nombre"
```

### Asignación de interfaces a un namespace

A partir del comando **ip** con la opción **link**, se puede asignar una interfaz a un namespace en particular:

```shell
sudo ip link set dev "interfaz" netns "nombre"
```

Donde: | |
-- | -- |
**interfaz** = nombre de la interfaz que se quiere asignar | **nombre** = nombre del namespace a crear |

### Ejecución de comandos dentro de un namespace

Para la ejecución de un comando en un namespace específico, se debe implementar el siguiente comando:

```bash
sudo ip netns exec "nombre" "comando"
```

Donde: | |
-- | -- |
**nombre** = nombre del namespace a crear | **comando** = comando a ejecutar en el namespace |


## Ejemplo

Considerando que se quiere implementar el siguiente diagrama de red:

<div align="center"> <img src="https://gitlab.com/labredes/linux/-/raw/master/docs/imagenes/namespaces/ej1.png"></div>

<div align="center"> Figura 1 - Arquitectura a implementar </div>

A partir de que se cuenta con una computadora con dos interfaces de red como la siguiente:

<div align="center"> <img src="https://gitlab.com/labredes/linux/-/raw/master/docs/imagenes/namespaces/ej2.png"></div>

<div align="center"> Figura 2 - Recursos físicos </div>

Si sólo se asigna configuración de red a ambas placas, existirá un conflicto, ya que las dos interfaces se encontrarán en la misma dirección de red y en caso de, por ejemplo, querer hacer un ping a una cualquiera de las dos direcciones de red, el mensaje ICMP se resolverá localmente, sin necesidad de salir de la PC.

<div align="center"> <img src="https://gitlab.com/labredes/linux/-/raw/master/docs/imagenes/namespaces/ej3.png"></div>

<div align="center"> Figura 3 - División en manespaces </div>

Para poder realizar la prueba correctamente, es posible asignar una de las interfaces a un nuevo namespace, de forma tal de generar todo el stack de protocolos en forma aislada y de esa forma poder comunicar ambas configuraciones de red en forma paralela.

<div align="center"> <img src="https://gitlab.com/labredes/linux/-/raw/master/docs/imagenes/namespaces/ej4.png"></div>

<div align="center"> Figura 4 - Diagrama equivalente implementando namespaces </div>

Configuración PCA:

```bash
# Configuración de dirección IP a la interfaz "enp2s0" dentro del namespace por defecto:
sudo ip address add 192.168.1.100/24 dev enp2s0
```

Configuración PCB:

```bash
# Crear un namespace llamado "huesped":
sudo ip netns add huesped

# Asignar la interfaz "enp4s0" al namespace "huesped":
sudo ip link set dev enp4s0 netns huesped

# Configurar la dirección IP al namespace "huesped":
sudo ip netns exec huesped ip add add 192.168.1.101/24 dev enp4s0

# Habilitar la interfaz "enp4s0":
sudo ip netns exec huesped ip link set enp4s0 up

# Comprobación de conectividad mediante un ping:
sudo ip netns exec huesped ping 192.168.1.100
```
