# Cliente DHCP

Para configurar la placa dinámicamente a través de un servidor DHCP se ejecuta el comando dhclient seguido del nombre de la interfaz:

```bash
sudo dhclient "interfaz"
```

Donde: | |
-- | --
**interfaz** = es el nombre lógico de la interfaz que se desea configurar por medio de dhcp. |