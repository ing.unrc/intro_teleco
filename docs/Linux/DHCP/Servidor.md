# Servidor DHCP

## Instalación

En caso de no contar con el servidor instalado en el sistema operativo, el comando para su instalación es el siguiente:

```bash
#apt install isc-dhcp-server
```

## Configuración 

Editar el archivo /etc/default/isc-dhcp-server, indicando la interfaz/ces donde se ofrece el servicio:

```bash
INTERFACES="iterfaz"
```
Donde: | |
-- | --
**interfaz** = es el nombre lógico de la interfaz, se debe ingresar el nombre de la interfaz entre comillas. |

Generar o editar el fichero /etc/dhcp/dhcpd.conf

```bash
subnet "IPred" netmask "Mascara" {
  option routers "IPgw";
  option broadcast-address "IPbroadcast";
  ddns-domainname "hpc.local";
  next-server "IPprox";
  filename "fai/pxelinux.0";
  
  host "NombrePC" {
    hardware ethernet "MAC";
    option host-name "NombrePC";
    fixed-address "IPpc";
  }
```

Donde: | |
-- | --
**IPred" netmask "Mascara"

## Reinicio del servicio

```bash
# systemctl restart isc-dhcp-server
```
