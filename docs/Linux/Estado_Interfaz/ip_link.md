# Estado de interfaces - Comando ip link

**ip link** se encuentra disponible en versiones recientes de Linux. 

De ser necesaria su instalación, adicionar el paquete **iproute2** como se muestra a continuación. 

De no encontrarse en el sistema, puede utilizarse el [comando ifconfig](/linux/Estado_Interfaz/lshw).

## Instalación

De no encontrarse el comando **ip**, se puede instalar con:

```bash
sudo apt update
sudo apt install iproute2
```

## Visualización de estado de interfaces

### Visualización abreviada

```
ip -c -br link

"interfaz"      "estado"        "MAC"       <"modos"> 
```

El argumento **-c** permite resaltar parte de la configuración con colores. Por otro lado **-br** permite organizar el resultado en una tabla.

Donde: ||
-- | -- |
**interfaz** = Nombre lógico de la interfaz. | **estado** = si está funcionando UP o no DOWN.
**MAC** = dirección MAC de la interfaz | **modos** = modos de funcionamiento (BROADCAST,MULTICAST,UP,LOOPBACK...) 

### Visualización Completa

Para modificar conocer el estado de las interfaces, se implementa el comando ip con la opción link:

```bash
ip -c link

"numero": "interfaz" <"modos"> mtu "tamañomtu" qdisc "cola" state "estado"...
```

Donde: ||
-- | -- |
**numero** = número de interfaz mostrada. | **interfaz** = Nombre lógico de la interfaz.
**modos** = modos de funcionamiento (BROADCAST,MULTICAST,UP,LOOPBACK...) | **tamañomtu** = indica el valor del mtu.
**cola** = tipo de cola implementada en la interfaz. | **estado** = si está funcionando UP o no DOWN.

Ejemplo: |
-- |
``` 1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000 link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00``` |
``` 2: enp2s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN mode DEFAULT group default qlen 1000 link/ether 2c:60:0c:85:47:0f brd ff:ff:ff:ff:ff:ff``` |
En este caso se encuentran las interfaces **lo** (loopback) y la interfaz alámbrica **enp2s0**.  |
**enp2s0** tiene los modos: "NO-CARRIER,BROADCAST,MULTICAST,UP", tiene un mtu de 1500, implementa una cola del tipo "fq_codel" y se encuentra en estado "DOWN" lo que implica que puede estar deshabilitada administrativamente o que no tiene ningún cable conectado. |

## Activación/desactivación de una placa de red - Comando ip link

```bash
sudo ip link set "interfaz" "up/down"
```

Donde: | |
-- | -- |
**interfaz** = interfaz de red que se desea configurar. |
**up** = habilita la interfaz. | **down** = deshabilita la interfaz.
