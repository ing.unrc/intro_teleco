# Estado de interfaces - Comando lshw

El comando List Hardware lshw muestra datos del hardware, el filtro -c network provee datos de las conexiones de red. Es necesario ejecutar el comando como administrador:

``` bash
sudo lshw -c network
```

Del listado que aparece observe los siguientes datos para verificar el estado de las placas de red instaladas en el equipo y determinar cuál es el estado de la placa que va a configurar.

*-network 1 DISABLED
Indica el estado habilitado/deshabilitado de la placa
logical name: enp1s0
Identifica el nombre de la placa de red
serial: d8:d3:85:23:8a:c0
Identifica el número de MAC de dicha placa
link=yes
Muestra el estado de conexión física

## Activación/desactivación de una placa de red -  Comando ifconfig

**ifconfig** se encuentra disponible en versiones antiguas de Ubuntu o en distribuciones acotadas de Linux. 

De no encontrarse en el sistema, puede utilizarse el [comando ip](/Estado_interfaz/ip_link).

```bash
sudo ifconfig "interfaz" "up/down"
```

Donde: | |
-- | -- |
**interfaz** = interfaz de red que se desea configurar. | 
**up** = habilita la interfaz. | **down** = deshabilita la interfaz.
