# Tabla de rutas - Comando route

**route** se encuentra disponible en versiones antiguas de Linux. 

De ser necesaria su instalación, adicionar el paquete **net-tools**, como se muestra en el comando ifconfig: [enlace](/linux/Direcciones_IP/ifconfig). 

De no encontrarse en el sistema, puede utilizarse el [comando ip route](/linux/Tabla_de_rutas/ip_route).

## Visualización de puerta de enlace/tabla de rutas

```bash
route -n
```

## Configuración de puerta de enlace (Gateway)

```bash
sudo route "add/del" default gw "z.z.z.z"
```

Donde: | |
-- | --
**add** = adiciona una nueva línea a la tabla | **del** = elimina una línea preexistente en la tabla
**z.z.z.z** = dirección IP de la puerta de enlace

### Ejemplo
Para a un caso práctico, visualizar el video [ejemplo 1.2.2](/linux/Ejemplos/Parametros_Basicos/122-comando-route)

## Carga de rutas en forma estática

```bash
sudo route "add/del" -net "x.x.x.x" netmask "y.y.y.y" gw "z.z.z.z"
```

Donde: | |
-- | --
**add** = adiciona una nueva línea a la tabla | **del** = elimina una línea preexistente en la tabla
**x.x.x.x** = dirección de red a alcanzar | **y.y.y.y** = máscara de red en decimal u octal, según el comando
**z.z.z.z** = dirección IP del próximo salto
