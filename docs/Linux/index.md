#Linux

Este tutorial presenta los comandos básicos de configuración de red en el sistema operativo Linux, orientado a la configuración de distribuciones **UBUNTU**.

En particular, para la configuración de direcciones IP y tabla de rutas se ofrecen dos alternativas, comando **ip** y comandos **ifconfig**/**route**. La selección del comando depende de la versión de Linux que se esté utilizando.

En la sección [Ejemplos](Ejemplos/Parametros_Basicos.md) se presenta la configuración de un dispositivo Linux con los parámetros:

![](imagenes/Diagrama1.png)

* Dirección IP.
* Puerta de enlace.
* DNS.

## Conexión al LabRedes

### Gestor iproute2 (comando ip)

El gestor iproute2 cuenta con el comando ip, para la configuración de parámetros de red:

```bash
# Cambiar a superusuario
sudo su

# Visualizar el estado de las interfaces (labredes.gitlab.io/linux/Estado_Interfaz/ip_link)
ip -c link

# En caso de ser necesario, habilitar la interfaz (labredes.gitlab.io/linux/Estado_Interfaz/ip_link)
ip link set dev "interfaz" up

# Ver direccion IP configurada a todas las interfaces (labredes.gitlab.io/linux/Direcciones_IP/ip_address)
ip -c address show

# Asignar direccion IP a la interfaz (labredes.gitlab.io/linux/Direcciones_IP/ip_address)
ip address add "IP"/"mascara" dev "interfaz"

# Configuracion de puerta de enlace (labredes.gitlab.io/linux/Tabla_de_rutas/ip_route)
ip route add default via "IPgw"

# Configurar servidor DNS (labredes.gitlab.io/linux/DNS)
echo nameserver "IPdns" > /etc/resolv.conf
```

Donde: ||
-- | -- |
**interfaz** = nombre de la interfaz conectada a la red | **IP** = Dirección IP a configurar
**mascara** = máscara de red (en decimal) | **IPdns** = dirección IP del servidor DNS |
**IPgw** = IP de la puerta de enlace||

### Gestor net-tools (comandos ifconfig - route)

El gestor net-tools cuenta con el comando ifconfig y route, para la configuración de parámetros de red:

```bash
# Cambiar a superusuario
sudo su

# Visualizar el estado de las interfaces (labredes.gitlab.io/linux/Estado_Interfaz/lshw)
lshw -c network

# En caso de ser necesario, habilitar la interfaz (labredes.gitlab.io/linux/Estado_Interfaz/lshw)
ifconfig "interfaz" up

# Ver direccion IP configurada a todas las interfaces (labredes.gitlab.io/linux/Direcciones_IP/ifconfig)
ifconfig

# Asignar direccion IP a la interfaz (labredes.gitlab.io/linux/Direcciones_IP/ifconfig)
ifconfig "interfaz" "IP"/"mascara"

# Configuracion de puerta de enlace (labredes.gitlab.io/linux/Tabla_de_rutas/route)
route add default gw "IPgw"

# Configurar servidor DNS (labredes.gitlab.io/linux/DNS)
echo nameserver "IPdns" > /etc/resolv.conf
```

Donde: ||
-- | -- |
**interfaz** = nombre de la interfaz conectada a la red | **IP** = Dirección IP a configurar
**mascara** = máscara de red (en decimal) | **IPdns** = dirección IP del servidor DNS |
**IPgw** = IP de la puerta de enlace||

