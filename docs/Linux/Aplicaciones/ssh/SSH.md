# SSH

## Instalación de software
### Servidor

Para poder comunicarse con un dispositivo por medio de SSH, éste debe configurarse como servidor SSH, de forma tal de esperar conexiones en el puerto 22. Una opción de instalación es:

```bash
sudo apt update
sudo apt install openssh-server
```
### Cliente

```bash
sudo apt update
sudo apt install openssh-client
```
## Acceso por SSH

Ingresando el comando ssh en un cliente de este servicio, es posible el acceso a un servidor, de la forma:

![](ssh.png)

```bash
ssh "UserName"@"x.x.x.x" 
```

Donde: | |
-- | -- 
**UserName** = nombre de usuario en el servidor SSH | **x.x.x.x** = dirección IP del servidor SSH

## Copia de archivos con comando SCP

### Copia de un archivo desde una PC local a una PC remota

Con el comando que se presenta a continuación, es posible el envío de un archivo a un dispositivo que se encuentre configurado como servidor ssh:

![](scp2.png)

```bash
sudo scp "UbicaciónOrigen" "UserName"@"x.x.x.x":/"ubicación"
```

Donde: | |
-- | --
**UserName** = nombre de usuario en el servidor | **UbicacionOrigen** = ubicación del archivo a copiar en el dispositivo remoto | 
**x.x.x.x** = dirección IP del servidor | **UbicacionDestino** = posición donde se quiere guardar el archivo en el servidor

### Copia de un archivo desde una PC remota a la PC local

A partir del siguiente comando, se puede descargar un archivo desde un dispositivo que funcione como servidor ssh:

![](scp1.png)

```bash
sudo scp "UserName"@"x.x.x.x":/"UbicacionOrigen" "UbicacionDestino"
```

Donde: | |
-- | --
**UserName** = nombre de usuario en el servidor | **UbicacionOrigen** = ubicación del archivo a copiar en el dispositivo remoto | 
**x.x.x.x** = dirección IP del servidor | **UbicacionDestino** = posición donde se quiere guardar el archivo localmente
