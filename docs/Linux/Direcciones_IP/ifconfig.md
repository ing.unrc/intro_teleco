# Direcciones IP - Comando ifconfig

**ifconfig** se encuentra disponible en versiones antiguas de Ubuntu o en distribuciones acotadas de Linux. 

De ser necesaria su instalación, adicionar el paquete **net-tools** como se muestra a continuación. 

De no encontrarse en el sistema, puede utilizarse el [comando ip](/linux/Direcciones_IP/ip_address).

## Instalación

De no encontrarse **ifconfig**, se puede instalar con:

```bash
sudo apt update
sudo apt install net-tools
```

## Visualización de configuración IP

```bash
ifconfig
```

## Asignación de una dirección IP a una interfaz

```bash
sudo ifconfig "interfaz" "x.x.x.x"/"y"
```

Luego de asignar la configuración, se recomienda visualizar sus resultados.

## Ejemplo

Para a un caso práctico, visualizar el video [ejemplo 1.1.2](/linux/Ejemplos/Parametros_Basicos/#112-comando-ifconfig)