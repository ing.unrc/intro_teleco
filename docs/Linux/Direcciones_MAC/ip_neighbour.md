# Direcciones MAC - Tabla ARP - Comando: ip neighbour

## Visualización tabla ARP

```bash
ip -c neighbour show

"ip" dev "interfaz" lladdr "MAC" STALE
```

Donde ||
-- | -- |
**ip** = Dirección IP alcanzable | **interfaz** = nombre de interfaz por la cual se alcanza al dispositivo |
**MAC** = Dirección MAC de la interfaz que tiene configurada la dirección IP | | 

## Limpeza de caché ARP por completo

A partir del siguiente comando, es posible eliminar todas las entradas de la tabla ARP:

```bash
sudo ip -s -s neighbour flush all
```
## Limpeza una entrada de la tabla ARP

A partir del siguiente comando, es posible eliminar una entrada de la tabla ARP:

```bash
sudo ip neighbour del "ip" dev "interfaz"
```

Donde ||
-- | -- |
**ip** = Dirección IP alcanzable | **interfaz** = nombre de interfaz por la cual se alcanza al dispositivo |

## Carga manual de una dirección MAC a la tabla ARP

A partir del siguiente comando, es posible cargar en forma estática una asociación entre una dirección IP y dirección MAC asociada a la interfaz donde se encuentra configurada dicha IP:

```bash
sudo ip neighbour add "ip" lladdr "MAC" dev "interfaz"
```

Donde ||
-- | -- |
**ip** = Dirección IP alcanzable | **interfaz** = nombre de interfaz por la cual se alcanza al dispositivo |
**MAC** = Dirección MAC de la interfaz que tiene configurada la dirección IP | | 
